import pytest

from raik_history.calculations.aggregates import get_max, time_weighted_sum
from raik_history.db.data_tables import add_value, add_many_values
from raik_history.quality import QualityCodes
from raik_history.series.constants import TIME_TYPE_EPOCH_MS
from raik_history.series.series import Series


@pytest.fixture
def some_good_data(real_series_connection, real_series_name):
    # fmt: off
    add_value(real_series_connection, real_series_name, 12000, 53.2, QualityCodes.GOOD_NON_SPECIFIC)
    add_value(real_series_connection, real_series_name, 12005, 42.1, QualityCodes.GOOD_NON_SPECIFIC)
    add_value(real_series_connection, real_series_name, 12010, 82.2, QualityCodes.GOOD_NON_SPECIFIC)
    add_value(real_series_connection, real_series_name, 12015, 42.3, QualityCodes.GOOD_NON_SPECIFIC)
    # fmt: on


@pytest.mark.usefixtures("some_good_data")
def test_get_max__normal(real_series_connection, real_series_name):
    assert (12010, 82.2, QualityCodes.GOOD_NON_SPECIFIC) == get_max(
        real_series_connection, real_series_name, 0, 13000
    )


@pytest.fixture
def mostly_bad_data(real_series_connection, real_series_name):
    # fmt: off
    add_value(real_series_connection, real_series_name, 12000, 53.2, QualityCodes.GOOD_NON_SPECIFIC)
    add_value(real_series_connection, real_series_name, 12005, 42.1, QualityCodes.BAD_NOT_CONNECTED)
    add_value(real_series_connection, real_series_name, 12010, 82.2, QualityCodes.BAD_NOT_CONNECTED)
    add_value(real_series_connection, real_series_name, 12015, 42.3, QualityCodes.GOOD_LOCAL_OVERRIDE)  # noqa: e501
    # fmt: on


@pytest.mark.usefixtures("mostly_bad_data")
def test_get_max__ignore_bad_values(real_series_connection, real_series_name):
    assert (12000, 53.2, QualityCodes.GOOD_NON_SPECIFIC) == get_max(
        real_series_connection, real_series_name, 0, 13000
    )


@pytest.mark.usefixtures("mostly_bad_data")
def test_get_max__include_bad_values(real_series_connection, real_series_name):
    assert (12010, 82.2, QualityCodes.BAD_NOT_CONNECTED) == get_max(
        real_series_connection, real_series_name, 0, 13000, include_bad_values=True
    )


def test_get_max__include_bad_values_includes_uncertain(
    real_series_connection, real_series_name
):
    """If Bad values are allowed, so are uncertain inherently"""
    # fmt: off
    add_value(real_series_connection, real_series_name, 12000, 53.2, QualityCodes.BAD_NON_SPECIFIC)
    add_value(real_series_connection, real_series_name, 12005, 62.1, QualityCodes.BAD_NON_SPECIFIC)  # noqa: e501
    add_value(real_series_connection, real_series_name, 12010, 82.2, QualityCodes.UNCERTAIN_NON_SPECIFIC)  # noqa: e501
    add_value(real_series_connection, real_series_name, 12015, 42.3, QualityCodes.GOOD_LOCAL_OVERRIDE)  # noqa: e501
    # fmt: on
    assert (12010, 82.2, QualityCodes.UNCERTAIN_NON_SPECIFIC) == get_max(
        real_series_connection, real_series_name, 0, 13000, include_bad_values=True
    )


@pytest.fixture
def mostly_uncertain_data(real_series_connection, real_series_name):
    # fmt: off
    add_value(real_series_connection, real_series_name, 12000, 53.2, QualityCodes.GOOD_NON_SPECIFIC)
    add_value(real_series_connection, real_series_name, 12005, 62.1, QualityCodes.UNCERTAIN_NON_SPECIFIC)  # noqa: e501
    add_value(real_series_connection, real_series_name, 12010, 82.2, QualityCodes.BAD_NON_SPECIFIC)  # noqa: e501
    add_value(real_series_connection, real_series_name, 12015, 42.3, QualityCodes.GOOD_LOCAL_OVERRIDE)  # noqa: e501
    # fmt: on


@pytest.mark.usefixtures("mostly_uncertain_data")
def test_get_max__ignore_uncertain_values(real_series_connection, real_series_name):
    assert (12000, 53.2, QualityCodes.GOOD_NON_SPECIFIC) == get_max(
        real_series_connection, real_series_name, 0, 13000
    )


@pytest.mark.usefixtures("mostly_uncertain_data")
def test_get_max__include_uncertain_values(real_series_connection, real_series_name):
    assert (12005, 62.1, QualityCodes.UNCERTAIN_NON_SPECIFIC) == get_max(
        real_series_connection,
        real_series_name,
        0,
        13000,
        include_uncertain_values=True,
    )


@pytest.mark.xfail  # Fix some day
def test_time_weighted_sum__range_outside_of_good_data(
    real_series_connection, real_series_name
):
    add_many_values(
        real_series_connection,
        real_series_name,
        values=[
            (15000, 53.2, QualityCodes.GOOD_NON_SPECIFIC),
            (17000, 42.1, QualityCodes.GOOD_NON_SPECIFIC),
            (18000, 82.2, QualityCodes.GOOD_NON_SPECIFIC),
            (25000, 42.3, QualityCodes.GOOD_NON_SPECIFIC),
        ],
    )
    assert {
        "excluded_points": 0,
        "excluded_seconds": 30,  # Outside and inside
        "included_points": 4,
        "included_seconds": 10,
        "result": 593.2,
        "units": "J",
    } == time_weighted_sum(
        real_series_connection,
        real_series_name,
        TIME_TYPE_EPOCH_MS,
        0,
        40000,
        integral_units="J",
    )


@pytest.mark.xfail  # Fix some day
def test_time_weighted_sum__just_coverage(real_series_connection, real_series_name):
    """
    This data sample exists just for coverage
    """
    add_many_values(
        real_series_connection,
        real_series_name,
        values=[
            (15000, 53.2, QualityCodes.BAD_NON_SPECIFIC),
            (17000, 42.1, QualityCodes.GOOD_NON_SPECIFIC),
            (18000, 82.2, QualityCodes.GOOD_NON_SPECIFIC),
            (25000, 42.3, QualityCodes.GOOD_NON_SPECIFIC),
        ],
    )
    assert {
        "excluded_points": 1,
        "excluded_seconds": 32,
        "included_points": 3,
        "included_seconds": 8,
        "result": 497.9,
        "units": "",
    } == time_weighted_sum(
        real_series_connection, real_series_name, TIME_TYPE_EPOCH_MS, 0, 40000
    )


@pytest.mark.xfail  # Fix some day
def test_time_weighted_sum__integrated_real_world_data_test(
    real_series_connection, real_series_name
):
    """
    Checks real world data stored in Watts using the Series Class
    """
    add_many_values(
        real_series_connection,
        real_series_name,
        values=[
            (1667541147677, 0, QualityCodes.GOOD_NON_SPECIFIC),
            (1667541137751, 3130, QualityCodes.GOOD_NON_SPECIFIC),
            (1667541132965, 3159, QualityCodes.GOOD_NON_SPECIFIC),
            (1667541128173, 3157, QualityCodes.GOOD_NON_SPECIFIC),
            (1667541122913, 3142, QualityCodes.GOOD_NON_SPECIFIC),
            (1667541118403, 3148, QualityCodes.GOOD_NON_SPECIFIC),
            (1667541113936, 3139, QualityCodes.GOOD_NON_SPECIFIC),
            (1667541109254, 3161, QualityCodes.GOOD_NON_SPECIFIC),
            (1667541104736, 3170, QualityCodes.GOOD_NON_SPECIFIC),
            (1667541100131, 3141, QualityCodes.GOOD_NON_SPECIFIC),
            (1667541095411, 3152, QualityCodes.GOOD_NON_SPECIFIC),
            (1667541090923, 3136, QualityCodes.GOOD_NON_SPECIFIC),
            (1667541086353, 3140, QualityCodes.GOOD_NON_SPECIFIC),
            (1667541077091, 3172, QualityCodes.GOOD_NON_SPECIFIC),
        ],
    )
    series = Series.from_db(real_series_name, connection=real_series_connection)
    assert {
        "excluded_points": 0,
        "excluded_seconds": 0.001,
        "included_points": 14,
        "included_seconds": 70.586,
        "result": 206623.628,  # This response is in J
        "units": "J",
    } == series.time_weighted_sum(
        1667541077091,
        1667541147678,
        connection=real_series_connection,
    )
