"""
This file performs component and integration testing for the Series Class.

These tests are not isolated from unit tests.  Ensure other tests are passing
before troubleshooting these.
"""
import pytest

from raik_history.series.constants import (
    DATA_TYPE_STRING,
)
from raik_history.series.json_series import JsonSeries


@pytest.fixture
def sample_json_series(tmp_db_path):
    return JsonSeries(
        series_name="sample_series",
        data_path="",
        data_type=DATA_TYPE_STRING,
        db_path=tmp_db_path,
    )


@pytest.fixture
def mixed_json_like_data():
    return {
        "Hello": "World",
        "A List": [
            {"A": 1, "B": 2},
            {"C": 1, "D": 2},
        ],
    }


@pytest.mark.parametrize(
    "data_path, expected",
    [
        ("Hello", "World"),
        ("A List/0/A", 1),
    ],
)
def test_get_data_path__normal(mixed_json_like_data, data_path, expected):
    assert expected == JsonSeries.get_data_path(data_path, mixed_json_like_data)
