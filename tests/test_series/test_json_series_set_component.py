"""
This file performs component and integration testing for the Series Class.

These tests are not isolated from unit tests.  Ensure other tests are passing
before troubleshooting these.
"""
import pytest

import raik_history.db.connection
from raik_history.db.data_tables import get_all_values_no_page
from raik_history.quality import QualityCodes
from raik_history.series.constants import (
    DATA_TYPE_STRING,
    DATA_TYPE_INT,
)
from raik_history.series.json_series import JsonSeries
from raik_history.series.json_series_set import JsonSeriesSet
from raik_history.stream import stream_multiple_series


@pytest.fixture
def sample_json_series_set(tmp_db_path):
    class J(JsonSeriesSet):
        db_path = tmp_db_path
        TIME_STAMP_PATH = "time"

        SERIES = [
            JsonSeries(
                series_name="sample_series1",
                data_path="A List/0/B",
                data_type=DATA_TYPE_INT,
            ),
            JsonSeries(
                series_name="sample_series2",
                data_path="Hello",
                data_type=DATA_TYPE_STRING,
            ),
        ]

    J.create_series()

    return J


@pytest.fixture
def mixed_json_like_data():
    return {
        "Hello": "World",
        "A List": [
            {"A": 1, "B": 2},
            {"C": 1, "D": 2},
        ],
        "time": 120,
    }


def test_add_value_to_stream__normal(
    tmp_db_path, sample_json_series_set, mixed_json_like_data
):

    with stream_multiple_series(
        tmp_db_path, sample_json_series_set.series_names()
    ) as stream:
        sample_json_series_set.add_values_to_stream(
            stream=stream, raw_data=mixed_json_like_data
        )

    with raik_history.db.connection.db_connection(tmp_db_path) as c:
        data = [
            get_all_values_no_page(c, sample_json_series_set.SERIES[0].name),
            get_all_values_no_page(c, sample_json_series_set.SERIES[1].name),
        ]

    assert (120, 2, QualityCodes.UNCERTAIN_STREAM_START) == data[0][0]
    assert (120, "World", QualityCodes.UNCERTAIN_STREAM_START) == data[1][0]
    assert 2 == len(data[0])
    assert 2 == len(data[1])


def test_add_value_to_db__normal(
    tmp_db_path, sample_json_series_set, mixed_json_like_data
):
    with raik_history.db.connection.db_connection(tmp_db_path) as c:
        sample_json_series_set.add_values_to_db(
            raw_data=mixed_json_like_data, connection=c
        )
        data = [
            get_all_values_no_page(c, sample_json_series_set.SERIES[0].name),
            get_all_values_no_page(c, sample_json_series_set.SERIES[1].name),
        ]

    assert [
        [(120, 2, QualityCodes.UNCERTAIN_NON_SPECIFIC)],
        [(120, "World", QualityCodes.UNCERTAIN_NON_SPECIFIC)],
    ] == data


def test_db_connection_distributes_to_contained_series(
    tmp_db_path, sample_json_series_set, mixed_json_like_data
):
    """
    If a series set is given a connection or db_path, these should be
    passed to the contained series to reduce spurious db_calls at a set or event level.
    """
    for s in sample_json_series_set.SERIES:
        assert s.db_path == sample_json_series_set.db_path
        assert s.db_connection == sample_json_series_set.db_connection
