"""
This file performs component and integration testing for the Series Class.

These tests are not isolated from unit tests.  Ensure other tests are passing
before troubleshooting these.
"""
import pytest

from raik_history.db.connection import db_connection
from raik_history.db.data_tables import get_all_values_no_page
from raik_history.db.series_table import get_series_list
from raik_history.errors import DataExistsError
from raik_history.quality import QualityCodes
from raik_history.series.constants import (
    DATA_TYPE_STRING,
    DATA_TYPE_BOOL,
    DATA_TYPE_INT,
    DATA_TYPE_REAL,
)
from raik_history.series.series import Series
from raik_history.stream import stream_multiple_series


@pytest.fixture
def sample_series(tmp_db_path):
    return Series(
        series_name="sample_series",
        units="units",
        data_type=DATA_TYPE_STRING,
        db_path=tmp_db_path,
    )


def test_sync_series__create_series_expect_series_created(tmp_db, sample_series):
    sample_series.sync_series()

    assert sample_series.units == list(get_series_list(tmp_db))[0]["units"]


def test_sync_series__update_series_expect_series_updated(tmp_db, sample_series):
    sample_series.sync_series()
    sample_series.units = "kw"
    sample_series.sync_series()

    assert sample_series.units == list(get_series_list(tmp_db))[0]["units"]


def test_create_series_in_db__normal_with_new_path(tmp_db_path, tmp_db, sample_series):
    sample_series.sync_series()

    assert sample_series.name == list(get_series_list(tmp_db))[0]["name"]


@pytest.mark.parametrize(
    "data_type, val, expected",
    [
        (DATA_TYPE_STRING, 1, "1"),
        (DATA_TYPE_STRING, None, None),
    ],
)
def test_default_clean_val__normal(sample_series, data_type, val, expected):
    sample_series.data_type = DATA_TYPE_STRING

    assert expected == sample_series.default_clean_val(val)


@pytest.mark.parametrize(
    "data_type, default_clean, val, expected",
    [
        (DATA_TYPE_STRING, True, 1, "1"),
        (DATA_TYPE_STRING, False, 1, 1),
        (DATA_TYPE_STRING, True, None, None),
        (DATA_TYPE_STRING, False, None, None),
    ],
)
def test_clean__normal(sample_series, data_type, default_clean, val, expected):
    sample_series.data_type = DATA_TYPE_STRING
    sample_series.default_clean = default_clean

    assert (100, expected, 0) == sample_series.clean(100, val, 0)


def test_generate_values__normal(sample_series):
    sample_series.data_type = DATA_TYPE_STRING
    sample_series.default_clean = False

    assert [
        (100, "test_generate_values__normal", QualityCodes.UNCERTAIN_NON_SPECIFIC)
    ] == list(sample_series._generate_values(100, "test_generate_values__normal"))


def test_generate_values__normal_list_data(sample_series):
    """
    Not a great test, exists just for coverage
    """
    sample_series.data_type = DATA_TYPE_STRING
    # Monkey Patch the clean function to produce a list
    sample_series.clean = lambda ts, v, q: ([ts] * len(v), v, [q] * len(v))

    assert [
        (100, "test_generate_values__normal0", QualityCodes.UNCERTAIN_NON_SPECIFIC),
        (100, "test_generate_values__normal1", QualityCodes.UNCERTAIN_NON_SPECIFIC),
    ] == list(
        sample_series._generate_values(
            100,
            ["test_generate_values__normal0", "test_generate_values__normal1"],
        )
    )


def test_add_value_to_stream__normal(tmp_db_path, sample_series):
    sample_series.sync_series()

    with stream_multiple_series(tmp_db_path, [sample_series.name]) as stream:
        sample_series.add_value_to_stream(stream, "35.0", 12000)

    with db_connection(tmp_db_path) as c:
        data = get_all_values_no_page(c, sample_series.name)

    assert (12000, "35.0", QualityCodes.UNCERTAIN_STREAM_START) == data[0]
    assert 2 == len(data)
    assert QualityCodes.BAD_STREAM_END == data[1][-1]


def test_add_value_to_db__normal(tmp_db_path, sample_series):
    sample_series.sync_series()

    with db_connection(tmp_db_path) as c:
        sample_series.add_value_to_db("A", 12000, connection=c)
        data = get_all_values_no_page(c, sample_series.name)

    assert [(12000, "A", QualityCodes.UNCERTAIN_NON_SPECIFIC)] == data


def test_add_value_to_db__normal_without_connection(tmp_db_path, sample_series):
    sample_series.sync_series()
    sample_series.add_value_to_db("A", 12000)

    with db_connection(tmp_db_path) as c:
        data = get_all_values_no_page(c, sample_series.name)

    assert [(12000, "A", QualityCodes.UNCERTAIN_NON_SPECIFIC)] == data


def test_add_value_to_db__duplicate_time_raises_exception(sample_series):
    sample_series.sync_series()
    sample_series.add_value_to_db("A", 12000)

    with pytest.raises(DataExistsError):
        sample_series.add_value_to_db("B", 12000)


def test_add_value_to_db__duplicate_value_no_exception(sample_series):
    sample_series.sync_series()
    sample_series.add_value_to_db("A", 12000)
    sample_series.add_value_to_db("A", 12000)


@pytest.mark.parametrize(
    "data_type, none_value",
    [
        (DATA_TYPE_BOOL, False),
        (DATA_TYPE_STRING, ""),
        (DATA_TYPE_INT, 0),
        (DATA_TYPE_REAL, 0.0),
    ],
)
def test_strict_value__for_coverage(sample_series, data_type, none_value):
    sample_series.data_type = data_type

    assert none_value == sample_series.strict_value()


def test_strict_value__string_value_stored(sample_series):
    sample_series.data_type = DATA_TYPE_STRING
    sample_series.default_clean = False
    list(sample_series._generate_values(100, "test_strict_value__string_value_stored"))

    assert "test_strict_value__string_value_stored" == sample_series.strict_value()
