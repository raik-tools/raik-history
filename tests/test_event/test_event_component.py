import pytest

from raik_history.db.event_tables import get_event
from raik_history.event import Event
from raik_history.quality import QualityCodes
from raik_history.series.constants import TIME_STAMP_TYPE_REMOTE, TIME_TYPE_EPOCH_MS


@pytest.fixture
def trial_event_class(tmp_db):
    class TrialEvent(Event):

        NAME = "TrialEvent"
        DESCRIPTION = "Used for testing"
        db_connection = tmp_db

    return TrialEvent


def test_create__normal(trial_event_class, tmp_db):
    """
    A simple test that checks the creation and retrieval of an event
    """

    test_ts = 500

    e = trial_event_class(test_ts)
    e.save()

    with tmp_db:
        ret = get_event(tmp_db, 1, test_ts)
    assert {
        "event_type_id": 1,
        "name": trial_event_class.NAME,
        "description": trial_event_class.DESCRIPTION,
        "time_stamp_type": TIME_STAMP_TYPE_REMOTE,
        "time_type": TIME_TYPE_EPOCH_MS,
        "start_ts": 500,
        "end_ts": None,
        "comments": None,
        "quality": QualityCodes.UNCERTAIN_NON_SPECIFIC,
    } == ret


def test_create_double_save__expect_noop(trial_event_class):
    test_ts = 500
    e = trial_event_class(test_ts)
    e.save()
    e.save()


def test_multiple_instances_refer_to_the_same_data(trial_event_class):
    test_ts = 500
    test_comment = "test_comment"
    trial_event_class(test_ts, comments=test_comment).save()
    e = trial_event_class(test_ts)
    assert test_comment == e.comments


def test_create_and_update_event__expect_change(trial_event_class):
    test_ts = 500
    test_comment = "test_comment"
    e = trial_event_class(test_ts)
    e.save()

    e.comments = test_comment
    e.save()
    assert test_comment == trial_event_class(test_ts).comments


def test_close_event__normal(trial_event_class):
    test_ts = 500
    end_ts = 510
    e = trial_event_class(test_ts)
    e.save()
    e.close_event(end_ts)
    assert end_ts == trial_event_class(test_ts).end_ts


def test_get_events__get_good_only(trial_event_class):
    trial_event_class(start_ts=500, end_ts=510, comments="1").save()
    trial_event_class(
        start_ts=510, end_ts=520, comments="2", quality=QualityCodes.GOOD_NON_SPECIFIC
    ).save()
    data = [x.raw_data for x in trial_event_class.get_events(only_good_quality=True)]
    assert [
        (1, 510, 520, "2", QualityCodes.GOOD_NON_SPECIFIC),
    ] == data


@pytest.fixture
def event_collection(trial_event_class):
    trial_event_class(start_ts=500, end_ts=510, comments="11").save()
    trial_event_class(start_ts=510, end_ts=520, comments="21").save()
    trial_event_class(start_ts=520, end_ts=530, comments="32").save()
    trial_event_class(start_ts=530, end_ts=540, comments="42").save()
    trial_event_class(start_ts=540, end_ts=550, comments="53").save()


def test_get_events__get_all(trial_event_class, event_collection):

    data = [x.raw_data for x in trial_event_class.get_events(only_good_quality=False)]
    assert [
        (1, 500, 510, "11", QualityCodes.UNCERTAIN_NON_SPECIFIC),
        (1, 510, 520, "21", QualityCodes.UNCERTAIN_NON_SPECIFIC),
        (1, 520, 530, "32", QualityCodes.UNCERTAIN_NON_SPECIFIC),
        (1, 530, 540, "42", QualityCodes.UNCERTAIN_NON_SPECIFIC),
        (1, 540, 550, "53", QualityCodes.UNCERTAIN_NON_SPECIFIC),
    ] == data


def test_get_events__comments__contains(trial_event_class, event_collection):

    data = [
        x.raw_data
        for x in trial_event_class.get_events(
            comments__contains="2", only_good_quality=False
        )
    ]
    assert [
        (1, 510, 520, "21", QualityCodes.UNCERTAIN_NON_SPECIFIC),
        (1, 520, 530, "32", QualityCodes.UNCERTAIN_NON_SPECIFIC),
        (1, 530, 540, "42", QualityCodes.UNCERTAIN_NON_SPECIFIC),
    ] == data


def test_get_events__get_start(trial_event_class, event_collection):

    data = [
        x.raw_data
        for x in trial_event_class.get_events(count=1, only_good_quality=False)
    ]
    assert [
        (1, 500, 510, "11", QualityCodes.UNCERTAIN_NON_SPECIFIC),
    ] == data


def test_get_events__get_last(trial_event_class, event_collection):

    data = [
        x.raw_data
        for x in trial_event_class.get_events(
            count=1, chronological_order=False, only_good_quality=False
        )
    ]
    assert [
        (1, 540, 550, "53", QualityCodes.UNCERTAIN_NON_SPECIFIC),
    ] == data


def test_get_events__get_middle(trial_event_class, event_collection):

    data = [
        x.raw_data
        for x in trial_event_class.get_events(
            start_ts__gte=510, start_ts__lt=540, only_good_quality=False
        )
    ]
    assert [
        (1, 510, 520, "21", QualityCodes.UNCERTAIN_NON_SPECIFIC),
        (1, 520, 530, "32", QualityCodes.UNCERTAIN_NON_SPECIFIC),
        (1, 530, 540, "42", QualityCodes.UNCERTAIN_NON_SPECIFIC),
    ] == data
