"""
This module focuses on extensions of the event component through inheritance
"""
import pytest

from raik_history.event import Event
from raik_history.quality import QualityCodes


@pytest.fixture
def extended_event_class(tmp_db):
    # noinspection PyAbstractClass
    class ExtendedEvent(Event):

        NAME = "ExtendedEvent"
        DESCRIPTION = "Used for testing"
        db_connection = tmp_db

        columns = ["event_duration", "custom_fx"]

        def custom_fx(self) -> int:
            return 3 * self.start_ts

    return ExtendedEvent


@pytest.fixture
def event_collection(extended_event_class):
    extended_event_class(start_ts=500, end_ts=510, comments="11").save()
    extended_event_class(start_ts=510, end_ts=520, comments="21").save()
    extended_event_class(start_ts=520, end_ts=530, comments="32").save()
    extended_event_class(start_ts=530, end_ts=540, comments="42").save()
    extended_event_class(start_ts=540, end_ts=550, comments="53").save()


def test_columns_dict(tmp_db):
    # noinspection PyAbstractClass
    class TestEvent(Event):

        NAME = "TestEvent"
        DESCRIPTION = "Used for testing"
        db_connection = tmp_db

    e = TestEvent(start_ts=500, end_ts=510, comments="11")
    e.save()
    assert {"event_duration": 10} == e.columns_dict()


def test_annotated_absolute_error(tmp_db):
    # noinspection PyAbstractClass
    class TestEvent(Event):

        NAME = "TestEvent"
        DESCRIPTION = "Used for testing"
        db_connection = tmp_db

    e = TestEvent(start_ts=500, end_ts=510, comments="11")
    assert e.duration_s.absolute_error == "0.0005"


def test_columns_dict_custom_fx(extended_event_class):
    e = extended_event_class(start_ts=500, end_ts=510, comments="11")
    e.save()
    assert {"custom_fx": 1500, "event_duration": 10} == e.columns_dict()


def test_get_data_no_related_series__expect_exception(
    extended_event_class, event_collection
):
    e = extended_event_class(start_ts=500, end_ts=510, comments="11")
    with pytest.raises(NotImplementedError):
        e.get_data()


def test_get_events__get_filtered__no_match(extended_event_class, event_collection):

    data = [
        x.raw_data
        for x in extended_event_class.get_events(
            only_good_quality=False, custom_fx=1531
        )
    ]
    assert [] == data


def test_get_events__get_filtered__one_match(extended_event_class, event_collection):

    data = [
        x.raw_data
        for x in extended_event_class.get_events(
            only_good_quality=False, custom_fx=1530
        )
    ]
    assert [
        (1, 510, 520, "21", QualityCodes.UNCERTAIN_NON_SPECIFIC),
    ] == data


def test_get_events__get_filtered__comparison(extended_event_class, event_collection):

    data = [
        x.raw_data
        for x in extended_event_class.get_events(
            only_good_quality=False, custom_fx__gt=1530
        )
    ]
    assert [
        (1, 520, 530, "32", 64),
        (1, 530, 540, "42", 64),
        (1, 540, 550, "53", 64),
    ] == data
