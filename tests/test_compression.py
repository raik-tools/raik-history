import pytest

from raik_history.compression import ChangeOnlyCompressor
from raik_history.series.constants import DataTuple
from tests import test_compression_test_data


class TempCompressor(ChangeOnlyCompressor):
    def __init__(self, repeat_period: int = 7200):
        super().__init__(repeat_period=repeat_period)
        self.data = []

    def on_new_value(self, val: DataTuple):
        self.data.append(val)

    def on_update_value(self, val: DataTuple):
        self.data[-1] = val


def test_change_only_compression__one_value__expect_value_stored():
    c = TempCompressor()
    c.add_value((10000, 4.0, 192))
    assert [(10000, 4.0, 192)] == c.data


def test_change_only_compression__two_identical_points__expect_both_stored():
    c = TempCompressor(repeat_period=10000)
    c.add_value((10000, 4.0, 192))
    c.add_value((12000, 4.0, 192))

    assert [(10000, 4.0, 192), (12000, 4.0, 192)] == c.data


def test_change_only_compression__unchanging_value_within_repeat__expect_ends_only():
    c = TempCompressor(repeat_period=10000)
    c.add_value((10000, 4.0, 192))
    c.add_value((10500, 4.0, 192))
    c.add_value((11000, 4.0, 192))

    assert [(10000, 4.0, 192), (11000, 4.0, 192)] == c.data


def test_change_only_compression__unchanging_value_after_repeat__expect_all_values():
    c = TempCompressor(repeat_period=1)
    c.add_value((10000, 4.0, 192))
    c.add_value((12000, 4.0, 192))
    c.add_value((14000, 4.0, 192))

    assert [(10000, 4.0, 192), (12000, 4.0, 192), (14000, 4.0, 192)] == c.data


@pytest.mark.parametrize(
    "recent_values, compressed_values, repeat_period",
    test_compression_test_data.test_change_only_compression,
)
def test_change_only_compression(recent_values, compressed_values, repeat_period):
    """
    This series of tests demonstrate a series of scenarios
    """
    c = TempCompressor(repeat_period=repeat_period)
    for v in recent_values:
        c.add_value(v)

    assert compressed_values == c.data
