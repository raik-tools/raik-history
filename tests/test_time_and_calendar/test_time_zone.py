# Copyright: (c) 2022, Gary Thompson <coding@garythompson.au>
# SPDX-License-Identifier: MIT
import datetime

from raik_history.time_and_calendar import (
    create_time_aware,
    get_current_timezone,
    epoch_ms_to_datetime,
    is_aware,
)


def test_get_current_timezone__from_env(mock_os_env__time_zone):
    assert "Australia/Brisbane" in str(get_current_timezone())


def test_get_current_timezone__integrated_from_env(mock_os_env__time_zone):
    assert "+10:00" in str(epoch_ms_to_datetime(1000000000))


def test_get_current_timezone__from_system(mock_time_timezone__utc10):
    assert "UTC+01:00" in str(get_current_timezone())


def test_get_current_timezone__integrated_from_system(mock_time_timezone__utc10):
    assert "+01:00" in str(epoch_ms_to_datetime(1000000000))


def test_is_aware__true():
    assert is_aware(create_time_aware(2021, 12, 28, 8, 0, 0, 0, "UTC")) is True


def test_is_aware__false():
    assert is_aware(datetime.datetime.now()) is False
