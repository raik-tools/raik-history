# Copyright: (c) 2022, Gary Thompson <coding@garythompson.au>
# SPDX-License-Identifier: MIT
from zoneinfo import ZoneInfo

from raik_history.time_and_calendar import (
    create_time_aware,
    now_datetime,
    is_aware,
    epoch_ms_to_datetime,
    datetime_to_string,
    iso_string_to_datetime,
    create_local_time,
)


def test_create_time_aware__normal():
    dt = create_time_aware(2021, 12, 28, 8, 0, 0, 0, "Australia/Brisbane")
    assert "2021-12-28T08:00:00+10:00" == dt.isoformat()


def test_time_conversion__spot_check():
    """
    This is just to verify time conversion in python 3.9 which is where
    Django has dropped pytz in favor of the new ZoneInfo model.  Previously,
    time conversions would be unstable.
    """
    dt = create_time_aware(2021, 12, 28, 8, 0, 0, 0, "UTC")
    assert "2021-12-28T08:00:00+00:00" == dt.isoformat()
    assert (
        "2021-12-28T18:00:00+10:00"
        == dt.astimezone(ZoneInfo("Australia/Brisbane")).isoformat()
    )


def test_now_datetime__normal():
    """
    If this passes being aware then it is a datetime object and is time aware.
    """
    v = now_datetime()
    assert is_aware(v) is True


def test_epoch_ms_to_datetime__normal():
    """
    Snapshot tests of epoch_ms_to_datetime for coverage (low value test)
    """
    v = epoch_ms_to_datetime(100000000000)
    assert "1973-03-03 19:46:40+10:00" == str(v)


def test_datetime_to_string__normal(mock_time_timezone__utc10):
    """
    Snapshot tests of epoch_ms_to_datetime for coverage (low value test)
    """
    v = epoch_ms_to_datetime(100000000000)
    assert "1973-03-03 10:46:40 UTC+01:00" == datetime_to_string(v)


def test_iso_string_to_datetime__normal_short():
    v = iso_string_to_datetime("2022-08-23T12:26:12")
    assert create_time_aware(2022, 8, 23, 12, 26, 12, 0) == v


def test_iso_string_to_datetime__normal_decimal_seconds():
    v = iso_string_to_datetime("2022-08-23T12:26:12.5")
    assert create_time_aware(2022, 8, 23, 12, 26, 12, 500000) == v


def test_iso_string_to_datetime__normal_zulu():
    v = iso_string_to_datetime("2022-08-23T12:26:12Z")
    assert create_time_aware(2022, 8, 23, 12, 26, 12, 0) == v


def test_iso_string_to_datetime__normal_utc_offset():
    v = iso_string_to_datetime("2022-08-23T12:26:12+0000")
    assert create_time_aware(2022, 8, 23, 12, 26, 12, 0) == v


def test_iso_string_to_datetime__normal_utc_offset_with_colon():
    v = iso_string_to_datetime("2022-08-23T12:26:12+00:00")
    assert create_time_aware(2022, 8, 23, 12, 26, 12, 0) == v


def test_iso_string_to_datetime__normal_brisbane_offset():
    v = iso_string_to_datetime("2022-08-23T12:26:12.5+1000")
    assert create_time_aware(2022, 8, 23, 12, 26, 12, 500000, "Australia/Brisbane") == v


def test_iso_string_to_datetime__only_date_part():
    """
    If only a date is provided, it is assumed to be a local time (a calendar reference)
    :return:
    """
    v = iso_string_to_datetime("2022-08-23")
    assert create_local_time(2022, 8, 23) == v
