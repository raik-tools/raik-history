from unittest import mock

import pytest

from raik_history.time_and_calendar import (
    get_current_timezone,
)


@pytest.fixture
def mock_os_env__time_zone():
    with mock.patch("os.environ") as m:
        m.get = mock.MagicMock(return_value="Australia/Brisbane")
        yield


@pytest.fixture
def mock_time_timezone__utc10():
    with mock.patch("time.localtime") as m:
        m.return_value = mock.MagicMock()
        # noinspection SpellCheckingInspection
        m.return_value.tm_gmtoff = 3600
        yield


@pytest.fixture(autouse=True)
def clear_time_zone_caches():
    get_current_timezone.cache_clear()


@pytest.fixture
def mock_current_time():
    with mock.patch("time.time", return_value=17000000000):
        with mock.patch("time.time_ns", return_value=17000000000 * 10 ** 9):
            yield
