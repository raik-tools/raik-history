# Copyright: (c) 2022, Gary Thompson <coding@garythompson.au>
# SPDX-License-Identifier: MIT
import datetime

import pytest

from raik_history.time_and_calendar.epoch_ms import (
    now_epoch_ms,
    datetime_to_epoch_ms,
    iso_string_to_epoch_ms,
    seconds_apart,
)
from raik_history.time_and_calendar.date_time import create_time_aware


def test_now_epoch_ms__coverage(mock_current_time):
    """
    A low value snapshot test just for coverage
    """
    assert 17000000000000 == now_epoch_ms()


def test_datetime_to_epoch_ms__normal(mock_current_time):
    dt = create_time_aware(2022, 2, 19, 22, 0, 0, 0, "Australia/Brisbane")
    assert 1645272000000 == datetime_to_epoch_ms(dt)


def test_datetime_to_epoch_ms__error_on_naive(mock_current_time):
    dt = datetime.datetime(2022, 2, 19, 22, 0, 0)
    with pytest.raises(AssertionError):
        datetime_to_epoch_ms(dt)


def test_iso_string_to_epoch_ms__snapshot():
    assert 1661257560000 == iso_string_to_epoch_ms("2022-08-23T12:26:00.0")


def test_seconds_apart__normal():
    assert 3.0 == seconds_apart(5000, 2000)
    assert 3.5 == seconds_apart(5000, 1500)
