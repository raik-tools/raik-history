# Copyright: (c) 2022, Gary Thompson <coding@garythompson.au>
# SPDX-License-Identifier: MIT
import datetime

import pytest

from raik_history.time_and_calendar import (
    create_time_aware,
    now_epoch_100ns,
    datetime_to_epoch_100ns,
)


def test_now_epoch_100ns__coverage(mock_current_time):
    """
    A low value snapshot test just for coverage
    """
    assert 170000000000000000 == now_epoch_100ns()


def test_datetime_to_epoch_100ns__normal(mock_current_time):
    dt = create_time_aware(2022, 2, 19, 22, 0, 0, 0, "Australia/Brisbane")
    assert 16452720000000000 == datetime_to_epoch_100ns(dt)


def test_datetime_to_epoch_100ns__error_on_naive(mock_current_time):
    dt = datetime.datetime(2022, 2, 19, 22, 0, 0)
    with pytest.raises(AssertionError):
        datetime_to_epoch_100ns(dt)
