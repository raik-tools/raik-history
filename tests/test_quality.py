from raik_history.quality import (
    quality_is_good,
    QualityCodes,
    quality_is_uncertain,
    quality_is_bad,
)


def test_quality_is_good__expect_success():
    assert quality_is_good(QualityCodes.GOOD_NON_SPECIFIC)
    assert quality_is_good(QualityCodes.GOOD_LOCAL_OVERRIDE_HIGH_LIMITED)


def test_quality_is_good__expect_failure():
    assert not quality_is_good(QualityCodes.BAD_DEVICE_FAILURE)
    assert not quality_is_good(QualityCodes.BAD_NON_SPECIFIC)
    assert not quality_is_good(QualityCodes.UNCERTAIN_NON_SPECIFIC)
    assert not quality_is_good(QualityCodes.UNCERTAIN_EU_EXCEEDED)
    assert not quality_is_good(QualityCodes.UNCERTAIN_STREAM_START)
    assert not quality_is_good(QualityCodes.BAD_STREAM_END)


def test_quality_is_uncertain__expect_success():
    assert quality_is_uncertain(QualityCodes.UNCERTAIN_NON_SPECIFIC)
    assert quality_is_uncertain(QualityCodes.UNCERTAIN_LAST_USABLE)
    assert quality_is_uncertain(QualityCodes.UNCERTAIN_STREAM_START)


def test_quality_is_uncertain__expect_failure():
    assert not quality_is_uncertain(QualityCodes.GOOD_NON_SPECIFIC)
    assert not quality_is_uncertain(QualityCodes.GOOD_STREAM_START)
    assert not quality_is_uncertain(QualityCodes.GOOD_LOCAL_OVERRIDE)
    assert not quality_is_uncertain(QualityCodes.BAD_NON_SPECIFIC)
    assert not quality_is_uncertain(QualityCodes.BAD_DEVICE_FAILURE)
    assert not quality_is_uncertain(QualityCodes.BAD_STREAM_END)
    assert not quality_is_uncertain(QualityCodes.GOOD_STREAM_START)


def test_quality_is_bad__expect_success():
    assert quality_is_bad(QualityCodes.BAD_NON_SPECIFIC)
    assert quality_is_bad(QualityCodes.BAD_SENSOR_FAILURE)


def test_quality_is_bad__expect_failure():
    assert not quality_is_bad(QualityCodes.GOOD_NON_SPECIFIC)
    assert not quality_is_bad(QualityCodes.GOOD_STREAM_START)
    assert not quality_is_bad(QualityCodes.GOOD_LOCAL_OVERRIDE)
    assert not quality_is_bad(QualityCodes.UNCERTAIN_NON_SPECIFIC)
    assert not quality_is_bad(QualityCodes.UNCERTAIN_EU_EXCEEDED_CONSTANT)
