import pytest

from raik_history.db.connection import db_connection
from raik_history.db.data_tables import (
    get_all_values_no_page,
    add_value,
    get_latest_value,
)
from raik_history.db.series_table import SeriesInterface, add_series
from raik_history.quality import QualityCodes
from raik_history.series.constants import (
    DATA_TYPE_REAL,
    TIME_TYPE_EPOCH_MS,
    TIME_STAMP_TYPE_REMOTE,
)
from raik_history.stream import StreamInterface, stream_multiple_series


@pytest.fixture
def stream_interface_one_real_series(real_series_connection, real_series_name):
    return StreamInterface(
        connection=real_series_connection, series_list=[real_series_name]
    )


def test_stream_interface_add_value__empty_series(
    stream_interface_one_real_series, real_series_name
):
    stream_interface_one_real_series.add_value(
        real_series_name, 500, 50.0, quality=QualityCodes.GOOD_NON_SPECIFIC
    )
    assert [(500, 50.0, QualityCodes.GOOD_STREAM_START)] == get_all_values_no_page(
        stream_interface_one_real_series.connection, real_series_name
    )


def test_stream_interface_add_value__compression_check(
    stream_interface_one_real_series, real_series_name
):
    """
    Change detection will not occur on the first value as it will be the
    beginning of the stream.
    """
    stream_interface_one_real_series.add_value(
        real_series_name, 500, 50.0, quality=QualityCodes.GOOD_NON_SPECIFIC
    )
    stream_interface_one_real_series.add_value(
        real_series_name, 510, 50.0, quality=QualityCodes.GOOD_NON_SPECIFIC
    )
    stream_interface_one_real_series.add_value(
        real_series_name, 520, 50.0, quality=QualityCodes.GOOD_NON_SPECIFIC
    )
    stream_interface_one_real_series.add_value(
        real_series_name, 530, 50.0, quality=QualityCodes.GOOD_NON_SPECIFIC
    )
    stream_interface_one_real_series.add_value(
        real_series_name, 540, 50.0, quality=QualityCodes.GOOD_NON_SPECIFIC
    )
    stream_interface_one_real_series.add_value(
        real_series_name, 550, 50.0, quality=QualityCodes.GOOD_NON_SPECIFIC
    )
    assert [
        (500, 50.0, QualityCodes.GOOD_STREAM_START),
        (510, 50.0, QualityCodes.GOOD_NON_SPECIFIC),
        (550, 50.0, QualityCodes.GOOD_NON_SPECIFIC),
    ] == get_all_values_no_page(
        stream_interface_one_real_series.connection, real_series_name
    )


def test_stream_interface_add_value__only_apply_stream_start_on_first(
    stream_interface_one_real_series, real_series_name
):
    stream_interface_one_real_series.add_value(
        real_series_name, 500, 50.0, quality=QualityCodes.GOOD_NON_SPECIFIC
    )
    assert [(500, 50.0, QualityCodes.GOOD_STREAM_START)] == get_all_values_no_page(
        stream_interface_one_real_series.connection, real_series_name
    )

    stream_interface_one_real_series.add_value(
        real_series_name, 510, 51.0, quality=QualityCodes.GOOD_NON_SPECIFIC
    )
    assert [
        (500, 50.0, QualityCodes.GOOD_STREAM_START),
        (510, 51.0, QualityCodes.GOOD_NON_SPECIFIC),
    ] == get_all_values_no_page(
        stream_interface_one_real_series.connection, real_series_name
    )


@pytest.fixture
def stream_interface_one_real_series_with_null_data(
    real_series_connection, real_series_name
):
    add_value(
        real_series_connection,
        real_series_name,
        499,
        None,
        quality=QualityCodes.BAD_NON_SPECIFIC,
    )

    return StreamInterface(
        connection=real_series_connection, series_list=[real_series_name]
    )


def test_stream_interface_add_value__null_series_duplicate_value__expect_no_change(
    stream_interface_one_real_series_with_null_data, real_series_name
):
    stream_interface_one_real_series_with_null_data.add_value(
        real_series_name, 499, None, quality=QualityCodes.BAD_NON_SPECIFIC
    )
    assert [(499, None, QualityCodes.BAD_NON_SPECIFIC)] == get_all_values_no_page(
        stream_interface_one_real_series_with_null_data.connection, real_series_name
    )


@pytest.fixture
def stream_interface_one_real_series_with_data(
    real_series_connection, real_series_name
):
    add_value(real_series_connection, real_series_name, 499, 42.1)

    return StreamInterface(
        connection=real_series_connection, series_list=[real_series_name]
    )


def test_stream_interface_add_value__non_empty_series(
    stream_interface_one_real_series_with_data, real_series_name
):
    stream_interface_one_real_series_with_data.add_value(
        real_series_name, 500, 50.0, quality=QualityCodes.GOOD_NON_SPECIFIC
    )
    assert [
        (499, 42.1, QualityCodes.UNCERTAIN_NON_SPECIFIC),
        (500, 50.0, QualityCodes.GOOD_STREAM_START),
    ] == get_all_values_no_page(
        stream_interface_one_real_series_with_data.connection, real_series_name
    )


def test_stream_interface_add_existing_init_value__no_action(
    stream_interface_one_real_series_with_data, real_series_name
):
    stream_interface_one_real_series_with_data.add_value(
        real_series_name, 499, 42.1, quality=QualityCodes.GOOD_NON_SPECIFIC
    )
    assert [
        (499, 42.1, QualityCodes.UNCERTAIN_NON_SPECIFIC),
    ] == get_all_values_no_page(
        stream_interface_one_real_series_with_data.connection, real_series_name
    )


def test_stream_interface_add_value__close_stream(
    stream_interface_one_real_series_with_data, real_series_name
):
    stream_interface_one_real_series_with_data.close()
    latest_value = get_latest_value(
        stream_interface_one_real_series_with_data.connection, real_series_name
    )
    assert latest_value[1] is None
    assert QualityCodes.BAD_STREAM_END == latest_value[2]


def test_stream_multiple_series__normal(tmp_db_path, real_series_name):
    with db_connection(tmp_db_path) as c:
        series_data = SeriesInterface(
            name=real_series_name,
            data_type=DATA_TYPE_REAL,
            time_type=TIME_TYPE_EPOCH_MS,
            time_stamp_type=TIME_STAMP_TYPE_REMOTE,
            units="",
            differential_units="",
            integral_units="",
            storage_configuration={},
            retrieval_configuration={},
        )
        add_series(c, series_data)

    with stream_multiple_series(tmp_db_path, [real_series_name]) as i:
        i.add_value(real_series_name, 500, 50.0, quality=QualityCodes.GOOD_NON_SPECIFIC)
        assert (500, 50.0, QualityCodes.GOOD_STREAM_START) == get_latest_value(
            i.connection, real_series_name
        )

    with db_connection(tmp_db_path) as c:
        latest_value = get_latest_value(c, real_series_name)
        assert latest_value[1] is None
        assert QualityCodes.BAD_STREAM_END == latest_value[2]
