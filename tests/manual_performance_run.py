# Copyright: (c) 2022, Gary Thompson <coding@garythompson.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import os
import random
import time
import pathlib

from raik_history import reset_caches
from raik_history.db.connection import db_connection
from raik_history.db.data_tables import add_value
from raik_history.db.series_table import add_series, SeriesInterface
from raik_history.series.constants import (
    DATA_TYPE_REAL,
    TIME_TYPE_EPOCH_MS,
    TIME_STAMP_TYPE_REMOTE,
)
from raik_history.time_and_calendar import now_epoch_ms

if __name__ == "__main__":

    def add_data_to_all_series():
        """
        A rough performance test looking at the impact of database shape
        :return:
        """
        data_dir = pathlib.Path(__file__).parent.parent / "data"
        db_path = data_dir / "trial.db"

        for tables, rows_per_table in [
            (10, 10),
            (10, 100),
            (10, 1000),
            (1000, 10),
        ]:
            reset_caches()
            if db_path.exists():
                os.remove(db_path)
            t1 = time.time()
            with db_connection(db_path) as c:
                series_names = []
                for i in range(tables):
                    name = f"t{i}"
                    series_names.append(name)
                    add_series(
                        c,
                        SeriesInterface(
                            name=name,
                            data_type=DATA_TYPE_REAL,
                            time_type=TIME_TYPE_EPOCH_MS,
                            time_stamp_type=TIME_STAMP_TYPE_REMOTE,
                            units="",
                            differential_units="",
                            integral_units="",
                            storage_configuration={},
                            retrieval_configuration={},
                        ),
                    )
                for _ in range(rows_per_table):
                    for series_name in series_names:
                        add_value(c, series_name, now_epoch_ms(), random.random())
            t2 = time.time()
            total_data_points = tables * rows_per_table
            data_size = db_path.stat().st_size
            duration = t2 - t1
            print(
                f"add_data_to_all_series: "
                f"{total_data_points} points in {duration}s or about "
                f"{duration / total_data_points} per point.  "
                f"{data_size/ 1024**2} MiB total or about "
                f"{data_size / total_data_points} B per point."
            )

    add_data_to_all_series()
