import pytest

from raik_history.db.connection import db_connection


def test_db_connection__normal_coverage(tmp_path, caplog):
    """
    This is a basic test to establish coverage for a normal
    path.  It does not add a lot of value, but if any issues
    occur later on it can scaffold additional tests.
    """
    db_path = tmp_path / "db.sqlite"
    with db_connection(db_path) as c:
        assert 1 == c.total_changes
        assert db_path.exists()

    assert not caplog.records, "No errors are expected to have been logged in this test"


def test_db_connection__exception_coverage(tmp_path, caplog):
    """
    This is a basic test to establish coverage for an exception
    path.  It does not add a lot of value, but if any issues
    occur later on it can scaffold additional tests.
    """
    db_path = tmp_path / "db.sqlite"
    with pytest.raises(Exception):
        with db_connection(db_path):
            raise Exception("This is an exception")

    assert "This is an exception" in caplog.records[0].message
