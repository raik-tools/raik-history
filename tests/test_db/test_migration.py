import sqlite3

import pytest

from raik_history.db.connection import db_connection
from raik_history.db.data_tables import (
    add_value,
    get_latest_value,
    get_value_at_time,
)

# noinspection PyProtectedMember
from raik_history.db.migration import (
    data_table_name_for_series,
    _execute_data_tables_migrations,
    table_list,
    META_TABLE_NAME,
    SERIES_TABLE_NAME,
    EVENT_TYPE_TABLE_NAME,
    EVENT_TABLE_NAME,
    MIGRATION_VERSION,
    get_migration_number,
    migrate,
    set_migration_number,
    create_data_series_table,
    set_meta_value,
    get_meta_value,
)
from raik_history.db.series_table import SeriesInterface, add_series
from raik_history.series.constants import (
    DATA_TYPE_REAL,
    TIME_TYPE_EPOCH_MS,
    TIME_STAMP_TYPE_REMOTE,
)


@pytest.fixture
def series_interface():
    return SeriesInterface(
        name="trial_1234",
        data_type=DATA_TYPE_REAL,
        time_type=TIME_TYPE_EPOCH_MS,
        time_stamp_type=TIME_STAMP_TYPE_REMOTE,
        units="",
        differential_units="",
        integral_units="",
        storage_configuration={},
        retrieval_configuration={},
    )


@pytest.fixture
def tmp_db_with_real_series(tmp_path, series_interface):
    db_path = tmp_path / "db.sqlite"
    with db_connection(db_path) as c:
        add_series(c, series_interface)
        yield c


def test_set_meta_value__round_trip_new_value(tmp_path):
    """
    This function tests the set and retrieval of meta values
    """
    db_path = tmp_path / "db.sqlite"
    with db_connection(db_path) as c:
        set_meta_value(c, "Trial 1", "Value 1")

        assert "Value 1" == get_meta_value(c, "Trial 1")


def test_set_meta_value__round_trip_update_value(tmp_path):
    """
    This function tests the set and retrieval of meta values
    """
    db_path = tmp_path / "db.sqlite"
    with db_connection(db_path) as c:
        set_meta_value(c, "Trial 1", "Value 1")
        set_meta_value(c, "Trial 1", "Value 2")

        assert "Value 2" == get_meta_value(c, "Trial 1")


def test_set_meta_value__round_trip_no_change_in_value(tmp_path):
    """
    This function tests the set and retrieval of meta values
    """
    db_path = tmp_path / "db.sqlite"
    with db_connection(db_path) as c:
        set_meta_value(c, "Trial 1", "Value 1")
        set_meta_value(c, "Trial 1", "Value 1")

        assert "Value 1" == get_meta_value(c, "Trial 1")


def test_migrate__new_db(tmp_path, caplog):
    """
    This is an integration test that checks a new DB contains the appropriate
    tables.  As a side effect, this also tests:

    * get_meta_value,
    * set_meta_value,
    * get_migration_number,
    * set_migration_number,
    * _execute_meta_migrations,
    * _execute_series_migrations
    * table_list

    Ideally isolated tests will verify the functions above, but the decision
    here is to favour pragmatism.
    """
    db_path = tmp_path / "db.sqlite"
    with db_connection(db_path) as c:
        tables = table_list(c)
        assert {
            META_TABLE_NAME,
            SERIES_TABLE_NAME,
            EVENT_TYPE_TABLE_NAME,
            EVENT_TABLE_NAME,
            "sqlite_sequence",
        } == set(tables)
        assert MIGRATION_VERSION == get_migration_number(c)

    assert not caplog.records, "No errors are expected to have been logged in this test"


def test_migrate__multiple_migrate_request_expect_no_difference(tmp_path, caplog):
    """
    This is an integration test that checks a new DB with multiple migration
    requests at the same version will have no impact
    """
    db_path = tmp_path / "db.sqlite"
    with db_connection(db_path) as c:
        migrate(c)
        assert MIGRATION_VERSION == get_migration_number(c)
        migrate(c)
        assert MIGRATION_VERSION == get_migration_number(c)

    assert not caplog.records, "No errors are expected to have been logged in this test"


def test_set_migration_number__normal(tmp_path, caplog):
    """
    A quick check of set_migration_number paths.  This also tests
    set_meta_value, get_meta_value and get_migration_number
    """
    db_path = tmp_path / "db.sqlite"
    with db_connection(db_path) as c:
        old_v = MIGRATION_VERSION
        new_v = 5
        set_migration_number(c, new_v)
        assert new_v == get_migration_number(c)
        set_migration_number(c, old_v)
        assert old_v == get_migration_number(c)

    assert not caplog.records, "No errors are expected to have been logged in this test"


def test_create_data_series_table__normal(tmp_path, caplog, series_interface):
    db_path = tmp_path / "db.sqlite"
    with db_connection(db_path) as c:
        create_data_series_table(c, series_interface)
        assert (data_table_name_for_series(series_interface["name"])) in table_list(c)

    assert not caplog.records, "No errors are expected to have been logged in this test"


def test_create_data_series_table__duplicate(tmp_path, caplog, series_interface):
    db_path = tmp_path / "db.sqlite"
    with db_connection(db_path) as c:
        create_data_series_table(c, series_interface)
        with pytest.raises(ValueError):
            create_data_series_table(c, series_interface)


def test_create_data_series_table__bad_name(tmp_path, caplog, series_interface):
    db_path = tmp_path / "db.sqlite"
    with db_connection(db_path) as c:
        with pytest.raises(ValueError):
            series_interface["name"] = "INSERT ;"
            create_data_series_table(c, series_interface)


@pytest.fixture
def tmp_db_with_real_series_migration_1_only(tmp_db_with_real_series, series_interface):
    # Drop the data table and migrate to the first table version
    tmp_db_with_real_series.execute(
        f"DROP TABLE {data_table_name_for_series(series_interface['name'])};"
    )

    create_data_series_table(
        tmp_db_with_real_series, series_interface, target_migration_version=1
    )
    return tmp_db_with_real_series


def test_execute_data_tables_migrations__migrate_to_null(
    tmp_db_with_real_series_migration_1_only, series_interface
):
    """
    This is more of an integration test to see how the migration process
    succeeds.

    # WARN:  This test does not have particularly great isolation, this should
    be refactored either into a module that is known to have low levels of
    isolation or find ways to improve isolation.
    """
    # Add a value for verification that the migration did not lose data
    add_value(
        tmp_db_with_real_series_migration_1_only, series_interface["name"], 99, 50.0, 1
    )
    assert (99, 50.0, 1) == get_value_at_time(
        tmp_db_with_real_series_migration_1_only, series_interface["name"], 99
    )

    # Write null Should Fail
    with pytest.raises(sqlite3.IntegrityError):
        add_value(
            tmp_db_with_real_series_migration_1_only,
            series_interface["name"],
            100,
            None,
        )

    # Migrate to the next table version
    _execute_data_tables_migrations(
        tmp_db_with_real_series_migration_1_only, series_interface, 1, 2
    )

    # Write null
    add_value(
        tmp_db_with_real_series_migration_1_only, series_interface["name"], 100, None, 0
    )
    assert (99, 50.0, 1) == get_value_at_time(
        tmp_db_with_real_series_migration_1_only, series_interface["name"], 99
    )
    assert (100, None, 0) == get_latest_value(
        tmp_db_with_real_series_migration_1_only, series_interface["name"]
    )
