import pytest

from raik_history.db.series_table import add_series, SeriesInterface, get_series
from raik_history.series.constants import (
    DATA_TYPE_REAL,
    TIME_TYPE_EPOCH_MS,
    TIME_STAMP_TYPE_REMOTE,
)


def test_round_trip__coverage(tmp_db):
    """
    This is just a round trip test for coverage purposes
    """
    series_name = "test_round_trip__coverage.with.a.dot"
    series_data = SeriesInterface(
        name=series_name,
        data_type=DATA_TYPE_REAL,
        time_type=TIME_TYPE_EPOCH_MS,
        time_stamp_type=TIME_STAMP_TYPE_REMOTE,
        units="",
        differential_units="",
        integral_units="",
        storage_configuration={},
        retrieval_configuration={},
    )
    add_series(tmp_db, series_data)
    assert series_data == get_series(tmp_db, series_name)


def test_bad_series_name(tmp_db):
    with pytest.raises(ValueError):
        get_series(tmp_db, "No Series")
