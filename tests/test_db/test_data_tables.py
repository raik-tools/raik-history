import pytest
from raik_history.db.data_tables import (
    query_data_values,
    add_value,
    get_value_at_time,
    get_latest_value,
    get_oldest_value,
    get_last_or_at_value,
    get_at_or_next_value,
    get_periodic_series,
    get_all_values_no_page,
    update_latest_value,
)
from raik_history.quality import QualityCodes
from raik_history.series.constants import Quality


@pytest.mark.parametrize(
    "safe_val,safe_quality",
    [
        (53.3, QualityCodes.GOOD_NON_SPECIFIC),
        (53, QualityCodes.GOOD_NON_SPECIFIC),
        (None, QualityCodes.BAD_NON_SPECIFIC),
    ],
)
def test_add_real_value__normal(
    real_series_connection, real_series_name, safe_val, safe_quality: Quality
):
    """
    This is just a round trip test of safe values
    """
    add_value(
        real_series_connection,
        real_series_name,
        12000,
        safe_val,
        safe_quality,
    )

    assert (12000, safe_val, safe_quality) == get_value_at_time(
        real_series_connection, real_series_name, 12000
    )


@pytest.mark.parametrize(
    "safe_val,safe_quality",
    [(53, QualityCodes.GOOD_NON_SPECIFIC), (None, QualityCodes.BAD_NON_SPECIFIC)],
)
def test_add_int_value__normal(
    int_series_connection, int_series_name, safe_val, safe_quality: Quality
):
    """
    This is just a round trip test of safe values
    """
    add_value(
        int_series_connection,
        int_series_name,
        12000,
        safe_val,
        safe_quality,
    )

    assert (12000, safe_val, safe_quality) == get_value_at_time(
        int_series_connection, int_series_name, 12000
    )


@pytest.mark.parametrize(
    "safe_val,safe_quality",
    [
        (True, QualityCodes.GOOD_NON_SPECIFIC),
        (False, QualityCodes.GOOD_NON_SPECIFIC),
        (None, QualityCodes.BAD_NON_SPECIFIC),
    ],
)
def test_add_bool_value__normal(
    bool_series_connection, bool_series_name, safe_val, safe_quality: Quality
):
    """
    This is just a round trip test of safe values
    """
    add_value(
        bool_series_connection,
        bool_series_name,
        12000,
        safe_val,
        safe_quality,
    )

    assert (12000, safe_val, safe_quality) == get_value_at_time(
        bool_series_connection, bool_series_name, 12000
    )


@pytest.mark.parametrize(
    "safe_val,safe_quality",
    [
        ("True", QualityCodes.GOOD_NON_SPECIFIC),
        ("", QualityCodes.GOOD_NON_SPECIFIC),
        (None, QualityCodes.BAD_NON_SPECIFIC),
    ],
)
def test_add_string_value__normal(
    string_series_connection, string_series_name, safe_val, safe_quality: Quality
):
    """
    This is just a round trip test of safe values
    """
    add_value(
        string_series_connection,
        string_series_name,
        12000,
        safe_val,
        safe_quality,
    )

    assert (12000, safe_val, safe_quality) == get_value_at_time(
        string_series_connection, string_series_name, 12000
    )


@pytest.mark.parametrize(
    "safe_val,safe_quality,update_val",
    [
        (53.3, QualityCodes.GOOD_NON_SPECIFIC, 12.0),
        (53, QualityCodes.GOOD_NON_SPECIFIC, 12.0),
        (None, QualityCodes.BAD_NON_SPECIFIC, 12.0),
        (
            True,
            QualityCodes.BAD_NON_SPECIFIC,
            False,
        ),  # This might break in future sqlite updates?
    ],
)
def test_update_latest_value__normal(
    real_series_connection,
    real_series_name,
    safe_val,
    safe_quality: Quality,
    update_val,
):
    """
    This is just a round trip test of safe values
    """
    add_value(
        real_series_connection,
        real_series_name,
        12000,
        safe_val,
        safe_quality,
    )

    assert [(12000, safe_val, safe_quality)] == get_all_values_no_page(
        real_series_connection, real_series_name
    )

    update_latest_value(
        real_series_connection,
        real_series_name,
        12010,
        update_val,
        safe_quality,
    )

    assert [(12010, update_val, safe_quality)] == get_all_values_no_page(
        real_series_connection, real_series_name
    )


def test_get_value_at_time__no_data(real_series_connection, real_series_name):
    """
    This is just a round trip test for coverage purposes
    """
    add_value(real_series_connection, real_series_name, 12000, 53.2)

    assert get_value_at_time(real_series_connection, real_series_name, 12001) is None


def test_get_latest_value__normal(real_series_connection, real_series_name):
    """
    This is just a round trip test for coverage purposes
    """
    add_value(real_series_connection, real_series_name, 12000, 53.2)
    add_value(real_series_connection, real_series_name, 12001, 42.1)

    assert (12001, 42.1, QualityCodes.UNCERTAIN_NON_SPECIFIC) == get_latest_value(
        real_series_connection, real_series_name
    )


def test_get_latest_value__no_data(real_series_connection, real_series_name):
    assert get_latest_value(real_series_connection, real_series_name) is None


def test_get_oldest_value__normal(real_series_connection, real_series_name):
    """
    This is just a round trip test for coverage purposes
    """
    add_value(real_series_connection, real_series_name, 12000, 53.2)
    add_value(real_series_connection, real_series_name, 12001, 42.1)

    assert (12000, 53.2, QualityCodes.UNCERTAIN_NON_SPECIFIC) == get_oldest_value(
        real_series_connection, real_series_name
    )


def test_get_oldest_value__no_data(real_series_connection, real_series_name):
    assert get_oldest_value(real_series_connection, real_series_name) is None


def test_get_last_or_at_value__normal_at_value(
    real_series_connection, real_series_name
):
    """
    This is just a round trip test for coverage purposes
    """
    add_value(real_series_connection, real_series_name, 12000, 53.2)
    add_value(real_series_connection, real_series_name, 12005, 42.1)

    assert [(12000, 53.2, QualityCodes.UNCERTAIN_NON_SPECIFIC)] == get_last_or_at_value(
        real_series_connection, real_series_name, 12000
    )


def test_get_last_or_at_value__normal_in_front_of_value(
    real_series_connection, real_series_name
):
    """
    This is just a round trip test for coverage purposes
    """
    add_value(real_series_connection, real_series_name, 11800, 13.2)
    add_value(real_series_connection, real_series_name, 11900, 23.2)
    add_value(real_series_connection, real_series_name, 12000, 53.2)
    add_value(real_series_connection, real_series_name, 12005, 42.1)

    assert [
        (11900, 23.2, QualityCodes.UNCERTAIN_NON_SPECIFIC),
        (12000, 53.2, QualityCodes.UNCERTAIN_NON_SPECIFIC),
    ] == get_last_or_at_value(real_series_connection, real_series_name, 12004, count=2)


def test_get_last_or_at_value__no_data(real_series_connection, real_series_name):
    assert [] == get_last_or_at_value(real_series_connection, real_series_name, 12000)


def test_get_at_or_next_value__normal_at_value(
    real_series_connection, real_series_name
):
    """
    This is just a round trip test for coverage purposes
    """
    add_value(real_series_connection, real_series_name, 12000, 53.2)
    add_value(real_series_connection, real_series_name, 12005, 42.1)

    assert [(12005, 42.1, QualityCodes.UNCERTAIN_NON_SPECIFIC)] == get_at_or_next_value(
        real_series_connection, real_series_name, 12005
    )


def test_get_at_or_next_value__normal_behind_value(
    real_series_connection, real_series_name
):
    """
    This is just a round trip test for coverage purposes
    """
    add_value(real_series_connection, real_series_name, 12000, 53.2)
    add_value(real_series_connection, real_series_name, 12005, 42.1)
    add_value(real_series_connection, real_series_name, 12006, 52.1)
    add_value(real_series_connection, real_series_name, 12007, 62.1)

    assert [
        (12005, 42.1, QualityCodes.UNCERTAIN_NON_SPECIFIC),
        (12006, 52.1, QualityCodes.UNCERTAIN_NON_SPECIFIC),
    ] == get_at_or_next_value(real_series_connection, real_series_name, 12004, count=2)


def test_get_at_or_next_value__no_data(real_series_connection, real_series_name):
    assert [] == get_at_or_next_value(real_series_connection, real_series_name, 12000)


def test_get_all_values_no_page__normal(real_series_connection, real_series_name):
    """
    This is just a round trip test for coverage purposes
    """
    add_value(real_series_connection, real_series_name, 12000, 53.2)
    add_value(real_series_connection, real_series_name, 12005, 42.1)

    assert [
        (12000, 53.2, QualityCodes.UNCERTAIN_NON_SPECIFIC),
        (12005, 42.1, QualityCodes.UNCERTAIN_NON_SPECIFIC),
    ] == get_all_values_no_page(real_series_connection, real_series_name)


def test_get_periodic_series__normal(real_series_connection, real_series_name):
    add_value(real_series_connection, real_series_name, 12000, 53.2)
    add_value(real_series_connection, real_series_name, 12005, 42.1)
    add_value(real_series_connection, real_series_name, 12010, 42.2)
    add_value(real_series_connection, real_series_name, 12015, 42.3)

    assert [
        (12000, 53.2, QualityCodes.UNCERTAIN_NON_SPECIFIC),
        (12005, 42.1, QualityCodes.UNCERTAIN_NON_SPECIFIC),
        (12010, 42.2, QualityCodes.UNCERTAIN_NON_SPECIFIC),
        (12015, 42.3, QualityCodes.UNCERTAIN_NON_SPECIFIC),
    ] == list(get_periodic_series(real_series_connection, real_series_name, 12000, 5))


def test_get_periodic_series__normal_with_end_time(
    real_series_connection, real_series_name
):
    add_value(real_series_connection, real_series_name, 12000, 53.2)
    add_value(real_series_connection, real_series_name, 12005, 42.1)
    add_value(real_series_connection, real_series_name, 12010, 42.2)
    add_value(real_series_connection, real_series_name, 12015, 42.3)

    assert [
        (12000, 53.2, QualityCodes.UNCERTAIN_NON_SPECIFIC),
        (12005, 42.1, QualityCodes.UNCERTAIN_NON_SPECIFIC),
    ] == list(
        get_periodic_series(
            real_series_connection, real_series_name, 12000, 5, end_time_ms=12010
        )
    )


def test_get_periodic_series__normal_paged(real_series_connection, real_series_name):
    add_value(real_series_connection, real_series_name, 12000, 53.2)
    add_value(real_series_connection, real_series_name, 12005, 42.1)
    add_value(real_series_connection, real_series_name, 12010, 42.2)
    add_value(real_series_connection, real_series_name, 12015, 42.3)

    assert [
        (12000, 53.2, QualityCodes.UNCERTAIN_NON_SPECIFIC),
        (12005, 42.1, QualityCodes.UNCERTAIN_NON_SPECIFIC),
        (12010, 42.2, QualityCodes.UNCERTAIN_NON_SPECIFIC),
        (12015, 42.3, QualityCodes.UNCERTAIN_NON_SPECIFIC),
    ] == list(
        get_periodic_series(
            real_series_connection, real_series_name, 12000, 5, page_size=1
        )
    )


def test_get_periodic_series__normal_missing_data(
    real_series_connection, real_series_name
):
    add_value(real_series_connection, real_series_name, 12000, 53.2)
    add_value(real_series_connection, real_series_name, 12005, 42.1)
    add_value(real_series_connection, real_series_name, 12010, 42.2)

    assert [
        (12000, 53.2, QualityCodes.UNCERTAIN_NON_SPECIFIC),
        (12001, None, QualityCodes.BAD_NON_SPECIFIC),
        (12002, None, QualityCodes.BAD_NON_SPECIFIC),
        (12003, None, QualityCodes.BAD_NON_SPECIFIC),
        (12004, None, QualityCodes.BAD_NON_SPECIFIC),
        (12005, 42.1, QualityCodes.UNCERTAIN_NON_SPECIFIC),
        (12006, None, QualityCodes.BAD_NON_SPECIFIC),
        (12007, None, QualityCodes.BAD_NON_SPECIFIC),
        (12008, None, QualityCodes.BAD_NON_SPECIFIC),
        (12009, None, QualityCodes.BAD_NON_SPECIFIC),
    ] == list(
        get_periodic_series(
            real_series_connection, real_series_name, 12000, 1, end_time_ms=12010
        )
    )


def test_get_periodic_series__normal_misaligned_data(
    real_series_connection, real_series_name
):
    add_value(real_series_connection, real_series_name, 12000, 53.2)
    add_value(real_series_connection, real_series_name, 12003, 42.1)
    add_value(real_series_connection, real_series_name, 12006, 42.2)

    assert [
        (12000, 53.2, QualityCodes.UNCERTAIN_NON_SPECIFIC),
        (12002, None, QualityCodes.BAD_NON_SPECIFIC),
        (12004, None, QualityCodes.BAD_NON_SPECIFIC),
        (12006, 42.2, QualityCodes.UNCERTAIN_NON_SPECIFIC),
        (12008, None, QualityCodes.BAD_NON_SPECIFIC),
    ] == list(
        get_periodic_series(
            real_series_connection, real_series_name, 12000, 2, end_time_ms=12010
        )
    )


def test_query_data_values__include_outer_value_start(
    real_series_connection, real_series_name
):
    """
    This is just a round trip test for coverage purposes
    """
    add_value(real_series_connection, real_series_name, 11800, 13.2)
    add_value(real_series_connection, real_series_name, 11900, 23.2)
    add_value(real_series_connection, real_series_name, 12000, 53.2)
    add_value(real_series_connection, real_series_name, 12005, 42.1)

    assert [
        (11900, 23.2, QualityCodes.UNCERTAIN_NON_SPECIFIC),
        (12000, 53.2, QualityCodes.UNCERTAIN_NON_SPECIFIC),
        (12005, 42.1, QualityCodes.UNCERTAIN_NON_SPECIFIC),
    ] == list(
        query_data_values(
            real_series_connection,
            real_series_name,
            ts__gte=11905,
            include_outer_value_start=True,
        )
    )


def test_query_data_values__include_outer_value_end(
    real_series_connection, real_series_name
):
    """
    This is just a round trip test for coverage purposes
    """
    add_value(real_series_connection, real_series_name, 11800, 13.2)
    add_value(real_series_connection, real_series_name, 11900, 23.2)
    add_value(real_series_connection, real_series_name, 12000, 53.2)
    add_value(real_series_connection, real_series_name, 12005, 42.1)

    assert [
        (11800, 13.2, QualityCodes.UNCERTAIN_NON_SPECIFIC),
        (11900, 23.2, QualityCodes.UNCERTAIN_NON_SPECIFIC),
        (12000, 53.2, QualityCodes.UNCERTAIN_NON_SPECIFIC),
    ] == list(
        query_data_values(
            real_series_connection,
            real_series_name,
            ts__lte=11905,
            include_outer_value_end=True,
        )
    )


def test_query_data_values__include_start_and_end_outer_values_for_single_boundary_request(
    real_series_connection, real_series_name
):
    """
    This is just a round trip test for coverage purposes
    """
    add_value(real_series_connection, real_series_name, 11800, 13.2)
    add_value(real_series_connection, real_series_name, 11900, 23.2)
    add_value(real_series_connection, real_series_name, 12000, 53.2)
    add_value(real_series_connection, real_series_name, 12005, 42.1)

    assert [(11900, 23.2, QualityCodes.UNCERTAIN_NON_SPECIFIC)] == list(
        query_data_values(
            real_series_connection,
            real_series_name,
            ts__gte=11900,
            ts__lte=11900,
            include_outer_value_end=True,
            include_outer_value_start=True,
        )
    )
