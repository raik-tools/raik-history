from raik_history.units_of_measure import U, convert_units


def test_percent_units():
    """
    Spot check of the added percent unit
    """
    x = 24 * U.percent
    assert "24 %" == f"{x:~}"


def test_convert_units__normal_example():
    """
    Spot check of the added percent unit
    """
    assert 16.09344 == convert_units(10, "miles", "km")
