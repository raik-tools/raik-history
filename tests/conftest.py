import pytest
import uuid

from raik_history.db.connection import db_connection
from raik_history.db.series_table import add_series, SeriesInterface
from raik_history.series.constants import (
    DATA_TYPE_REAL,
    TIME_TYPE_EPOCH_MS,
    TIME_STAMP_TYPE_REMOTE,
    DATA_TYPE_INT,
    DATA_TYPE_BOOL,
    DATA_TYPE_STRING,
)


@pytest.fixture
def tmp_db_path(tmp_path):
    return tmp_path / f"{uuid.uuid4()}.db"


@pytest.fixture
def tmp_db(tmp_db_path):
    with db_connection(tmp_db_path) as c:
        yield c


@pytest.fixture(autouse=True)
def clear_caches():
    from raik_history import reset_caches

    reset_caches()


@pytest.fixture
def real_series_name():
    return "real.series"


@pytest.fixture
def real_series_connection(tmp_db, real_series_name):
    series_data = SeriesInterface(
        name=real_series_name,
        data_type=DATA_TYPE_REAL,
        time_type=TIME_TYPE_EPOCH_MS,
        time_stamp_type=TIME_STAMP_TYPE_REMOTE,
        units="kW",
        differential_units="",
        integral_units="J",
        storage_configuration={},
        retrieval_configuration={},
    )
    add_series(tmp_db, series_data)
    return tmp_db


@pytest.fixture
def int_series_name():
    return "int_series"


@pytest.fixture
def int_series_connection(tmp_db, int_series_name):
    series_data = SeriesInterface(
        name=int_series_name,
        data_type=DATA_TYPE_INT,
        time_type=TIME_TYPE_EPOCH_MS,
        time_stamp_type=TIME_STAMP_TYPE_REMOTE,
        units="",
        differential_units="",
        integral_units="",
        storage_configuration={},
        retrieval_configuration={},
    )
    add_series(tmp_db, series_data)
    return tmp_db


@pytest.fixture
def bool_series_name():
    return "bool_series"


@pytest.fixture
def bool_series_connection(tmp_db, bool_series_name):
    series_data = SeriesInterface(
        name=bool_series_name,
        data_type=DATA_TYPE_BOOL,
        time_type=TIME_TYPE_EPOCH_MS,
        time_stamp_type=TIME_STAMP_TYPE_REMOTE,
        units="",
        differential_units="",
        integral_units="",
        storage_configuration={},
        retrieval_configuration={},
    )
    add_series(tmp_db, series_data)
    return tmp_db


@pytest.fixture
def string_series_name():
    return "string_series"


@pytest.fixture
def string_series_connection(tmp_db, string_series_name):
    series_data = SeriesInterface(
        name=string_series_name,
        data_type=DATA_TYPE_STRING,
        time_type=TIME_TYPE_EPOCH_MS,
        time_stamp_type=TIME_STAMP_TYPE_REMOTE,
        units="",
        differential_units="",
        integral_units="",
        storage_configuration={},
        retrieval_configuration={},
    )
    add_series(tmp_db, series_data)
    return tmp_db
