import functools
import logging
import pathlib
import sqlite3
from typing import Optional

import pandas as pandas

from raik_history.db.event_tables import (
    get_or_create_event_id,
    create_event,
    get_event,
    update_event,
    get_multiple_events,
)
from raik_history.quality import QualityCodes, worst_quality
from raik_history.series.constants import (
    TIME_TYPE_EPOCH_MS,
    TIME_STAMP_TYPE_REMOTE,
    DataTuple,
    DATA_TYPE_STRING,
    DATA_TYPE_INT,
    DATA_TYPE_REAL,
    DATA_TYPE_BOOL,
    Value,
    Timestamp,
    Quality,
)
from raik_history.series.series import existing_or_new_connection, Series
from raik_history.series.series_set import SeriesSet
from raik_history.time_and_calendar import (
    datetime_to_string,
    epoch_ms_to_datetime,
    seconds_apart,
)
from raik_history.units_of_measure import specify_absolute_error

logger = logging.getLogger(__name__)


class Event:
    """
    An event object is defined by the class that defines the event.  All event
    types will be a subclass of this class.

    The instance of the event refers to the specific start and end time for
    that event.
    """

    NAME = "event"
    DESCRIPTION = ""
    TIME_TYPE = TIME_TYPE_EPOCH_MS
    TIME_STAMP_TYPE = TIME_STAMP_TYPE_REMOTE

    db_path: pathlib.Path = None
    db_connection: sqlite3.Connection = None

    trigger_series: Optional[Series] = None
    interpolate_at_start_boundary = False
    interpolate_at_end_boundary = False

    # Refer to:
    # https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.interpolate.html
    default_interpolation_map = {
        DATA_TYPE_STRING: "pad",
        DATA_TYPE_INT: "index",
        DATA_TYPE_REAL: "index",
        DATA_TYPE_BOOL: "pad",
    }
    series_interpolation_map = None

    columns = ["event_duration"]

    class RelatedSeries(SeriesSet):
        pass

    def __init__(
        self,
        start_ts: int,
        end_ts: Optional[int] = None,
        comments: Optional[str] = None,
        quality: Optional[Quality] = None,
        _skip_db_load=False,
    ):
        self.start_ts = start_ts
        self.end_ts = end_ts
        self.comments = comments
        self.quality = quality

        # Synchronise db_path and/or db_connection with RelatedSeries
        class _RelatedSeries(self.RelatedSeries):
            db_path = self.RelatedSeries.db_path or self.db_path
            db_connection = self.RelatedSeries.db_connection or self.db_connection

        self.RelatedSeries = _RelatedSeries

        # Synchronise with the DB
        if _skip_db_load:
            self._new = True
            self._saved_end_ts = None
            self._saved_comments = None
            self._saved_quality = None
            self._event_type_id = None
        else:
            with existing_or_new_connection(db_path=self.db_path, obj=self) as c:
                self._event_type_id = get_or_create_event_id(
                    c, self.NAME, self.DESCRIPTION, self.TIME_TYPE, self.TIME_STAMP_TYPE
                )
                e = get_event(c, self._event_type_id, self.start_ts)
                if e is None:
                    self._new = True
                    self._saved_end_ts = None
                    self._saved_comments = None
                    self._saved_quality = None
                else:
                    self._new = False
                    self._saved_end_ts = e["end_ts"]
                    self._saved_comments = e["comments"]
                    self._saved_quality = e["quality"]
                    if end_ts is None:
                        self.end_ts = self._saved_end_ts
                    if comments is None:
                        self.comments = self._saved_comments
                    if quality is None:
                        self.quality = self._saved_quality

        # Set Defaults

        if self.quality is None:
            self.quality = QualityCodes.UNCERTAIN_NON_SPECIFIC

    def __str__(self):
        if self.is_open:
            return f"<Event {self.NAME} {self.start_ts}>"
        else:
            return f"<Event {self.NAME} {self.start_ts} to {self.end_ts}>"

    def has_changed(self) -> bool:
        if self._new:  # pragma:nocover
            return True
        else:
            return (
                self.end_ts != self._saved_end_ts
                or self.comments != self._saved_comments
                or self.quality != self._saved_quality
            )

    def save(self):
        if self._new:
            with existing_or_new_connection(db_path=self.db_path, obj=self) as c:
                create_event(
                    c,
                    self._event_type_id,
                    self.start_ts,
                    self.end_ts,
                    self.comments,
                    self.quality,
                )
        elif self.has_changed():
            with existing_or_new_connection(db_path=self.db_path, obj=self) as c:
                update_event(
                    c,
                    self._event_type_id,
                    self.start_ts,
                    self.end_ts,
                    self.comments,
                    self.quality,
                )
        self._new = False
        self._saved_end_ts = self.end_ts
        self._saved_comments = self.comments

    def close_event(self, end_ts):
        self.end_ts = end_ts
        self.quality = self.calculate_quality()
        self.save()

    @property
    def raw_data(self) -> tuple[int, int, int, str, int]:
        return (
            self._event_type_id,
            self.start_ts,
            self.end_ts,
            self.comments,
            self.quality,
        )

    _filter_fx__lt = "lt"
    _filter_fx__lte = "lte"
    _filter_fx__eq = "eq"
    _filter_fx__gte = "gte"
    _filter_fx__gt = "gt"
    _filter_fx__contains = "contains"

    def filter(self, **filter_args) -> bool:
        """
        By default, each filter will test the value against a column and
        if there is not a match, drops the event.
        """
        for filter_name, filter_val in filter_args.items():
            if "__" in filter_name:
                col_name, col_fx = filter_name.split("__")
            else:
                col_name = filter_name
                col_fx = self._filter_fx__eq

            v = getattr(self, col_name)()

            if col_fx == self._filter_fx__lt and v < filter_val:
                continue
            elif col_fx == self._filter_fx__lte and v <= filter_val:
                continue
            elif col_fx == self._filter_fx__eq and v == filter_val:
                continue
            elif col_fx == self._filter_fx__gte and v >= filter_val:
                continue
            elif col_fx == self._filter_fx__gt and v > filter_val:
                continue
            elif col_fx == self._filter_fx__contains and filter_val in v:
                continue
            else:
                return False
        return True

    @classmethod
    def get_events(
        cls,
        start_ts__gte: Optional[int] = None,
        start_ts__lt: Optional[int] = None,
        comments__contains: Optional[str] = None,
        count: Optional[int] = None,
        chronological_order=True,
        only_good_quality=True,
        **filter_args,
    ) -> list["Event"]:
        """
        Retrieves all events and then, if additional keywords are provided,
        will pass these to a custom filter function.  The default is an exact
        match check against the associated column.
        """
        with existing_or_new_connection(db_path=cls.db_path, obj=cls) as c:
            event_type_id = get_or_create_event_id(
                c, cls.NAME, cls.DESCRIPTION, cls.TIME_TYPE, cls.TIME_STAMP_TYPE
            )
            events = []
            for data in get_multiple_events(
                c,
                event_type_id=event_type_id,
                start_ts__gte=start_ts__gte,
                start_ts__lt=start_ts__lt,
                comments__contains=comments__contains,
                count=count,
                chronological_order=chronological_order,
                only_good_quality=only_good_quality,
            ):
                # Instantiate an event instance
                e = cls(
                    data["start_ts"],
                    data["end_ts"],
                    data["comments"],
                    quality=data["quality"],
                    _skip_db_load=True,
                )
                e._new = False
                e._event_type_id = event_type_id
                e._saved_end_ts = e.end_ts
                e._saved_comments = e.comments
                # Test the filter on this instance
                if filter_args and not e.filter(**filter_args):
                    pass
                else:
                    events.append(e)

        return events

    @classmethod
    def event_open_detected(cls, last_val: DataTuple, val: DataTuple) -> bool:
        raise NotImplementedError

    @classmethod
    def event_close_detected(cls, last_val: DataTuple, val: DataTuple) -> bool:
        raise NotImplementedError

    # noinspection PyUnusedLocal
    @classmethod
    def open_event_from_trigger(cls, last_val: DataTuple, val: DataTuple) -> "Event":
        return cls(start_ts=val[0])

    # noinspection PyUnusedLocal
    @classmethod
    def close_event_from_trigger(
        cls, event: "Event", last_val: DataTuple, val: DataTuple
    ):
        event.close_event(end_ts=last_val[0])

    @classmethod
    def latest_event(cls) -> Optional["Event"]:
        latest_events: list[Event] = cls.get_events(
            chronological_order=False, count=1, only_good_quality=False
        )
        if latest_events:
            return latest_events[0]
        else:
            return None

    @classmethod
    def create_or_close_events(cls):
        """
        Scan all trigger series data since the last event.  Create new events on
        trigger.

        This event starts when there is positive confirmation of charging and
        ends at the last positive confirmation of charging.  While this is not
        entirely accurate, it is better for calculation accuracy to only include
        charge state data.  (Whereas trip planning is better off overlapping the data.)
        """
        # ## Find Start Time -- If there are no closed events
        # Fetch the last event times
        latest_event: Event = cls.latest_event()

        if latest_event and latest_event.is_open:
            current_event = latest_event
            last_trigger_time = current_event.start_ts
        elif latest_event:
            current_event = None
            last_trigger_time = latest_event.end_ts
        else:
            current_event = None
            last_trigger_time = 0

        last_data = None

        trigger_series = cls.trigger_series.get_all_values_in_range(
            start_time=last_trigger_time,
            end_time=None,
        )
        for data in trigger_series:
            if data[1] is None:
                pass
            elif last_data is None:
                pass
            else:
                if current_event and cls.event_close_detected(last_data, data):
                    cls.close_event_from_trigger(current_event, last_data, data)
                    logger.info(
                        "<event %s> Closing %s from transition %s --> %s"
                        % (cls.NAME, current_event, last_data, data)
                    )

                    current_event = None
                elif not current_event and cls.event_open_detected(last_data, data):
                    current_event = cls.open_event_from_trigger(last_data, data)
                    logger.info(
                        "<event %s> Opening %s from transition %s --> %s"
                        % (cls.NAME, current_event, last_data, data)
                    )

            last_data = data

    @property
    def is_open(self) -> bool:
        return not self.end_ts or self.end_ts < self.start_ts

    @functools.cache
    def get_data(
        self,
    ) -> pandas.DataFrame:
        """
        The data returned is monotonic but not isochronous.  If the raw
        data is not time stamped at the same time then interpolation is
        automatically performed where data exists within the event.

        Interpolation at the boundary is controlled by the class variables.

        If a series has no data, it is excluded from the data frame.  If there
        is a single data point, then padding interpolation is performed.

        Quality is currently ignored.

        :raises ValueError:  If this event is not closed
        """
        if self.is_open:
            raise ValueError("Data for open event is not available")
        if len(self.RelatedSeries.SERIES) == 0:
            raise NotImplementedError(
                f"No related series have been implemented for {self}"
            )

        data = self.RelatedSeries.query_data_values(
            ts__gte=self.start_ts,
            ts__lte=self.end_ts,
            include_outer_value_start=self.interpolate_at_start_boundary,
            include_outer_value_end=self.interpolate_at_end_boundary,
        )

        data_series = {}
        for series_name, raw_data in data.items():
            if raw_data:
                data_series[series_name] = pandas.Series(
                    data=map(lambda x: x[1], raw_data),
                    index=map(lambda x: x[0], raw_data),
                )
            # else:
            #     data_series[series_name] = pandas.Series()

        df = pandas.DataFrame.from_dict(data_series)

        # During debugging a copy can be useful for analysing behaviour below
        # df2 = df.copy(deep=True)
        df2 = df
        for s in self.RelatedSeries.SERIES:
            if s.name not in df2:
                continue
            default_interpolation = self.default_interpolation_map[s.data_type]
            interpolation = (
                df2[s.name].count() == 1
                and "pad"
                or (
                    self.series_interpolation_map.get(s.name, default_interpolation)
                    if self.series_interpolation_map
                    else default_interpolation
                )
            )
            if interpolation == "slinear":
                df2[s.name].interpolate(
                    method=interpolation, inplace=True, fill_value="extrapolate"
                )
            else:
                df2[s.name].interpolate(method=interpolation, inplace=True)

        df2.dropna(inplace=True)
        return df2

    def quality_check_only_clean_trigger_data(self) -> Quality:
        """
        If there is any bad quality data or stream breaks in the
        trigger, then this returns a bad quality.
        """
        if self.trigger_series:
            bad_quality_data = list(
                self.trigger_series.query_data_values(
                    ts__gte=self.start_ts,
                    ts__lte=self.end_ts,
                    quality__lt=QualityCodes.MIN_GOOD_QUALITY,
                )
            )
            admin_quality_data = list(
                self.trigger_series.query_data_values(
                    ts__gte=self.start_ts,
                    ts__lte=self.end_ts,
                    quality__gte=QualityCodes.ADMIN_QUALITY_STARTS,
                )
            )
            if admin_quality_data:
                return QualityCodes.BAD_COMMUNICATION_FAILURE
            elif bad_quality_data:
                return QualityCodes.UNCERTAIN_NON_SPECIFIC
            else:
                return QualityCodes.GOOD_NON_SPECIFIC
        else:
            return QualityCodes.UNCERTAIN_NON_SPECIFIC

    def quality_check_complete_data_set(self) -> Quality:
        """
        If there is any bad quality data or stream breaks in the
        trigger, then this returns a bad quality.
        """
        if len(self.RelatedSeries.SERIES) == 0:
            return QualityCodes.UNCERTAIN_NON_SPECIFIC

        df: pandas.DataFrame = self.get_data()
        if all(s.name in df for s in self.RelatedSeries.SERIES):
            return QualityCodes.GOOD_NON_SPECIFIC
        else:
            return QualityCodes.UNCERTAIN_NON_SPECIFIC

    def calculate_quality(self) -> Quality:
        """
        This can be overridden by subclasses to specify how quality for
        an event is determined on a per-event basis.
        """
        return worst_quality(
            (
                self.quality_check_only_clean_trigger_data(),
                self.quality_check_complete_data_set(),
            )
        )

    @classmethod
    def rescan_event_qualities(cls):
        """
        Reviews the data of all associated events.  If the trigger series has
        no interruptions then the quality is set to Good.
        """
        for e in cls.get_events(only_good_quality=False):
            q = e.calculate_quality()
            if q != e.quality:
                e.quality = q
                e.save()

    @functools.cache
    def data_start_time(self) -> Timestamp:
        """
        This is not the same as the event start and end time, as it
        reflects when all related series have data.

        If there is no related series, the epoch is returned.
        """
        if len(self.RelatedSeries.SERIES) == 0:
            return 0

        df: pandas.DataFrame = self.get_data()
        return int(df.index[0])

    @functools.cache
    def data_end_time(self) -> Timestamp:
        """
        This is not the same as the event start and end time, as it
        reflects when all related series have data.

        If there is no related series, the epoch is returned.
        """
        if len(self.RelatedSeries.SERIES) == 0:
            return 0

        df: pandas.DataFrame = self.get_data()
        return int(df.index[-1])

    @functools.cache
    def data_start_time_local_str(self) -> str:
        t = self.data_start_time()
        return datetime_to_string(epoch_ms_to_datetime(t))

    @functools.cache
    def data_end_time_local_str(self) -> str:
        t = self.data_end_time()
        return datetime_to_string(epoch_ms_to_datetime(t))

    @functools.cache
    def duration(self) -> int:
        """This refers to the data duration within the event."""
        return self.data_end_time() - self.data_start_time()

    @functools.cache
    @specify_absolute_error("0.0005")
    def duration_s(self) -> float:
        """This refers to the data duration within the event."""
        return seconds_apart(self.data_end_time(), self.data_start_time())

    @functools.cache
    @specify_absolute_error("0.00000833")
    def duration_m(self) -> float:
        """This refers to the data duration within the event."""
        return self.duration_s() / 60

    @functools.cache
    @specify_absolute_error("0.000000139")
    def duration_h(self) -> float:
        """This refers to the data duration within the event."""
        return self.duration_s() / 3600

    @functools.cache
    def event_duration(self) -> int:
        """This refers to the event start and end only"""
        return self.end_ts - self.start_ts

    @functools.cache
    @specify_absolute_error("0.0005")
    def event_duration_s(self) -> float:
        """This refers to the data duration within the event."""
        return seconds_apart(self.end_ts, self.start_ts)

    @functools.cache
    @specify_absolute_error("0.00000833")
    def event_duration_m(self) -> float:
        """This refers to the data duration within the event."""
        return self.event_duration_s() / 60

    @functools.cache
    @specify_absolute_error("0.000000139")
    def event_duration_h(self) -> float:
        """This refers to the data duration within the event."""
        return self.event_duration_s() / 3600

    def columns_dict(self) -> dict[str, Value]:
        return {col_name: getattr(self, col_name)() for col_name in self.columns}
