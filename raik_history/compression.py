"""
This module stores all the compression routines used by raik_history.

"""
from typing import Optional

from raik_history.series.constants import (
    DataTuple,
    TIME_TYPE_EPOCH_MS,
    Timestamp,
)
from raik_history.time_and_calendar import seconds_apart


class CompressorBase:

    last_val: Optional[DataTuple]
    last_new_time: Timestamp
    time_type: str

    def add_value(self, val: DataTuple):
        raise NotImplementedError

    def on_new_value(self, val: DataTuple):
        """
        A subclass needs to implement this method and store the
        value provided.
        """
        raise NotImplementedError

    def on_update_value(self, val: DataTuple):
        """
        A subclass needs to implement this method and update the
        previously stored value to the new value provided.
        """
        raise NotImplementedError


class ChangeOnlyCompressor(CompressorBase):
    """
    This compressor only stores changed values.  However, it ensures
    that when the stream is not changing, both ends of the unchanging
    period are preserved when unchanged values are repeatedly added.
    """

    def __init__(
        self,
        repeat_period: int = 7200,
        time_type: str = TIME_TYPE_EPOCH_MS,
    ):
        if time_type != TIME_TYPE_EPOCH_MS:
            raise NotImplementedError

        self.last_val = None
        self.last_new_time = 0
        self.static_value_count = 0
        self.repeat_period = repeat_period
        self.time_type = time_type

    def add_value(self, val: DataTuple):
        if self.last_val and val[0] == self.last_val[0]:
            # Ignore duplicate time stamps
            return
        if self.last_val is None:
            delta_time = 0
            val_has_changed = True
            quality_has_changed = True
        else:
            delta_time = seconds_apart(val[0], self.last_new_time)
            val_has_changed = val[1] != self.last_val[1]
            quality_has_changed = val[2] != self.last_val[2]

        if not (val_has_changed or quality_has_changed):
            self.static_value_count += 1
        else:
            self.static_value_count = 0

        if (
            val_has_changed
            or quality_has_changed
            or self.static_value_count == 1
            or (delta_time >= self.repeat_period)
        ):
            self.last_new_time = val[0]
            self.on_new_value(val)
        else:
            self.on_update_value(val)

        self.last_val = val

    def on_new_value(self, val: DataTuple):
        raise NotImplementedError

    def on_update_value(self, val: DataTuple):
        raise NotImplementedError
