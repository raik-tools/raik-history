from raik_history.series.constants import Timestamp


class DataExistsError(Exception):
    """
    Indicates data already exists in the time series and is not set
    to be overwritten.
    """

    def __init__(self, series_name: str, timestamp: Timestamp):
        super().__init__(f"Data already exists in {series_name} at {timestamp}")
