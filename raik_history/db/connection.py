# Copyright: (c) 2022, Gary Thompson <coding@garythompson.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
"""
SQLite Recap:

(summary from https://docs.python.org/3.8/library/sqlite3.html#controlling-transactions)

* A connection is required to communicate to the DB.  The connection
  should be closed when no longer needed and does not automatically
  commit changes.  A close called without a commit() will lose pending
  changes.

* A cursor object is useful for interacting with a result set, including
  pagination.

* If a cursor object is not needed the non-standard execute methods on
  the connection object

* Connection managers are available to instigate automatic roll back on
  an exception.
"""
import contextlib
import logging
import pathlib
import sqlite3

from raik_history.db.migration import migrate

logger = logging.getLogger(__name__)


# @functools.cache
def get_db_connection(db_path: pathlib.Path) -> sqlite3.Connection:  # pragma: no cover
    return sqlite3.connect(database=db_path)


@contextlib.contextmanager
def db_connection(db_path: pathlib.Path, perform_migration=True) -> sqlite3.Connection:
    """
    Opens a DB connection.  A commit and close is performed automatically
    when this context manager is closed.  Internally, it will commit the
    db periodically but if data needs to be available sooner than commits
    need to be manually called.  This will reduce performance.
    """
    try:
        c = get_db_connection(db_path)
        with c:
            if perform_migration:
                migrate(c)
            yield c
    except AssertionError:  # pragma: no cover
        # This code path is to allow automated tests to fail.
        raise
    except Exception as e:
        logger.warning(
            "Unhandled Exception occurred with the db connection %s: %s",
            db_path,
            e,
            exc_info=True,
        )
        raise
