"""
Design and code changes during the alpha development phase results in
cleanup necessary for early DBs.

This module provides the appropriate fixes and audits as concepts change.
"""
import pathlib
import sqlite3
import sys
import logging

from raik_history.db.connection import db_connection
from raik_history.db.data_tables import add_many_values, delete_value
from raik_history.db.migration import safe_data_table_name_for_series
from raik_history.db.series_table import get_series_list
from raik_history.quality import QualityCodes, UNCERTAIN_MASK, GOOD_MASK

logger = logging.getLogger(__name__)


def fix_change_of_quality_default(
    c: sqlite3.Connection,
    series_name: str,
):
    """
    Change 64 quality to 192

    This is due to an error with quality codes not being made available
    in Series and SeriesSet classes.
    """
    with c:
        # noinspection SqlResolve
        value_count = c.execute(
            f"SELECT COUNT(val) FROM {safe_data_table_name_for_series(series_name)} "
            f"WHERE quality = 64;"
        ).fetchone()[0]

        if value_count:
            logger.info(
                "Fixing default Quality value of %i points for %s",
                value_count,
                series_name,
            )

        # noinspection SqlResolve
        c.execute(
            f"UPDATE {safe_data_table_name_for_series(series_name)} "
            f"SET quality = 192 "
            f"WHERE quality = 64;"
        )


def fix_null_values_should_always_be_bad(
    c: sqlite3.Connection,
    series_name: str,
):
    """
    Null values should always be bad.
    """
    with c:
        # noinspection SqlResolve
        value_count = c.execute(
            f"SELECT COUNT(val) FROM {safe_data_table_name_for_series(series_name)} "
            f"WHERE val IS NULL "
            f"AND ("
            f"  (quality & {UNCERTAIN_MASK}) = {UNCERTAIN_MASK}"
            f"  OR (quality & {GOOD_MASK}) = {GOOD_MASK}"
            f");"
        ).fetchone()[0]

        if value_count:
            logger.info(
                "Fixing Quality of %i null points for %s", value_count, series_name
            )

        # noinspection SqlResolve
        c.execute(
            f"UPDATE {safe_data_table_name_for_series(series_name)} "
            f"SET quality = {QualityCodes.BAD_NON_SPECIFIC} "
            f"WHERE val IS NULL "
            f"AND ("
            f"  (quality & {UNCERTAIN_MASK}) = {UNCERTAIN_MASK}"
            f"  OR (quality & {GOOD_MASK}) = {GOOD_MASK}"
            f");"
        )


def fix_stream_start_and_end(
    c: sqlite3.Connection,
    series_name: str,
):
    """
    In order for aggregation functions to work, stream end needs to be
    an uncertain value. That occurs before a stream start.  The default
    stream end is inserted at the time of stream close with a None
    value.  This code will ensure these exist after every stream termination.

    Change 0x2C0 (704) to 0x140 (320) (Uncertain stream end)
    """
    with c:
        old_good_stream_end = 0x000002C0

        # noinspection SqlResolve
        value_count = c.execute(
            f"SELECT COUNT(val) FROM {safe_data_table_name_for_series(series_name)} "
            f"WHERE quality = {old_good_stream_end};"
        ).fetchone()[0]

        if value_count:
            logger.info(
                "Refactoring of %i stream end codes for %s", value_count, series_name
            )

        # noinspection SqlResolve
        c.execute(
            f"UPDATE {safe_data_table_name_for_series(series_name)} "
            f"SET quality = {QualityCodes.BAD_STREAM_END} "
            f"WHERE quality = {old_good_stream_end};"
        )

        # noinspection SqlResolve
        data = c.execute(
            f"SELECT ts, val, quality FROM {safe_data_table_name_for_series(series_name)} "
            f"ORDER BY ts;"
        ).fetchmany(100000)

        missing_stream_starts = []
        missing_stream_ends = []
        duplicate_stream_ends = []
        duplicate_stream_starts = []

        last_point = None
        for point in data:
            if last_point is None:
                pass
            elif (
                last_point[2] in QualityCodes.stream_end_codes
                or point[2] in QualityCodes.stream_start_codes
            ):
                # At a stream boundary
                if (
                    last_point[2] in QualityCodes.stream_start_codes
                    and point[2] in QualityCodes.stream_start_codes
                ):
                    # Duplicate stream start
                    duplicate_stream_starts.append(last_point)
                elif (
                    last_point[2] in QualityCodes.stream_end_codes
                    and point[2] in QualityCodes.stream_end_codes
                ):
                    # Duplicate stream ends
                    duplicate_stream_ends.append(point)
                elif last_point[2] not in QualityCodes.stream_end_codes:
                    missing_stream_ends.append(last_point)
                elif point[2] not in QualityCodes.stream_start_codes:
                    missing_stream_starts.append(last_point)

            last_point = point

        if missing_stream_ends:
            logger.info(
                "Fixing Missing stream end for %i boundaries for %s",
                len(missing_stream_ends),
                series_name,
            )

        new_values = []
        for missing_end in missing_stream_ends:
            new_values.append((missing_end[0] + 1, None, QualityCodes.BAD_STREAM_END))

        add_many_values(c, series_name, new_values)

        if missing_stream_starts:
            logger.info(
                "Fixing Missing stream starts for %i boundaries for %s",
                len(missing_stream_starts),
                series_name,
            )

        new_values = []
        for missing_start in missing_stream_starts:
            new_values.append(
                (
                    missing_start[0] - 1,
                    missing_start[1],
                    QualityCodes.UNCERTAIN_STREAM_START,
                )
            )

        add_many_values(c, series_name, new_values)

        if duplicate_stream_starts:
            logger.info(
                "Fixing duplicate stream starts for %i boundaries for %s",
                len(duplicate_stream_starts),
                series_name,
            )
        for p in duplicate_stream_starts:
            delete_value(c, series_name, p[0])

        if duplicate_stream_ends:
            logger.info(
                "Fixing duplicate stream ends for %i boundaries for %s",
                len(duplicate_stream_ends),
                series_name,
            )
        for p in duplicate_stream_ends:
            delete_value(c, series_name, p[0])


# #12
# def scan_stream_interruptions(
#     c: sqlite3.Connection,
#     series_name: str,
# ):
#     # This is for future
#     pass


def fix_all_series(c: sqlite3.Connection):
    for s in get_series_list(c):
        fix_change_of_quality_default(c, s["name"])
        fix_null_values_should_always_be_bad(c, s["name"])
        fix_stream_start_and_end(c, s["name"])
        # scan_stream_interruptions(c, s["name"])


def clean_db(c: sqlite3.Connection):
    """
    This performs an index cleanup and re-compaction.
    """
    with c:
        c.execute("VACUUM")


if __name__ == "__main__":
    print("Caution:  Read the module docs, this may not apply to your DB.")
    input("Press enter to continue")
    logging.basicConfig(level=logging.DEBUG)

    def main():
        path = pathlib.Path(sys.argv[1])
        print(path)
        with db_connection(path) as c:
            fix_all_series(c)

    main()
