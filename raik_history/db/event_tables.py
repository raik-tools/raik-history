# Copyright: (c) 2022, Gary Thompson <coding@garythompson.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
"""
The scope of this module is interfacing with the series table and
connecting to the associated data table (but not processing the data
table).
"""
import sqlite3
from typing import TypedDict, Optional, NoReturn

from raik_history.db.migration import (
    EVENT_TYPE_TABLE_NAME,
    EVENT_TABLE_NAME,
)
from raik_history.quality import QualityCodes, GOOD_MASK
from raik_history.series.constants import Quality


class EventType(TypedDict):
    event_type_id: int
    name: str
    description: str
    time_type: str
    time_stamp_type: str


class EventInterface(TypedDict):
    event_type_id: int
    name: str
    description: str
    time_type: str
    time_stamp_type: str
    start_ts: int
    end_ts: Optional[int]
    comments: Optional[str]
    quality: int


def get_event_types(c: sqlite3.Connection) -> list[EventType]:
    """
    Retrieves a list of all event types
    """
    with c:
        # noinspection SqlResolve
        ret = c.execute(
            f"SELECT id, name, description, time_type, time_stamp_type "
            f"FROM {EVENT_TYPE_TABLE_NAME};"
        ).fetchall()
    return [
        {
            "event_type_id": x[0],
            "name": x[1],
            "description": x[2],
            "time_type": x[3],
            "time_stamp_type": x[4],
        }
        for x in ret
    ]


def get_event_type_by(
    c: sqlite3.Connection, name: str = None, type_id: int = None
) -> EventType:
    """
    Retrieves a single event type using a name or an id
    """
    if name is not None and type_id is not None:
        raise NotImplementedError("Searching with both name and id is not supported")
    elif name:
        where_clause = f"WHERE name='{name}'"
    elif type_id is not None:
        where_clause = f"WHERE id='{type_id}'"
    else:
        raise ValueError("Either a name or id must be specified")

    with c:
        # noinspection SqlResolve
        ret = c.execute(
            f"SELECT id, name, description, time_type, time_stamp_type "
            f"FROM {EVENT_TYPE_TABLE_NAME} {where_clause};"
        ).fetchone()
    return {
        "event_type_id": ret[0],
        "name": ret[1],
        "description": ret[2],
        "time_type": ret[3],
        "time_stamp_type": ret[4],
    }


def get_or_create_event_id(
    c: sqlite3.Connection,
    name: str,
    description: str,
    time_type: str,
    time_stamp_type: str,
) -> int:
    """
    Retrieves the id for the event of the given name.  If the event
    does not exist, this will create an event with the provided fields.
    """
    with c:
        # noinspection SqlResolve
        ret = c.execute(
            f"SELECT id FROM {EVENT_TYPE_TABLE_NAME} WHERE name='{name}';"
        ).fetchall()
        if len(ret) > 0:
            return ret[0][0]
        else:
            # noinspection SqlResolve
            c.execute(
                f"INSERT INTO {EVENT_TYPE_TABLE_NAME}("
                f"name, "
                f"description, "
                f"time_type, "
                f"time_stamp_type"
                f") VALUES (?, ?, ?, ?)",
                (name, description, time_type, time_stamp_type),
            )
            ret = c.execute("SELECT last_insert_rowid()").fetchone()[0]
            c.commit()
            return ret


def get_event_id(
    c: sqlite3.Connection,
    name: str,
) -> int:
    with c:
        # noinspection SqlResolve
        ret = c.execute(
            f"SELECT id FROM {EVENT_TYPE_TABLE_NAME} WHERE name='{name}';"
        ).fetchall()
        if len(ret) > 0:
            return ret[0][0]
        else:
            raise KeyError(name)


def create_event(
    c: sqlite3.Connection,
    event_type_id: int,
    start_ts: int,
    end_ts: Optional[int] = None,
    comments: str = "",
    quality: Quality = QualityCodes.UNCERTAIN_NON_SPECIFIC,
) -> NoReturn:
    with c:
        # noinspection SqlResolve
        c.execute(
            f"INSERT INTO {EVENT_TABLE_NAME}("
            f"{EVENT_TYPE_TABLE_NAME}_id, "
            f"start_ts, "
            f"end_ts, "
            f"comments, "
            f"quality"
            f") VALUES (?, ?, ?, ?, ?)",
            (event_type_id, start_ts, end_ts, comments, quality),
        )
        c.commit()


def get_event(
    c: sqlite3.Connection,
    event_type_id: int,
    start_ts: int,
) -> Optional[EventInterface]:
    with c:
        # noinspection SqlResolve
        ret = c.execute(
            f"SELECT id, name, description, time_type, time_stamp_type, "
            f"start_ts, end_ts, comments, quality "
            f"FROM {EVENT_TABLE_NAME} "
            f"INNER JOIN {EVENT_TYPE_TABLE_NAME} "
            f"ON {EVENT_TYPE_TABLE_NAME}.id = {EVENT_TABLE_NAME}.{EVENT_TYPE_TABLE_NAME}_id "
            f"WHERE id=? AND start_ts=?",
            (event_type_id, start_ts),
        ).fetchone()
        if ret is None:
            return None
        else:
            return EventInterface(
                event_type_id=ret[0],
                name=ret[1],
                description=ret[2],
                time_type=ret[3],
                time_stamp_type=ret[4],
                start_ts=ret[5],
                end_ts=ret[6],
                comments=ret[7],
                quality=ret[8],
            )


def update_event(
    c: sqlite3.Connection,
    event_type_id: int,
    start_ts: int,
    end_ts: Optional[int] = None,
    comments: Optional[str] = None,
    quality: Optional[Quality] = None,
) -> NoReturn:
    """
    Event type ID and start_ts are used to identify an event.
    Only specified fields are updated.  Fields can not be set to Null once
    set previously.
    """

    update_fields = []
    update_vals = []
    if end_ts is not None:
        update_fields.append("end_ts=?")
        update_vals.append(end_ts)
    if comments is not None:
        update_fields.append("comments=?")
        update_vals.append(comments)
    if quality is not None:
        update_fields.append("quality=?")
        update_vals.append(quality)

    if not update_fields:  # pragma:nocover
        return

    with c:
        sql_parts = [
            f"UPDATE {EVENT_TABLE_NAME} ",
            f"SET {', '.join(update_fields)} "
            f"WHERE {EVENT_TYPE_TABLE_NAME}_id=? AND start_ts=?",
        ]
        c.execute(
            "".join(sql_parts),
            update_vals + [event_type_id, start_ts],
        )
        c.commit()


def get_multiple_events(
    c: sqlite3.Connection,
    event_type_id: int,
    start_ts__gte: Optional[int] = None,
    start_ts__lt: Optional[int] = None,
    comments__contains: Optional[str] = None,
    count: Optional[int] = None,
    chronological_order=True,
    only_good_quality=True,
) -> list[EventInterface]:
    """
    This is indirectly tested by the wrapper function on get_events in the Event base class
    for convenience.
    """
    with c:
        filter_qry = ["id=?"]
        params = [event_type_id]
        if start_ts__gte is not None:
            filter_qry.append("start_ts >= ?")
            params.append(start_ts__gte)
        if start_ts__lt is not None:
            filter_qry.append("start_ts < ?")
            params.append(start_ts__lt)
        if comments__contains is not None:
            filter_qry.append("comments LIKE ?")
            params.append(f"%{comments__contains}%")
        if only_good_quality:
            filter_qry.append(f"quality & {GOOD_MASK} = {GOOD_MASK}")
            filter_qry.append(f"quality < {QualityCodes.BAD_STREAM_END}")

        # noinspection SqlResolve
        base_sql = (
            f"SELECT id, name, description, time_type, time_stamp_type, "
            f"start_ts, end_ts, comments, quality "
            f"FROM {EVENT_TABLE_NAME} "
            f"INNER JOIN {EVENT_TYPE_TABLE_NAME} "
            f"ON {EVENT_TYPE_TABLE_NAME}.id = {EVENT_TABLE_NAME}.{EVENT_TYPE_TABLE_NAME}_id"
        )
        filter_sql = f"WHERE {' AND '.join(filter_qry)}"
        if count:
            count_sql = f"LIMIT ?"
            params.append(count)
        else:
            count_sql = ""

        if chronological_order:
            order_sql = f"ORDER BY start_ts"
        else:
            order_sql = f"ORDER BY start_ts DESC"

        ret = c.execute(
            " ".join([base_sql, filter_sql, order_sql, count_sql, ";"]), params
        ).fetchall()

        return [
            EventInterface(
                event_type_id=x[0],
                name=x[1],
                description=x[2],
                time_type=x[3],
                time_stamp_type=x[4],
                start_ts=x[5],
                end_ts=x[6],
                comments=x[7],
                quality=x[8],
            )
            for x in ret
        ]
