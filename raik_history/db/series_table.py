# Copyright: (c) 2022, Gary Thompson <coding@garythompson.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
"""
The scope of this module is interfacing with the series table and
connecting to the associated data table (but not processing the data
table).
"""
import json
import sqlite3

from typing import TypedDict, Iterable, Optional, NoReturn

from raik_history.db.migration import (
    SERIES_TABLE_NAME,
    create_data_series_table,
)


class SeriesInterface(TypedDict):
    name: str
    data_type: str
    time_type: str
    time_stamp_type: str
    units: str
    differential_units: str
    integral_units: str
    storage_configuration: dict
    retrieval_configuration: dict


def get_series(c: sqlite3.Connection, series_name: str) -> SeriesInterface:
    # noinspection SqlResolve
    ret = c.execute(
        f"SELECT name, data_type, time_type, time_stamp_type, "
        f"units, differential_units, integral_units, "
        f"storage_configuration, retrieval_configuration "
        f"FROM {SERIES_TABLE_NAME} "
        f"WHERE name='{series_name}';"
    ).fetchall()
    if len(ret) == 0:
        raise ValueError(f"No series named {series_name} defined")
    else:
        return SeriesInterface(
            name=ret[0][0],
            data_type=ret[0][1],
            time_type=ret[0][2],
            time_stamp_type=ret[0][3],
            units=ret[0][4],
            differential_units=ret[0][5],
            integral_units=ret[0][6],
            storage_configuration=json.loads(ret[0][7]),
            retrieval_configuration=json.loads(ret[0][8]),
        )


def get_series_list(c: sqlite3.Connection) -> Iterable[SeriesInterface]:
    # noinspection SqlResolve
    for ret in c.execute(
        f"SELECT name, data_type, time_type, time_stamp_type, "
        f"units, differential_units, integral_units, "
        f"storage_configuration, retrieval_configuration "
        f"FROM {SERIES_TABLE_NAME};"
    ).fetchall():
        yield SeriesInterface(
            name=ret[0],
            data_type=ret[1],
            time_type=ret[2],
            time_stamp_type=ret[0][3],
            units=ret[4],
            differential_units=ret[5],
            integral_units=ret[6],
            storage_configuration=json.loads(ret[7]),
            retrieval_configuration=json.loads(ret[8]),
        )


def add_series(c: sqlite3.Connection, description: SeriesInterface) -> NoReturn:
    with c:
        # noinspection SqlResolve
        c.execute(
            f"INSERT INTO {SERIES_TABLE_NAME}("
            f"name, "
            f"data_type, "
            f"time_type, "
            f"time_stamp_type, "
            f"units, "
            f"differential_units, "
            f"integral_units, "
            f"storage_configuration,"
            f"retrieval_configuration"
            f") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
            (
                description["name"],
                description["data_type"],
                description["time_type"],
                description["time_stamp_type"],
                description["units"],
                description["differential_units"],
                description["integral_units"],
                json.dumps(description["storage_configuration"]),
                json.dumps(description["retrieval_configuration"]),
            ),
        )
        create_data_series_table(c, description)
        c.commit()


def update_series(
    c: sqlite3.Connection,
    series_name: str,
    # These fields can not be arbitrarily changed
    # name: Optional[str] = None,
    # data_type: Optional[str] = None,
    # time_type: Optional[str] = None,
    # time_stamp_type: Optional[str] = None,
    units: Optional[str] = None,
    differential_units: Optional[str] = None,
    integral_units: Optional[str] = None,
    storage_configuration: Optional[dict] = None,
    retrieval_configuration: Optional[dict] = None,
) -> NoReturn:

    update_values = []
    if units:
        update_values.append(("units", units))
    if differential_units:
        update_values.append(("differential_units", differential_units))
    if integral_units:
        update_values.append(("integral_units", integral_units))
    if storage_configuration:
        update_values.append(("storage_configuration", storage_configuration))
    if retrieval_configuration:
        update_values.append(("retrieval_configuration", retrieval_configuration))

    if not update_values:
        return

    update_str = ", ".join([f"{x[0]}=?" for x in update_values])
    update_tuple = [x[1] for x in update_values]

    # noinspection SqlResolve
    sql_statement = (
        f"UPDATE {SERIES_TABLE_NAME} "
        f"SET {update_str} "
        f"WHERE name='{series_name}';"
    )

    c.execute(
        sql_statement,
        update_tuple,
    )
