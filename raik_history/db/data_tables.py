# Copyright: (c) 2022, Gary Thompson <coding@garythompson.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import logging
import sqlite3
from typing import Optional, Generator, List

from raik_history.db.migration import safe_data_table_name_for_series
from raik_history.quality import QualityCodes, quality_is_bad
from raik_history.series.constants import DataTuple, Timestamp, Quality, Value

logger = logging.getLogger(__name__)


def coerce_bool(val) -> int:
    """
    Used to create a DB representation of a value
    """
    return 1 if val else None if val is None else 0


def coerce_bool_in_tuple(val: DataTuple) -> DataTuple:
    """
    Used to create a DB representation of a value
    """
    return val[0], 1 if val[1] else None if val[1] is None else 0, val[2]


def add_value(
    c: sqlite3.Connection,
    series_name: str,
    ts: Timestamp,
    val: Optional[float | int | str | bool],
    quality: Quality = QualityCodes.UNCERTAIN_NON_SPECIFIC,
):
    """
    :param c:  A connection object
    :param series_name:  The name of the series
    :param ts: A timestamp in integer form
    :param val:  The value for the data point, matching the data type of the series
    :param quality:  A quality flag
    :return: Nothing
    :raises sqlite3.IntegrityError:  If the timestamp already exists.
    """
    if isinstance(val, bool):
        val = coerce_bool(val)
    if val is None and not quality_is_bad(quality):
        quality = QualityCodes.BAD_NON_SPECIFIC
    with c:
        # noinspection SqlResolve
        c.execute(
            f"INSERT INTO {safe_data_table_name_for_series(series_name)} "
            f"(ts, val, quality) values (?, ?, ?)",
            (ts, val, quality),
        )


def add_many_values(c: sqlite3.Connection, series_name: str, values: list[DataTuple]):
    if not values:
        return

    if isinstance(values[0][1], bool):
        values = map(coerce_bool_in_tuple, values)
    if any(map(lambda x: x[1] is None and not quality_is_bad(x[2]), values)):
        values = map(
            lambda x: (
                x[0],
                x[1],
                x[2]
                if x[1] is not None or quality_is_bad(x[2])
                else QualityCodes.BAD_NON_SPECIFIC,
            ),
            values,
        )

    with c:
        # noinspection SqlResolve
        c.executemany(
            f"INSERT INTO {safe_data_table_name_for_series(series_name)}"
            f"(ts, val, quality) values (?, ?, ?)",
            values,
        )


def update_latest_value(
    c: sqlite3.Connection,
    series_name: str,
    ts: Timestamp,
    val: Optional[float | int | str | bool],
    quality: Quality = QualityCodes.UNCERTAIN_NON_SPECIFIC,
):
    if isinstance(val, bool):
        val = coerce_bool(val)
    if val is None and not quality_is_bad(quality):
        quality = QualityCodes.BAD_NON_SPECIFIC

    with c:
        # Get the latest value, if it does not exist just add the value
        # noinspection SqlResolve
        ret = c.execute(
            f"SELECT ts, val, quality FROM {safe_data_table_name_for_series(series_name)} "
            f"ORDER BY ts DESC;"
        ).fetchone()
        if ret:
            # Latest value exists - delete it
            # noinspection SqlResolve
            c.execute(
                f"DELETE FROM {safe_data_table_name_for_series(series_name)} WHERE ts=?",
                (ret[0],),
            )
        # noinspection SqlResolve
        c.execute(
            f"INSERT INTO {safe_data_table_name_for_series(series_name)}"
            f"(ts, val, quality) values (?, ?, ?)",
            (ts, val, quality),
        )


def delete_value(
    c: sqlite3.Connection,
    series_name: str,
    ts: Timestamp,
):
    with c:
        # noinspection SqlResolve
        c.execute(
            f"DELETE FROM {safe_data_table_name_for_series(series_name)} WHERE ts=?",
            (ts,),
        )


def query_data_values(
    c: sqlite3.Connection,
    series_name: str,
    ts__lt: Optional[Timestamp] = None,
    ts__lte: Optional[Timestamp] = None,
    ts__eq: Optional[Timestamp] = None,
    ts__gte: Optional[Timestamp] = None,
    ts__gt: Optional[Timestamp] = None,
    val__is_none: Optional[bool] = None,
    val__lt: Optional[Value] = None,
    val__lte: Optional[Value] = None,
    val__eq: Optional[Value] = None,
    val__ne: Optional[Value] = None,
    val__gte: Optional[Value] = None,
    val__gt: Optional[Value] = None,
    quality__lt: Optional[Quality] = None,
    quality__lte: Optional[Quality] = None,
    quality__eq: Optional[Quality] = None,
    quality__ne: Optional[Quality] = None,
    quality__gte: Optional[Quality] = None,
    quality__gt: Optional[Quality] = None,
    include_outer_value_start=False,
    include_outer_value_end=False,
    chronological_order=True,
    count: Optional[int] = None,
    page_size: Optional[int] = 10000,
) -> Generator[DataTuple, None, None]:
    """
    :param c: A connection to the DB
    :param series_name: The series name to query
    :param ts__lt: If None, no filter is applied.
    :param ts__lte: If None, no filter is applied.
    :param ts__eq: If None, no filter is applied.
    :param ts__gte: If None, no filter is applied.
    :param ts__gt: If None, no filter is applied.
    :param val__is_none: Value Filter, applied if not None.
    :param val__lt: Value Filter, applied if not None.
    :param val__lte: Value Filter, applied if not None.
    :param val__eq: Value Filter, applied if not None.
    :param val__ne: Value Filter, applied if not None.
    :param val__gte: Value Filter, applied if not None.
    :param val__gt: Value Filter, applied if not None.
    :param quality__lt: Value Filter, applied if not None.
    :param quality__lte: Value Filter, applied if not None.
    :param quality__eq: Value Filter, applied if not None.
    :param quality__ne: Value Filter, applied if not None.
    :param quality__gte: Value Filter, applied if not None.
    :param quality__gt: Value Filter, applied if not None.
    :param include_outer_value_start: If the initial value is not on the starting bound,
        retrieve the value before it.
    :param include_outer_value_end:  If the final value is not on the ending bound, retrieve a
        value after it (if one exists).
    :param chronological_order: True means values are returned in chronological order, false means
        reversed.
    :param count: Limit the return to this number of values.
    :param page_size: Apply retrieval paging.  If None, no paging is applied.
    """
    # noinspection SqlResolve
    base_sql = (
        f"SELECT ts, val, quality FROM {safe_data_table_name_for_series(series_name)}"
    )
    filter_qry = []
    params = []
    if ts__lt is not None:
        filter_qry.append("ts < ?")
        params.append(ts__lt)
    if ts__lte is not None:
        filter_qry.append("ts <= ?")
        params.append(ts__lte)
    if ts__eq is not None:
        filter_qry.append("ts = ?")
        params.append(ts__eq)
    if ts__gte is not None:
        filter_qry.append("ts >= ?")
        params.append(ts__gte)
    if ts__gt is not None:
        filter_qry.append("ts > ?")
        params.append(ts__gt)

    if val__is_none is not None and val__is_none:
        filter_qry.append("val IS NULL")
    elif val__is_none is not None and not val__is_none:
        filter_qry.append("val IS NOT NULL")

    if val__lt is not None:
        filter_qry.append("val < ?")
        params.append(val__lt)
    if val__lte is not None:
        filter_qry.append("val <= ?")
        params.append(val__lte)
    if val__eq is not None:
        filter_qry.append("val = ?")
        params.append(val__eq)
    if val__ne is not None:
        filter_qry.append("val != ?")
        params.append(val__ne)
    if val__gte is not None:
        filter_qry.append("val >= ?")
        params.append(val__gte)
    if val__gt is not None:
        filter_qry.append("val > ?")
        params.append(val__gt)

    if quality__lt is not None:
        filter_qry.append("quality < ?")
        params.append(quality__lt)
    if quality__lte is not None:
        filter_qry.append("quality <= ?")
        params.append(quality__lte)
    if quality__eq is not None:
        filter_qry.append("quality = ?")
        params.append(quality__eq)
    if quality__ne is not None:
        filter_qry.append("quality != ?")
        params.append(quality__ne)
    if quality__gte is not None:
        filter_qry.append("quality >= ?")
        params.append(quality__gte)
    if quality__gt is not None:
        filter_qry.append("quality > ?")
        params.append(quality__gt)

    if filter_qry:
        filter_sql = f"WHERE {' AND '.join(filter_qry)}"
    else:
        filter_sql = ""

    if chronological_order:
        order_sql = f"ORDER BY ts"
    else:
        order_sql = f"ORDER BY ts DESC"

    if count:
        count_sql = f"LIMIT ?"
        params.append(count)
    else:
        count_sql = ""

    sql_statement = " ".join([base_sql, filter_sql, order_sql, count_sql, ";"])

    with c:
        val = None
        cursor = c.execute(sql_statement, params)
        if page_size:
            ret = cursor.fetchmany(page_size)
        else:
            ret = cursor.fetchall()

        if ret:
            val = ret[0]
            if include_outer_value_start:
                start_ts = ts__gt or ts__gte or ts__eq
                if start_ts and val[0] > start_ts:
                    start_bound = get_last_or_at_value(c, series_name, start_ts)
                    if start_bound:
                        yield start_bound[0]

            yield val
            ret = ret[1:]
            if not ret and page_size:
                ret = cursor.fetchmany(page_size)
        else:
            return None

        while ret:
            for val in ret:
                yield val
            if page_size:
                ret = cursor.fetchmany(page_size)
            else:
                break

        if include_outer_value_end:
            end_ts = ts__lt or ts__lte or ts__eq
            if end_ts and (val[0] < end_ts if val else True):
                end_bound = get_at_or_next_value(c, series_name, end_ts)
                if end_bound:
                    yield end_bound[0]


def get_value_at_time(
    c: sqlite3.Connection, series_name: str, ts: Timestamp
) -> Optional[DataTuple]:
    ret = list(query_data_values(c, series_name, ts__eq=ts, count=1))
    if ret:
        return ret[0]
    else:
        return None


def get_latest_value(c: sqlite3.Connection, series_name: str) -> Optional[DataTuple]:
    ret = list(query_data_values(c, series_name, chronological_order=False, count=1))
    if ret:
        return ret[0]
    else:
        return None


def get_oldest_value(c: sqlite3.Connection, series_name: str) -> Optional[DataTuple]:
    ret = list(query_data_values(c, series_name, chronological_order=True, count=1))
    if ret:
        return ret[0]
    else:
        return None


def get_last_or_at_value(
    c: sqlite3.Connection, series_name: str, ts: Timestamp, count=1
) -> list[DataTuple]:
    """
    Returns the data tuple at the given time or just before the given time
    """
    ret = list(
        query_data_values(
            c, series_name, chronological_order=False, ts__lte=ts, count=count
        )
    )
    ret.sort()
    return ret


def get_at_or_next_value(
    c: sqlite3.Connection, series_name: str, ts: Timestamp, count=1
) -> list[DataTuple]:
    """
    Returns the data tuple at the given time or just after the given time
    """
    ret = list(
        query_data_values(
            c, series_name, ts__gte=ts, chronological_order=True, count=count
        )
    )
    return ret


def get_all_values_no_page(
    c: sqlite3.Connection,
    series_name: str,
) -> List[DataTuple]:
    """
    Returns all data without paging.
    """
    ret = list(
        query_data_values(c, series_name, chronological_order=True, page_size=None)
    )
    return ret


def get_all_values_in_range(
    c: sqlite3.Connection,
    series_name: str,
    start_time: Optional[Timestamp],
    end_time: Optional[Timestamp],
    page_size=10000,
) -> Generator[DataTuple, None, None]:
    """
    Returns all data with paging.
    """
    for data in query_data_values(
        c,
        series_name,
        ts__gte=start_time,
        ts__lt=end_time,
        chronological_order=True,
        page_size=page_size,
    ):
        yield data


def get_periodic_series(
    c: sqlite3.Connection,
    series_name: str,
    start_time_ms: Timestamp,
    period_ms: int,
    end_time_ms: Timestamp = None,
    page_size=10000,
) -> Generator[DataTuple, None, None]:
    """
    For a series that is expected to be periodic, this ensures a data point
    is returned for the period specified. A data point must be present at
    the start time or a series of no data will be returned.

    :param c:  A connection to a database
    :param series_name:  Series name
    :param start_time_ms:  The time of the first data point
    :param period_ms:  The time between data points
    :param end_time_ms:  Finish execution at or after this time has passed.  If None, this function
                         stops when there is no more data in the series.
    :param page_size:  Internally this function loads data in pages, this value
                       represents the number of data points to load each time.
    """
    data = query_data_values(
        c,
        series_name,
        ts__gte=start_time_ms,
        ts__lt=end_time_ms,
        chronological_order=True,
        page_size=page_size,
    )

    current_time = start_time_ms
    for ts, val, quality in data:
        # Fast-forward if needed to the current data point
        while current_time < ts:
            logger.debug("Timestamp %i is missing", current_time)
            yield current_time, None, QualityCodes.BAD_NON_SPECIFIC
            current_time += period_ms

        if ts != current_time:
            logger.debug("Timestamp %i does not match the query period", ts)
            yield current_time, None, QualityCodes.BAD_NON_SPECIFIC
        else:
            yield ts, val, quality

        current_time += period_ms

    # In case the data stream runs out prematurely, complete the data set
    # as per the query if an end time is specified.
    if end_time_ms:
        while current_time < end_time_ms:
            logger.debug("Timestamp %i is missing", current_time)
            yield current_time, None, QualityCodes.BAD_NON_SPECIFIC
            current_time += period_ms
