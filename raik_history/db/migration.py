# Copyright: (c) 2022, Gary Thompson <coding@garythompson.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
"""
Only forward migrations are supported.  These are applied automatically
on and DB connection as it is expected that migrations will be quite
simple as the schemas are light-weight.

.. NOTE::
    Migrations are not fully implemented, this is mostly a proof of concept
    and some scaffolding for future needs.

Take not when managing migrations on SQLIte:  https://www.sqlite.org/lang_altertable.html

"""
import logging
import sqlite3
import re
import warnings
from typing import NoReturn, List, TYPE_CHECKING

from raik_history.series.constants import (
    DATA_TYPE_REAL,
    DATA_TYPE_INT,
    DATA_TYPE_BOOL,
    DATA_TYPE_STRING,
    TIME_STAMP_TYPE_LOCAL,
)

if TYPE_CHECKING:
    from raik_history.db.series_table import SeriesInterface

logger = logging.getLogger(__name__)

MIGRATION_VERSION = 5
META_TABLE_NAME = "raik_history"
SERIES_TABLE_NAME = "series"
EVENT_TYPE_TABLE_NAME = "event_type"
EVENT_TABLE_NAME = "event"


def data_table_name_for_series(series_name: str) -> str:
    if " " in series_name:
        # This is to prevent possible SQL injection later, as table
        # names can't use placeholders.
        raise ValueError("Spaces can not be used in series names")
    return f"series_{series_name}"


def safe_data_table_name_for_series(series_name: str) -> str:
    return f"[{data_table_name_for_series(series_name)}]"


DATA_TABLE_SERIES_NAME_RE = r"[a-z0-9_]+"
DATA_TABLE_SERIES_NAME_MATCH = re.compile(DATA_TABLE_SERIES_NAME_RE)
DATA_TABLE_NAME_RE = f"series_(?P<name>{DATA_TABLE_SERIES_NAME_RE})"

META_TABLE = {
    1: f"""
CREATE TABLE IF NOT EXISTS {META_TABLE_NAME} (
    key TEXT PRIMARY KEY,
    value TEXT DEFAULT ''
);
"""
}

KEY_MIGRATION_VERSION = "migration_version"

# noinspection SqlResolve
SERIES_TABLE = {
    1: f"""
CREATE TABLE IF NOT EXISTS {SERIES_TABLE_NAME} (
    name TEXT PRIMARY KEY,
    data_type TEXT NOT NULL,
    time_type TEXT NOT NULL,
    time_stamp_type TEXT DEFAULT '{TIME_STAMP_TYPE_LOCAL}',
    units TEXT DEFAULT '',
    storage_configuration TEXT DEFAULT '',
    retrieval_configuration TEXT DEFAULT ''
);
""",
    3: f"""
BEGIN TRANSACTION;

ALTER TABLE {SERIES_TABLE_NAME} ADD COLUMN differential_units TEXT DEAULT '';
ALTER TABLE {SERIES_TABLE_NAME} ADD COLUMN integral_units TEXT DEAULT '';

UPDATE {SERIES_TABLE_NAME} SET differential_units='',integral_units='' 
WHERE differential_units IS NULL;

COMMIT;
""",
}

# noinspection SqlResolve
EVENT_TYPE_TABLE = {
    4: f"""
CREATE TABLE IF NOT EXISTS {EVENT_TYPE_TABLE_NAME} (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT UNIQUE,
    description TEXT DEFAULT '',
    time_type TEXT NOT NULL,
    time_stamp_type TEXT DEFAULT '{TIME_STAMP_TYPE_LOCAL}'
);
"""
}

# noinspection SqlResolve
EVENT_TABLE = {
    4: f"""
CREATE TABLE IF NOT EXISTS {EVENT_TABLE_NAME} (
    {EVENT_TYPE_TABLE_NAME}_id INTEGER NOT NULL,
    start_ts INTEGER NOT NULL,
    end_ts INTEGER,
    comments TEXT DEFAULT '',
    FOREIGN KEY ({EVENT_TYPE_TABLE_NAME}_id)
        REFERENCES {EVENT_TYPE_TABLE_NAME} (id),
    PRIMARY KEY ({EVENT_TYPE_TABLE_NAME}_id, start_ts)
);
""",
    5: f"""
ALTER TABLE {EVENT_TABLE_NAME} ADD COLUMN quality INTEGER DEFAULT 0;
""",
}

# noinspection SqlResolve
DATA_TABLES = {
    DATA_TYPE_REAL: {
        1: """
CREATE TABLE IF NOT EXISTS [%(table_name)s] (
    ts INTEGER PRIMARY KEY,
    val REAL NOT NULL,
    quality INTEGER DEFAULT 0,
    alt REAL
);
""",
        2: """
BEGIN TRANSACTION;

CREATE TABLE [new_%(table_name)s] (
    ts INTEGER PRIMARY KEY,
    val REAL,
    quality INTEGER DEFAULT 0,
    alt REAL
);

INSERT INTO [new_%(table_name)s] (ts, val, quality, alt)
SELECT ts, val, quality, alt FROM [%(table_name)s];

DROP TABLE [%(table_name)s];

ALTER TABLE [new_%(table_name)s] RENAME TO [%(table_name)s];

COMMIT;
""",
    },
    DATA_TYPE_INT: {
        1: """
CREATE TABLE IF NOT EXISTS [%(table_name)s] (
    ts INTEGER PRIMARY KEY,
    val INTEGER NOT NULL,
    quality INTEGER DEFAULT 0,
    alt INTEGER
);
""",
        2: """
BEGIN TRANSACTION;

CREATE TABLE [new_%(table_name)s] (
    ts INTEGER PRIMARY KEY,
    val INTEGER,
    quality INTEGER DEFAULT 0,
    alt INTEGER
);

INSERT INTO [new_%(table_name)s] (ts, val, quality, alt)
SELECT ts, val, quality, alt FROM [%(table_name)s];

DROP TABLE [%(table_name)s];

ALTER TABLE [new_%(table_name)s] RENAME TO [%(table_name)s];

COMMIT;
""",
    },
    DATA_TYPE_BOOL: {
        1: """
CREATE TABLE IF NOT EXISTS [%(table_name)s] (
    ts INTEGER PRIMARY KEY,
    val INTEGER,
    quality INTEGER DEFAULT 0,
    alt INTEGER
);
""",
    },
    DATA_TYPE_STRING: {
        1: """
CREATE TABLE IF NOT EXISTS [%(table_name)s] (
    ts INTEGER PRIMARY KEY,
    val TEXT,
    quality INTEGER DEFAULT 0,
    alt INTEGER
);
""",
    },
}


# noinspection SqlResolve
def get_meta_value(c: sqlite3.Connection, key: str) -> str:
    r = c.execute(f"SELECT value FROM {META_TABLE_NAME} WHERE key='{key}';").fetchone()
    return r[0]


# noinspection SqlResolve
def set_meta_value(c: sqlite3.Connection, key: str, value: str) -> NoReturn:
    r = c.execute(f"SELECT value FROM {META_TABLE_NAME} WHERE key='{key}';").fetchone()
    if r is None:
        # New DB
        c.execute(f"INSERT INTO {META_TABLE_NAME} VALUES ('{key}', '{value}');")
    elif r[0] == value:
        pass
    else:
        c.execute(
            f"UPDATE {META_TABLE_NAME} SET value = '{value}' WHERE key = '{key}';"
        )
    c.commit()


# noinspection SqlResolve
def get_migration_number(c: sqlite3.Connection) -> int:
    return int(get_meta_value(c, KEY_MIGRATION_VERSION))


# noinspection SqlResolve
def set_migration_number(c: sqlite3.Connection, target_version: int) -> NoReturn:
    set_meta_value(c, KEY_MIGRATION_VERSION, str(target_version))


def migrate(c: sqlite3.Connection, target_version: int = MIGRATION_VERSION):
    """
    This function runs through all required DB migrations.

    .. ATTENTION::
        Data Tables migration is not yet implemented.
    """
    # First check if this is a new DB

    is_new = (
        len(c.execute("SELECT name FROM sqlite_master WHERE type='table';").fetchall())
        == 0
    )
    if is_new:
        current_version = 0
    else:
        current_version = get_migration_number(c)

    if current_version < target_version:
        if not is_new:
            logger.warning("A migration is occurring, make sure to be keeping backups.")
        _execute_meta_migrations(c, current_version, target_version)
        _execute_series_migrations(c, current_version, target_version)
        _execute_events_migrations(c, current_version, target_version)

        from raik_history.db.series_table import get_series_list

        for series_info in get_series_list(c):
            _execute_data_tables_migrations(
                c, series_info, current_version, target_version
            )

        set_migration_number(c, target_version)
    else:  # pragma: nocover
        # Nothing to do
        pass


def _execute_meta_migrations(
    c: sqlite3.Connection, db_version: int, target_version: int
):
    migration_list = [
        META_TABLE[i]
        for i in sorted(META_TABLE.keys())
        if i > db_version <= target_version
    ]
    if len(migration_list) > 1:  # pragma: nocover
        warnings.warn("Migrations of meta table not tested", FutureWarning)

    for m in migration_list:
        c.execute(m)
        c.commit()


def _execute_series_migrations(
    c: sqlite3.Connection, db_version: int, target_version: int
):
    migration_list = [
        SERIES_TABLE[i]
        for i in sorted(SERIES_TABLE.keys())
        if i > db_version <= target_version
    ]
    if len(migration_list) > 1:  # pragma: nocover
        warnings.warn("Migrations of the series table not unit tested", FutureWarning)

    for m in migration_list:
        c.executescript(m)
        c.commit()


def _execute_events_migrations(
    c: sqlite3.Connection, db_version: int, target_version: int
):
    migration_list = [
        EVENT_TYPE_TABLE[i]
        for i in sorted(EVENT_TYPE_TABLE.keys())
        if i > db_version <= target_version
    ] + [
        EVENT_TABLE[i]
        for i in sorted(EVENT_TABLE.keys())
        if i > db_version <= target_version
    ]
    if len(migration_list) > 2:  # pragma: nocover
        warnings.warn("Migrations of the events table not unit tested", FutureWarning)

    for m in migration_list:
        c.executescript(m)
        c.commit()


def _execute_data_tables_migrations(
    c: sqlite3.Connection,
    series_info: "SeriesInterface",
    db_version: int,
    target_version: int,
):
    table_name = data_table_name_for_series(series_info["name"])

    migration_list = [
        DATA_TABLES[series_info["data_type"]][i] % {"table_name": table_name}
        for i in sorted(DATA_TABLES[series_info["data_type"]])
        if db_version < i <= target_version
    ]
    for m in migration_list:
        c.executescript(m)
        c.commit()


def table_list(c: sqlite3.Connection) -> List[str]:
    results = c.execute("SELECT name FROM sqlite_master WHERE type='table';")
    return [x[0] for x in results.fetchall()]


def create_data_series_table(
    c: sqlite3.Connection,
    series_info: "SeriesInterface",
    target_migration_version: int = MIGRATION_VERSION,
):
    """
    Create a series table with migrations not supported yet.
    """
    series_name = series_info["name"]
    if not DATA_TABLE_SERIES_NAME_MATCH.match(series_name):
        raise ValueError(f"Invalid series name: '{series_name}'")
    else:
        table_name = data_table_name_for_series(series_name)
        if table_name in table_list(c):
            raise ValueError(f"A data table for series {series_name} already exists.")
        else:
            _execute_data_tables_migrations(c, series_info, 0, target_migration_version)
