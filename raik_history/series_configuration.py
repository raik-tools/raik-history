# Copyright: (c) 2022, Gary Thompson <coding@garythompson.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
"""
The series configuration module captures a class whose purpose is to
define the default capture and retrieval parameters for different
styles of data series in order to support the
"fast to use with sensible defaults (batteries included)" principle.
"""
import abc
import enum
import pint
from typing import Optional


class Compression(enum.Enum):
    NONE = "none"
    DEVIATION = "deviation"
    SWINGING_DOOR = "swinging_door_algorithm"


class SeriesConfiguration(abc.ABC):
    """
    The base class for all configuration.  This describes the common
    interface for describing series.

    :ivar compression:  The compression algorithm to use for this series
    :ivar acquisition_accuracy_ms:  All sensor sources will have delays
        in capturing and transporting data.  This value provides an
        indication of a realistic minimum time when data acquisition
        delays are negligible. Ideally this should be 100 to 1000 times
        larger than the total delays to where the timestamp is applied.
        You also need to consider clock inaccuracies and the quality of
        time synchronisation across the network in forming this number.
    """

    _default_compression = Compression.NONE
    _default_acquisition_accuracy_ms = 500.0

    def __init__(
        self,
        units_of_measure: Optional[pint.Unit] = None,
        compression: Compression = None,
        acquisition_accuracy_ms: float = None,
    ):
        self.units_of_measure = units_of_measure
        self.compression = compression or self._default_compression
        self.acquisition_accuracy_ms = (
            acquisition_accuracy_ms or self._default_acquisition_accuracy_ms
        )


class BooleanConfiguration(SeriesConfiguration):
    """
    The default for boolean storage
    """

    def __init__(
        self,
        true_label: str = "True",
        false_label: str = "False",
        none_allowed=True,
        value_for_none_if_none_disallowed=False,
        none_label="",
        **kwargs
    ):
        super().__init__(**kwargs)
        self.true_label = true_label
        self.false_label = false_label
        self.none_allowed = none_allowed
        self.none_label = none_label
        self.value_for_none_if_none_disallowed = value_for_none_if_none_disallowed


class IntConfiguration(SeriesConfiguration):
    """
    The default for integer storage
    """


class RealConfiguration(SeriesConfiguration):
    """
    The default for Real (floating point) storage

    :ivar required_accuracy_in_units:  This is a value in the units of
        measure for this series, which is used to determine compression
        parameters.  Any stored value for this series should be within
        this accuracy requirement.
    """

    _default_compression = Compression.SWINGING_DOOR

    def __init__(self, required_accuracy_in_units: float = 0.0, **kwargs):
        super().__init__(**kwargs)
        self.required_accuracy_in_units = required_accuracy_in_units
