import enum
import sqlite3
from typing import Optional, TypedDict

from raik_history.db.data_tables import get_last_or_at_value, get_at_or_next_value
from raik_history.db.migration import safe_data_table_name_for_series
from raik_history.quality import (
    GOOD_MASK,
    UNCERTAIN_MASK,
    quality_is_bad,
    quality_is_uncertain,
    QualityCodes,
)
from raik_history.series.constants import (
    DataTuple,
    Timestamp,
    TIME_FACTOR_TO_S,
)


def _prepare_common_filters(
    start_time: Timestamp,
    end_time: Timestamp,
    include_bad_values=False,
    include_uncertain_values=False,
    include_null=False,
):
    """
    Note:  Include bad values also includes uncertain
    """
    sql_filters = [
        f"ts >= {start_time}",
        f"ts < {end_time}",
    ]
    if not include_null:
        sql_filters.append(f"val IS NOT NULL")

    if include_bad_values:
        # No quality filtering
        pass
    elif include_uncertain_values:
        # No exclude bad values, part good bit must be set
        # This will include uncertain and good due to the mask relationship
        sql_filters.append(f"(quality & {UNCERTAIN_MASK}) = {UNCERTAIN_MASK}")
    else:
        # Only good values
        sql_filters.append(f"(quality & {GOOD_MASK}) = {GOOD_MASK}")
    return sql_filters


def _get_db_aggregate(
    c: sqlite3.Connection,
    series_name: str,
    start_time: Timestamp,
    end_time: Timestamp,
    include_bad_values=False,
    include_uncertain_values=False,
    agg_fx: str = "MAX",
) -> Optional[DataTuple]:
    """
    Returns the first maximum value encountered
    :param c:  A connection to a database
    :param series_name:  Series name
    :param start_time:  Start time of query (inclusive), in the time format of the series
    :param end_time:  End time of query (exclusive), in the time format of the series
    :param include_bad_values:  Consider bad values in the return result
    :param include_uncertain_values:  Consider uncertain values in the return result
    :param agg_fx:  Specify which DB aggregate function to execute
    """
    with c:
        sql_filters = _prepare_common_filters(
            start_time, end_time, include_bad_values, include_uncertain_values
        )
        # noinspection SqlResolve
        cursor = c.execute(
            f"SELECT {agg_fx}(val) FROM {safe_data_table_name_for_series(series_name)} "
            f"WHERE {' AND '.join(sql_filters)};"
        )

        max_value = cursor.fetchone()[0]

        if max_value is None:
            return None
        else:

            sql_filters.append(f"val = '{max_value}'")

            # noinspection SqlResolve
            return c.execute(
                f"SELECT ts, val, quality FROM {safe_data_table_name_for_series(series_name)} t1 "
                f"WHERE {' AND '.join(sql_filters)} "
                f"ORDER BY ts DESC;"
            ).fetchone()


def get_max(
    c: sqlite3.Connection,
    series_name: str,
    start_time: Timestamp,
    end_time: Timestamp,
    include_bad_values=False,
    include_uncertain_values=False,
) -> Optional[DataTuple]:
    """
    Returns the first maximum value encountered
    :param c:  A connection to a database
    :param series_name:  Series name
    :param start_time:  Start time of query (inclusive), in the time format of the series
    :param end_time:  End time of query (exclusive), in the time format of the series
    :param include_bad_values:  Consider bad values in the return result
    :param include_uncertain_values:  Consider uncertain values in the return result
    """
    return _get_db_aggregate(
        c,
        series_name,
        start_time,
        end_time,
        include_bad_values,
        include_uncertain_values,
        agg_fx="MAX",
    )


def get_min(
    c: sqlite3.Connection,
    series_name: str,
    start_time: Timestamp,
    end_time: Timestamp,
    include_bad_values=False,
    include_uncertain_values=False,
) -> Optional[DataTuple]:
    """
    Returns the first maximum value encountered
    :param c:  A connection to a database
    :param series_name:  Series name
    :param start_time:  Start time of query (inclusive), in the time format of the series
    :param end_time:  End time of query (exclusive), in the time format of the series
    :param include_bad_values:  Consider bad values in the return result
    :param include_uncertain_values:  Consider uncertain values in the return result
    """
    return _get_db_aggregate(
        c,
        series_name,
        start_time,
        end_time,
        include_bad_values,
        include_uncertain_values,
        agg_fx="MIN",
    )


def get_count(
    c: sqlite3.Connection,
    series_name: str,
    start_time: Timestamp,
    end_time: Timestamp,
    include_bad_values=False,
    include_uncertain_values=False,
) -> Optional[DataTuple]:
    """
    Returns the first maximum value encountered
    :param c:  A connection to a database
    :param series_name:  Series name
    :param start_time:  Start time of query (inclusive), in the time format of the series
    :param end_time:  End time of query (exclusive), in the time format of the series
    :param include_bad_values:  Consider bad values in the return result
    :param include_uncertain_values:  Consider uncertain values in the return result
    """
    return _get_db_aggregate(
        c,
        series_name,
        start_time,
        end_time,
        include_bad_values,
        include_uncertain_values,
        agg_fx="COUNT",
    )


class TimeWeightedAggregateData(TypedDict):
    result: float
    units: str
    included_points: int
    excluded_points: int
    included_seconds: float
    excluded_seconds: float
    start_time: int
    end_time: int


class TimeWeightedRateConversion(enum.Enum):
    PerSecond = 1
    PerMinute = 1 / 60
    PerHour = 1 / 3600


def time_weighted_sum(
    c: sqlite3.Connection,
    series_name: str,
    time_type: str,
    start_time: Timestamp,
    end_time: Timestamp,
    rate_conversion: TimeWeightedRateConversion = TimeWeightedRateConversion.PerSecond,
    include_bad_values=False,
    include_uncertain_values=False,
    page_size: int = 10000,
    integral_units: str = "",
) -> Optional[TimeWeightedAggregateData]:
    """
    Returns the sum of this series over time.  This is simply the sum of trapezoidal
    segments along the series.  If bad or uncertain values are excluded (the default)
    the periods before the bad or uncertain are a rectangular sum and the periods after
    are excluded.  Null values are always excluded and treated as just mentioned for a
    bad or uncertain.

    :param c:  A connection to a database
    :param series_name:  Series name
    :param time_type:  The time type which is needed for the rate conversion
    :param start_time:  Start time of query (inclusive), in the time format of the series
    :param end_time:  End time of query (exclusive), in the time format of the series
    :param rate_conversion:  When integrating, adjust the data to represent the provided rate.
    :param include_bad_values:  Consider bad values in the return result
    :param include_uncertain_values:  Consider uncertain values in the return result
    :param page_size:  An internal value to reduce database calls
    :param integral_units:  The unit to report in the return data for the result of this integration
    """
    with c:
        sql_filters = _prepare_common_filters(start_time, end_time, True, True, True)

        # noinspection SqlResolve
        data = c.execute(
            f"SELECT ts, val, quality FROM {safe_data_table_name_for_series(series_name)} "
            f"WHERE {' AND '.join(sql_filters)} "
            f"ORDER BY ts;"
        ).fetchmany(page_size)

        included_points = 0
        excluded_points = 0
        included_seconds = 0
        excluded_seconds = 0
        total = 0.0
        last_point = None
        first_point = None

        for point in data:
            if last_point is None:
                first_point = point
                last_point = point
                continue

            area, width_seconds = calculate_area_between_points(
                last_point,
                point,
                time_type,
                rate_conversion,
                include_bad_values,
                include_uncertain_values,
            )

            if (
                width_seconds is None
            ):  # pragma: no cover  #  inconsequential if this fails
                excluded_points += 1
            elif area is None:  # pragma: no cover  #  inconsequential if this fails
                excluded_points += 1
                excluded_seconds += width_seconds
            else:
                included_points += 1
                included_seconds += width_seconds
                total += area

            # Clean up counting the first point
            if first_point == last_point:
                if width_seconds is None:
                    # Both points excluded
                    excluded_points += 1
                else:
                    included_points += 1

            last_point = point

        # Process boundary conditions
        outside_point = get_last_or_at_value(c, series_name, start_time)
        if not outside_point:
            # Start boundary is just excluded time
            start_boundary = (start_time, None, QualityCodes.GOOD_NON_SPECIFIC)
        else:
            start_boundary = create_interpolated_point(
                outside_point[0], first_point, start_time
            )
        _, width_seconds = calculate_area_between_points(
            start_boundary,
            first_point,
            time_type,
            rate_conversion,
        )
        excluded_seconds += width_seconds

        outside_point = get_at_or_next_value(c, series_name, end_time)
        if not outside_point:
            # Start boundary is just excluded time
            end_boundary = (end_time, None, QualityCodes.GOOD_NON_SPECIFIC)
        else:
            end_boundary = create_interpolated_point(
                last_point, outside_point[0], end_time
            )
        _, width_seconds = calculate_area_between_points(
            last_point,
            end_boundary,
            time_type,
            rate_conversion,
        )
        excluded_seconds += width_seconds

        return TimeWeightedAggregateData(
            result=total,
            units=integral_units,
            included_points=included_points,
            excluded_points=excluded_points,
            included_seconds=included_seconds,
            excluded_seconds=excluded_seconds,
            start_time=start_time,
            end_time=end_time,
        )


def calculate_area_between_points(
    start_point: Optional[DataTuple],
    end_point: Optional[DataTuple],
    time_type: str,
    rate_conversion: TimeWeightedRateConversion = TimeWeightedRateConversion.PerSecond,
    include_bad_values=False,
    include_uncertain_values=False,
) -> tuple[Optional[float], Optional[float]]:
    """
    .. NOTE::
        Some testing has shown trapezoidal to over calculate by more than 10%.
        Until different compression algorithms are applied, this will be forced
        to return rectangular.  The testing involved a dump of real data and
        closer analysis.

    If there are any issues with the start point then the calculation returns None.
    If there is no end point, the calculation returns None.

    Otherwise, the calculation is a trapezoidal calculation when both points are
    acceptable.  If the end point is not acceptable, the calculation is a
    rectangular calculation.

    :returns:  The area and the duration the area covers.
    """
    if start_point is None or end_point is None:
        return None, None

    width_seconds = (end_point[0] - start_point[0]) / TIME_FACTOR_TO_S[time_type]

    if (
        start_point[1] is None
        or (not include_bad_values and quality_is_bad(start_point[2]))
        or (not include_uncertain_values and quality_is_uncertain(start_point[2]))
    ):
        return None, width_seconds

    # noinspection PyTypeChecker
    width = width_seconds * rate_conversion.value
    force_rectangular = True

    if (
        force_rectangular
        or end_point[1] is None
        or (not include_bad_values and quality_is_bad(end_point[2]))
        or (not include_uncertain_values and quality_is_uncertain(end_point[2]))
    ):
        # last point is included but next is excluded:  Rectangular area
        # as the second point is not trusted.
        height = start_point[1]
        area = width * height
    else:
        # Trapezoidal area calculation
        # The width is the time base
        average_height = (start_point[1] + end_point[1]) / 2
        area = width * average_height

    return area, width_seconds


def create_interpolated_point(
    start_point: Optional[DataTuple], end_point: Optional[DataTuple], target_time: int
) -> Optional[DataTuple]:
    """
    Creates an interpolated point at the target time.  The quality of the point
    is taken as the lowest quality of the provided points.
    """
    if (
        start_point is None
        or end_point is None
        or target_time < start_point[0]
        or target_time > end_point[0]
    ):
        return None
    elif target_time == start_point[0]:
        return start_point
    elif target_time == end_point[0]:
        return end_point

    width = end_point[0] - start_point[0]

    if start_point[1] is None or end_point[1] is None:
        v = None
        q = QualityCodes.BAD_NON_SPECIFIC
    else:
        delta_t = target_time - end_point[0]
        diff = end_point[1] - start_point[1]

        v = end_point[1] + diff * delta_t / width

        if quality_is_bad(start_point[2]):
            q = start_point[2]
        elif quality_is_bad(end_point[2]):
            q = end_point[2]
        elif quality_is_uncertain(start_point[2]):
            q = start_point[2]
        elif quality_is_uncertain(end_point[2]):
            q = end_point[2]
        else:
            q = start_point[3]

    return target_time, v, q
