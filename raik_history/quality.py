from typing import Iterable


class OpcQualityCode:
    """
    These codes are based on the old OPC-DA standard which are still
    commonly used.  I note that OPC-UA offers a very different quality
    code standard but are not as relevant to this application.

    000000000000 (0x0000)	Bad [Non-Specific]
    000000000100 (0x0004)	Bad [Configuration Error]
    000000001000 (0x0008)	Bad [Not Connected]
    000000001100 (0x000c)	Bad [Device Failure]
    000000010000 (0x0010)	Bad [Sensor Failure]
    000000010100 (0x0014)	Bad [Last Known Value]
    000000011000 (0x0018)	Bad [Communication Failure]
    000000011100 (0x001c)	Bad [Out of Service]
    000001000000 (0x0040)	Uncertain [Non-Specific]
    000001000001 (0x0041)	Uncertain [Non-Specific] (Low Limited)
    000001000010 (0x0042)	Uncertain [Non-Specific] (High Limited)
    000001000011 (0x0043)	Uncertain [Non-Specific] (Constant)
    000001000100 (0x0044)	Uncertain [Last Usable]
    000001000101 (0x0045)	Uncertain [Last Usable] (Low Limited)
    000001000110 (0x0046)	Uncertain [Last Usable] (High Limited)
    000001000111 (0x0047)	Uncertain [Last Usable] (Constant)
    000001010000 (0x0050)	Uncertain [Sensor Not Accurate]
    000001010001 (0x0051)	Uncertain [Sensor Not Accurate] (Low Limited)
    000001010010 (0x0052)	Uncertain [Sensor Not Accurate] (High Limited)
    000001010011 (0x0053)	Uncertain [Sensor Not Accurate] (Constant)
    000001010100 (0x0054)	Uncertain [EU Exceeded]
    000001010101 (0x0055)	Uncertain [EU Exceeded] (Low Limited)
    000001010110 (0x0056)	Uncertain [EU Exceeded] (High Limited)
    000001010111 (0x0057)	Uncertain [EU Exceeded] (Constant)
    000001011000 (0x0058)	Uncertain [Sub-Normal]
    000001011001 (0x0059)	Uncertain [Sub-Normal] (Low Limited)
    000001011010 (0x005a)	Uncertain [Sub-Normal] (High Limited)
    000001011011 (0x005b)	Uncertain [Sub-Normal] (Constant)
    000011000000 (0x00c0)	Good [Non-Specific]
    000011000001 (0x00c1)	Good [Non-Specific] (Low Limited)
    000011000010 (0x00c2)	Good [Non-Specific] (High Limited)
    000011000011 (0x00c3)	Good [Non-Specific] (Constant)
    000011011000 (0x00d8)	Good [Local Override]
    000011011001 (0x00d9)	Good [Local Override] (Low Limited)
    000011011010 (0x00da)	Good [Local Override] (High Limited)
    000011011011 (0x00db)	Good [Local Override] (Constant)
    """

    BAD_NON_SPECIFIC = 0x00000000
    BAD_CONFIGURATION_ERROR = 0x00000004
    BAD_NOT_CONNECTED = 0x00000008
    BAD_DEVICE_FAILURE = 0x0000000C
    BAD_SENSOR_FAILURE = 0x00000010
    BAD_LAST_KNOWN_VALUE = 0x00000014
    BAD_COMMUNICATION_FAILURE = 0x00000018
    BAD_OUT_OF_SERVICE = 0x0000001C
    UNCERTAIN_NON_SPECIFIC = 0x00000040
    UNCERTAIN_NON_SPECIFIC_LOW_LIMITED = 0x00000041
    UNCERTAIN_NON_SPECIFIC_HIGH_LIMITED = 0x00000042
    UNCERTAIN_NON_SPECIFIC_CONSTANT = 0x00000043
    UNCERTAIN_LAST_USABLE = 0x00000044
    UNCERTAIN_LAST_USABLE_LOW_LIMITED = 0x00000045
    UNCERTAIN_LAST_USABLE_HIGH_LIMITED = 0x00000046
    UNCERTAIN_LAST_USABLE_CONSTANT = 0x00000047
    UNCERTAIN_SENSOR_NOT_ACCURATE = 0x00000050
    UNCERTAIN_SENSOR_NOT_ACCURATE_LOW_LIMITED = 0x00000051
    UNCERTAIN_SENSOR_NOT_ACCURATE_HIGH_LIMITED = 0x00000052
    UNCERTAIN_SENSOR_NOT_ACCURATE_CONSTANT = 0x00000053
    UNCERTAIN_EU_EXCEEDED = 0x00000054
    UNCERTAIN_EU_EXCEEDED_LOW_LIMITED = 0x00000055
    UNCERTAIN_EU_EXCEEDED_HIGH_LIMITED = 0x00000056
    UNCERTAIN_EU_EXCEEDED_CONSTANT = 0x00000057
    UNCERTAIN_SUB_NORMAL = 0x00000058
    UNCERTAIN_SUB_NORMAL_LOW_LIMITED = 0x00000059
    UNCERTAIN_SUB_NORMAL_HIGH_LIMITED = 0x0000005A
    UNCERTAIN_SUB_NORMAL_CONSTANT = 0x0000005B
    GOOD_NON_SPECIFIC = 0x000000C0
    GOOD_NON_SPECIFIC_LOW_LIMITED = 0x000000C1
    GOOD_NON_SPECIFIC_HIGH_LIMITED = 0x000000C2
    GOOD_NON_SPECIFIC_CONSTANT = 0x000000C3
    GOOD_LOCAL_OVERRIDE = 0x000000D8
    GOOD_LOCAL_OVERRIDE_LOW_LIMITED = 0x000000D9
    GOOD_LOCAL_OVERRIDE_HIGH_LIMITED = 0x000000DA
    GOOD_LOCAL_OVERRIDE_CONSTANT = 0x000000DB

    verbose_names = {
        0: "Bad [Non-Specific]",
        4: "Bad [Configuration Error]",
        8: "Bad [Not Connected]",
        12: "Bad [Device Failure]",
        16: "Bad [Sensor Failure]",
        20: "Bad [Last Known Value]",
        24: "Bad [Communication Failure]",
        28: "Bad [Out of Service]",
        64: "Uncertain [Non-Specific]",
        65: "Uncertain [Non-Specific] (Low Limited)",
        66: "Uncertain [Non-Specific] (High Limited)",
        67: "Uncertain [Non-Specific] (Constant)",
        68: "Uncertain [Last Usable]",
        69: "Uncertain [Last Usable] (Low Limited)",
        70: "Uncertain [Last Usable] (High Limited)",
        71: "Uncertain [Last Usable] (Constant)",
        80: "Uncertain [Sensor Not Accurate]",
        81: "Uncertain [Sensor Not Accurate] (Low Limited)",
        82: "Uncertain [Sensor Not Accurate] (High Limited)",
        83: "Uncertain [Sensor Not Accurate] (Constant)",
        84: "Uncertain [EU Exceeded]",
        85: "Uncertain [EU Exceeded] (Low Limited)",
        86: "Uncertain [EU Exceeded] (High Limited)",
        87: "Uncertain [EU Exceeded] (Constant)",
        88: "Uncertain [Sub-Normal]",
        89: "Uncertain [Sub-Normal] (Low Limited)",
        90: "Uncertain [Sub-Normal] (High Limited)",
        91: "Uncertain [Sub-Normal] (Constant)",
        192: "Good [Non-Specific]",
        193: "Good [Non-Specific] (Low Limited)",
        194: "Good [Non-Specific] (High Limited)",
        195: "Good [Non-Specific] (Constant)",
        216: "Good [Local Override]",
        217: "Good [Local Override] (Low Limited)",
        218: "Good [Local Override] (High Limited)",
        219: "Good [Local Override] (Constant)",
    }


class QualityCodes(OpcQualityCode):
    """
    These values are custom quality codes that use the third byte to extend
    the codes above.

    Stream Start is still experimental, but all current use cases don't have
    another data type.
    """

    GOOD_STREAM_START = 0x000001C0  # 448
    UNCERTAIN_STREAM_START = 0x00000140  # 320
    BAD_STREAM_END = 0x00000100  # 256

    ADMIN_QUALITY_STARTS = 256
    MIN_GOOD_QUALITY = 192
    MAX_GOOD_QUALITY = 255

    verbose_names = {
        **OpcQualityCode.verbose_names,
        GOOD_STREAM_START: "Good - First value for a data streaming session.",
        UNCERTAIN_STREAM_START: "Uncertain - First value for a data streaming session.",
        BAD_STREAM_END: "Uncertain - Indicates the stream has closed.",
    }

    stream_codes = (GOOD_STREAM_START, UNCERTAIN_STREAM_START, BAD_STREAM_END)
    stream_start_codes = (GOOD_STREAM_START, UNCERTAIN_STREAM_START)
    stream_end_codes = (BAD_STREAM_END,)


GOOD_MASK = 0b11000000
UNCERTAIN_MASK = 0b01000000


def quality_is_good(v: int) -> bool:
    return (v & GOOD_MASK) == GOOD_MASK


def quality_is_uncertain(v: int) -> bool:
    return (v & UNCERTAIN_MASK) == UNCERTAIN_MASK and (v & GOOD_MASK) != GOOD_MASK


def quality_is_bad(v: int) -> bool:
    return not (quality_is_good(v) or quality_is_uncertain(v))


def worst_quality(v: Iterable[int]) -> int:
    """
    Takes a list of quality codes and returns the worst but more specific quality of the set
    """
    good_codes = []
    uncertain_codes = []
    bad_codes = []
    for i in v:
        if quality_is_good(i):
            good_codes.append(i)
        elif quality_is_uncertain(i):
            uncertain_codes.append(i)
        else:
            bad_codes.append(i)
    if bad_codes:
        return max(bad_codes)
    elif uncertain_codes:
        return max(uncertain_codes)
    else:
        return max(good_codes)
