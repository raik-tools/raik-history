# Copyright: (c) 2022, Gary Thompson <coding@garythompson.au>
# SPDX-License-Identifier: MIT
"""
Initialises the Units of Measure and creates a brief interface for
integrating Pint into configuration and other aspects.

The Unit registry is abbreviated to simplify regular use
"""
import pint

U = pint.UnitRegistry()

U.define("ratio = []")
U.define("percent = 100 * ratio = %")


def unit_to_string(u: pint.Unit) -> str:
    return str(u)


def string_to_unit(u: str) -> pint.Unit:
    return U(u).units


def convert_units(qty: float, source_unit: str, target_unit: str) -> float:
    """
    A conservative start to managing unit conversion.
    """
    val = qty * string_to_unit(source_unit)
    new_val = val.to(target_unit)
    return new_val.magnitude


def specify_absolute_error(absolute_error: str):
    """
    This is a decorator.

    :param absolute_error: A string representing the expected absolute error of the return value
    :return:
    """

    def decorator(func):
        func.absolute_error = absolute_error
        return func

    return decorator
