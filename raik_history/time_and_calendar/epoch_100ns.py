# Copyright: (c) 2022, Gary Thompson <coding@garythompson.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
"""
This was going to use a holocene reference for academic interest, but
the impracticalities of calculating this precisely (and avoiding an
ambiguous datum) was too challenging.  Instead, the Epoch is the standard
datum.

Refer to `decision.rst` for more information.
"""
import datetime
import time

from raik_history.time_and_calendar.time_delta import total_timedelta_microseconds
from raik_history.time_and_calendar.constants import (
    EPOCH,
)
from raik_history.time_and_calendar.time_zone import is_aware


def now_epoch_100ns() -> int:
    """
    Returns 100ns increments for the current time
    """
    epoch_time = time.time_ns() // 100
    return epoch_time


def datetime_to_epoch_100ns(v: datetime.datetime) -> int:
    """
    Returns ms for the time aware datetime provided.
    """
    assert is_aware(v)
    microseconds = total_timedelta_microseconds(v - EPOCH)
    return microseconds * 10


__all__ = ["now_epoch_100ns", "datetime_to_epoch_100ns"]
