# Copyright: (c) 2022, Gary Thompson <coding@garythompson.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
"""
Any function that results in a datetime object or exclusively uses datetime
objects are defined here
"""
import datetime
import zoneinfo

from raik_history.time_and_calendar.constants import EPOCH
from raik_history.time_and_calendar.base import now_epoch_ms
from raik_history.time_and_calendar.time_zone import get_current_timezone, is_aware


def create_time_aware(
    year=0, month=0, day=0, hour=0, minute=0, second=0, microsecond=0, zone_string="UTC"
) -> datetime.datetime:
    tz = zoneinfo.ZoneInfo(zone_string)
    dt = datetime.datetime(
        year=year,
        month=month,
        day=day,
        hour=hour,
        minute=minute,
        second=second,
        microsecond=microsecond,
        tzinfo=tz,
    )
    return dt


def create_local_time(
    year=0, month=0, day=0, hour=0, minute=0, second=0, microsecond=0
) -> datetime.datetime:
    tz = get_current_timezone()
    dt = datetime.datetime(
        year=year,
        month=month,
        day=day,
        hour=hour,
        minute=minute,
        second=second,
        microsecond=microsecond,
        tzinfo=tz,
    )
    return dt


def now_datetime() -> datetime.datetime:
    """
    Returns ms for the current time
    """
    return epoch_ms_to_datetime(now_epoch_ms())


def epoch_ms_to_datetime(epoch_ms: int) -> datetime.datetime:
    tz = get_current_timezone()
    dt = EPOCH + datetime.timedelta(milliseconds=epoch_ms)
    return dt.astimezone(tz)


def datetime_to_string(v: datetime.datetime) -> str:
    """
    This must only be used for display purposes, not data transfer.  I'm
    really only adding this for expediency instead of interpreting the ms
    value in the front end (which needs an extension to Fields Schema)
    """
    assert is_aware(v)
    if v.tzinfo.tzname(v):
        return v.strftime("%Y-%m-%d %H:%M:%S %Z")
    else:
        return v.strftime("%Y-%m-%d %H:%M:%S %z")  # pragma: no cover


def iso_string_to_datetime(v: str) -> datetime.datetime:
    """
    Decodes a string of the following format:  2022-08-23T12:26:00.0
    The time is assumed to be UTC time.  This is not fully implemented,
    see here for examples:
    https://en.wikipedia.org/wiki/ISO_8601

    This is intentionally developed in pure python to minimise dependencies
    """
    parts = v.split("T")
    if len(parts) == 1:
        year, month, day = parts[0].split("-")
        hour = 0
        minute = 0
        seconds = 0
        microseconds = 0
        tz = get_current_timezone()
    else:
        date_part, time_part = parts
        year, month, day = date_part.split("-")
        hour, minute, time_end = time_part.split(":", maxsplit=2)

        if time_end[-1] == "Z":
            seconds = time_end[:-1]
            offset = "+0000"
        elif "+" in time_end:
            seconds, offset = time_end.split("+")
            offset = f"+{offset}"
        elif "-" in time_end:
            seconds, offset = time_end.split("-")
            offset = f"-{offset}"
        else:
            seconds = time_end
            offset = "+0000"

        if "." in seconds:
            seconds, part_seconds = seconds.split(".")
            microseconds = int(float(f"0.{part_seconds}") * 1000000)
        else:
            seconds = seconds
            microseconds = 0

        offset_hours = int(offset[:3])
        offset_minutes = int(offset[-2:])
        tz = datetime.timezone(
            datetime.timedelta(hours=offset_hours, minutes=offset_minutes)
        )

    dt = datetime.datetime(
        year=int(year),
        month=int(month),
        day=int(day),
        hour=int(hour),
        minute=int(minute),
        second=int(seconds),
        microsecond=microseconds,
        tzinfo=tz,
    )
    return dt


__all__ = [
    "create_time_aware",
    "create_local_time",
    "now_datetime",
    "epoch_ms_to_datetime",
    "datetime_to_string",
    "iso_string_to_datetime",
]
