# Copyright: (c) 2022, Gary Thompson <coding@garythompson.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import datetime

EPOCH = datetime.datetime(1970, 1, 1, tzinfo=datetime.timezone.utc)
HOURS_24_IN_MS = 24 * 60 * 60 * 1000
MICROSECONDS_IN_A_DAY = 24 * 3600 * 10**6
ENV_VAR_TIME_ZONE = "TIME_ZONE"

__all__ = [
    "EPOCH",
    "HOURS_24_IN_MS",
    "ENV_VAR_TIME_ZONE",
    "MICROSECONDS_IN_A_DAY",
]

if __name__ == "__main__":  # pragma:  no cover

    def main():
        """
        This is used to print out calculated constants and avoid imports
        in this file (as imports will result in circular dependencies).
        """
        from raik_history.time_and_calendar.time_delta import (
            total_timedelta_microseconds,
        )

        # noinspection PyPep8Naming
        MILLENNIUM = datetime.datetime(2000, 1, 1, tzinfo=datetime.timezone.utc)
        # noinspection PyPep8Naming
        COMMON_ERA = datetime.datetime(1, 1, 1, tzinfo=datetime.timezone.utc)

        # Calculated as total_timedelta_microseconds(EPOCH - COMMON_ERA) * 10
        # EPOCH_TO_COMMON_ERA_100NS = 621355968000000000
        # noinspection PyPep8Naming
        MILLENNIUM_TO_COMMON_ERA_100NS = 630822816000000000

        # -10000 BCE - this is a crude measurement since the datetime class can't
        # go this far back but assumes the all leap adjustments are included.
        # noinspection PyPep8Naming
        HOLOCENE_FROM_COMMON_ERA_100NS = MILLENNIUM_TO_COMMON_ERA_100NS * 5

        print(
            f"HOLOCENE_FROM_COMMON_ERA_100NS="
            f"{total_timedelta_microseconds(EPOCH - COMMON_ERA) * 10}"
        )
        print(
            f"MILLENNIUM_TO_COMMON_ERA_100NS="
            f"{total_timedelta_microseconds(MILLENNIUM - COMMON_ERA) * 10}"
        )
        # See https://en.wikipedia.org/wiki/Gregorian_calendar
        print(f"RANGE_OF_100NS_IN_YEARS=" f"{int(2**64/(10**7)/3600/24/365.24225)}")
        print(f"RANGE_OF_US_IN_YEARS=" f"{int(2**64/(10**6)/3600/24/365.24225)}")
        print(f"RANGE_OF_MS_IN_YEARS=" f"{int(2**64/(10**3)/3600/24/365.24225)}")
        print(f"RANGE_OF_CS_IN_YEARS=" f"{int(2**64/(10**2)/3600/24/365.24225)}")
        print(f"RANGE_OF_DS_IN_YEARS=" f"{int(2**64/10/3600/24/365.24225)}")
        print(f"RANGE_OF_S_IN_YEARS=" f"{int(2**64/1/3600/24/365.24225)}")

        # https://docs.python.org/3/library/datetime.html#technical-detail
        # See point 4:  Datetime does not support leap seconds.
        # EPOCH ignores leap seconds:  https://docs.python.org/3/library/time.html#module-time
        # https://pumas.nasa.gov/examples/how-many-days-are-year
        print(
            f"Calculation test:  {HOLOCENE_FROM_COMMON_ERA_100NS} "
            f"vs {int(10000*365.24225*24*3600)*10**7}"
        )

        print(
            f"Calculation test:  {MILLENNIUM_TO_COMMON_ERA_100NS} "
            f"vs {int(1999*365.24225*24*3600)*10**7}"
        )

        # This last code is to see how often the date 22-2-22 Falls on a Tuesday
        years = []
        for i in range(9999):
            dt = datetime.datetime(i + 1, 2, 22)
            if dt.weekday() == 1 and dt.year % 100 == 22:
                print(dt)
                years.append(str(dt.year))
        print(", ".join(years))
        print(f"{len(years)} occurrences in 9999 years.")

    main()
