# Copyright: (c) 2022, Gary Thompson <coding@garythompson.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import datetime
import os
import time
import zoneinfo
import functools
from typing import Union

from raik_history.time_and_calendar.constants import ENV_VAR_TIME_ZONE


@functools.cache
def get_current_timezone() -> Union[zoneinfo.ZoneInfo, datetime.timezone]:
    """
    Attempts to identify the current timezone.  This is not the same
    as the UTC offset as a timezone may need to be calendar aware (such
    as transitioning between standard and daylight savings time).  As
    a fall back, the system offset will be used.
    """
    tz_string = os.environ.get(ENV_VAR_TIME_ZONE)
    if not tz_string:
        offset = time.localtime().tm_gmtoff
        td = datetime.timedelta(seconds=offset)
        return datetime.timezone(offset=td)
    else:
        return zoneinfo.ZoneInfo(tz_string)


def is_aware(d: datetime.datetime) -> bool:
    """
    This comes straight from:
    https://docs.python.org/3/library/datetime.html#determining-if-an-object-is-aware-or-naive
    """
    return d.tzinfo is not None and d.utcoffset() is not None


__all__ = ["get_current_timezone", "is_aware"]
