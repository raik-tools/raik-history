import datetime
from raik_history.time_and_calendar import MICROSECONDS_IN_A_DAY


def total_timedelta_microseconds(v: datetime.timedelta) -> int:
    """
    Microseconds is the highest resolution on timedelta and this function
    ensures there is no floating point errors introduced.
    """
    days_u = v.days * MICROSECONDS_IN_A_DAY
    return days_u + v.seconds * 10 ** 6 + v.microseconds
