"""
This module exists to avoid circular dependencies on commonly
used functions.
"""
import time


def now_epoch_ms() -> int:
    """
    Returns ms for the current time
    """
    return int(time.time_ns() // 10**6)
