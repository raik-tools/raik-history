# Copyright: (c) 2022, Gary Thompson <coding@garythompson.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
"""
Any function that results in an epoch_ms value or exclusively uses
epoch_ms values are defined here
"""
import datetime

from raik_history.time_and_calendar.base import now_epoch_ms
from raik_history.time_and_calendar.date_time import iso_string_to_datetime
from raik_history.time_and_calendar.time_delta import total_timedelta_microseconds
from raik_history.time_and_calendar.constants import EPOCH
from raik_history.time_and_calendar.time_zone import is_aware


def datetime_to_epoch_ms(v: datetime.datetime) -> int:
    """
    Returns ms for the time aware datetime provided.
    """
    assert is_aware(v)
    total_microseconds = total_timedelta_microseconds(v - EPOCH)
    return total_microseconds // 1000


def iso_string_to_epoch_ms(v: str) -> int:
    """
    Decodes a string of the following format:  2022-08-23T12:26:00.0
    See iso_string_to_datetime for more details
    """
    dt = iso_string_to_datetime(v)
    return datetime_to_epoch_ms(dt)


def seconds_apart(a: int, b: int) -> float:
    """Returns the absolute number of seconds between a and b"""
    return abs(a - b) / 1000.0


__all__ = [
    "now_epoch_ms",
    "datetime_to_epoch_ms",
    "iso_string_to_epoch_ms",
    "seconds_apart",
]
