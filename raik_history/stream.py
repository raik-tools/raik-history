# Copyright: (c) 2022, Gary Thompson <coding@garythompson.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
"""
The stream interface is a set of convenience tools to support both
continual updates and on the fly compression and state handling.
"""
import logging
import pathlib
import sqlite3
import time
from contextlib import contextmanager
from typing import NoReturn

from raik_history.compression import ChangeOnlyCompressor
from raik_history.db.connection import db_connection
from raik_history.db.data_tables import (
    add_value,
    update_latest_value,
)
from raik_history.db.series_table import get_series
from raik_history.quality import QualityCodes, quality_is_good, quality_is_uncertain
from raik_history.series.constants import (
    DataTuple,
    TIME_TYPE_EPOCH_MS,
    TIME_TYPE_EPOCH_100NS,
)
from raik_history.time_and_calendar import now_epoch_100ns, now_epoch_ms

logger = logging.getLogger(__name__)


class StreamCompressor(ChangeOnlyCompressor):
    def __init__(
        self,
        connection,
        series_name: str,
        repeat_period: int = 7200,
        time_type: str = TIME_TYPE_EPOCH_MS,
    ):
        super().__init__(repeat_period=repeat_period, time_type=time_type)
        self.series_name = series_name
        self.connection = connection

    def on_new_value(self, val: DataTuple):
        try:
            add_value(
                c=self.connection,
                series_name=self.series_name,
                ts=val[0],
                val=val[1],
                quality=val[2],
            )
        except sqlite3.IntegrityError:
            pass

    def on_update_value(self, val: DataTuple):
        update_latest_value(
            c=self.connection,
            series_name=self.series_name,
            ts=val[0],
            val=val[1],
            quality=val[2],
        )


class StreamInterface:
    """
    This interface preserves the current state of each series
    along with shortcuts for adding values to the db.
    """

    NOW_FX = {TIME_TYPE_EPOCH_MS: now_epoch_ms, TIME_TYPE_EPOCH_100NS: now_epoch_100ns}

    def __init__(
        self,
        connection,
        series_list: list[str],
        commit_period_s=10,
        repeat_period_s=7200,
    ):
        self.connection = connection
        self.series_list = series_list
        self._compressors: dict[str, StreamCompressor] = {}
        self._data_types: dict[str, str] = {}
        self.commit_period_s = commit_period_s
        self.repeat_period_s = repeat_period_s
        self.next_commit = time.time() + self.commit_period_s

        # Cache series data types and last values
        for s_name in series_list:
            s = get_series(connection, s_name)
            self._data_types[s_name] = s["data_type"]
            self._compressors[s_name] = StreamCompressor(
                connection=connection,
                series_name=s_name,
                repeat_period=repeat_period_s,
                time_type=s["time_type"],
            )

    def add_value(
        self,
        series_name: str,
        time_stamp: int,
        val,
        quality: int = QualityCodes.GOOD_NON_SPECIFIC,
    ) -> NoReturn:
        """
        The stream interface will automatically apply change only compression,
        that is, only changed values are placed in the database with some
        exceptions.

        If a value changes, it will be stored in the database.

        If a value to be added is the same as the previous value then the
        database will only store the first value and the most recent value so that
        a timestamp exists either side of the data not changing.  The exception to
        this is that every REPEAT_PERIOD a redundant data point is stored in the DB,
        so long as this function is called.  If this function is called at periods
        greater than REPEAT_PERIOD then the value is added as provided to this
        function.

        To better understand how this works, see the tests for change_only_compression.

        """
        compressor = self._compressors[series_name]
        first_value = compressor.last_val is None
        current_time_s = time.time()

        if first_value:
            if quality_is_good(quality):
                quality = QualityCodes.GOOD_STREAM_START
            elif quality_is_uncertain(quality):
                quality = QualityCodes.UNCERTAIN_STREAM_START
            else:
                logger.warning(
                    "The data stream will not begin until at least an uncertain"
                    "value is received: "
                    "Series: %s  Timestamp: %i  Quality: %i",
                    series_name,
                    time_stamp,
                    quality,
                )
                return

        val_tuple = (time_stamp, val, quality)
        compressor.add_value(val_tuple)

        if current_time_s >= self.next_commit:  # pragma: nocover
            self.connection.commit()
            self.next_commit = current_time_s + self.commit_period_s

    def close(self):
        """
        Indicates no data in each stream
        """
        for series_name, series_data_type in self._data_types.items():
            compressor = self._compressors[series_name]
            now_fx = self.NOW_FX[compressor.time_type]
            add_value(
                c=self.connection,
                series_name=series_name,
                ts=now_fx(),
                val=None,
                quality=QualityCodes.BAD_STREAM_END,
            )


@contextmanager
def stream_multiple_series(db_path: pathlib.Path, series: list[str]):
    with db_connection(db_path) as c:
        interface = StreamInterface(c, series_list=series)
        try:
            yield interface
        except Exception:
            raise
        finally:
            interface.close()
