from typing import Tuple, Union

Value = Union[float, int, bool, str, None]
Timestamp = int
Quality = int
DataTuple = Tuple[Timestamp, Value, Quality]

DATA_TYPE_REAL = "REAL"
DATA_TYPE_INT = "INT"
DATA_TYPE_BOOL = "BOOL"
DATA_TYPE_STRING = "STRING"

data_types_map = {
    float: DATA_TYPE_REAL,
    int: DATA_TYPE_INT,
    bool: DATA_TYPE_BOOL,
    str: DATA_TYPE_STRING,
}

TIME_TYPE_EPOCH_MS = "epoch_ms"
TIME_TYPE_EPOCH_100NS = "epoch_100ns"
TIME_TYPES = (TIME_TYPE_EPOCH_MS, TIME_TYPE_EPOCH_100NS)
TIME_FACTOR_EPOCH_MS_TO_S = 1e-3
TIME_FACTOR_EPOCH_S_TO_MS = 1e3
TIME_FACTOR_EPOCH_MS_TO_100NS = 1e7 / 1e3
TIME_FACTOR_EPOCH_100NS_TO_MS = 1e3 / 1e7
TIME_FACTOR_TO_S = {
    TIME_TYPE_EPOCH_MS: TIME_FACTOR_EPOCH_S_TO_MS,
    TIME_TYPE_EPOCH_100NS: TIME_FACTOR_EPOCH_100NS_TO_MS * TIME_FACTOR_EPOCH_S_TO_MS,
}

# Indicates if the timestamp comes from the remote data source or is
# generated locally.  Remote is preferred where available, otherwise
# lags create errors.
TIME_STAMP_TYPE_REMOTE = "remote"
TIME_STAMP_TYPE_LOCAL = "local"
