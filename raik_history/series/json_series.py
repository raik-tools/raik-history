import logging
import pathlib
from typing import Any

from raik_history.quality import QualityCodes
from raik_history.series.constants import (
    TIME_STAMP_TYPE_REMOTE,
    Quality,
    TIME_TYPE_EPOCH_MS,
)
from raik_history.series.series import Series
from raik_history.stream import StreamInterface

logger = logging.getLogger(__name__)


class JsonSeries(Series):
    """
    The purpose of this class is to reduce significant boilerplate that
    comes from retrieving and processing data from a JSON structure and
    then storing this data into a raik_history database.  This class may
    move to raik_history in the future.  This assumes the timestamp type
    is homogeneous where this class is used.

    The result of this class should be that where it is used can more
    easily follow a declarative style of coding.  The class can be
    subclassed if more complex processing is needed to create a value.
    """

    def __init__(
        self,
        series_name: str,
        data_path: str,
        data_type: str,
        units: str = "",
        differential_units: str = "",
        integral_units: str = "",
        time_stamp_type=TIME_STAMP_TYPE_REMOTE,
        time_type: str = TIME_TYPE_EPOCH_MS,
        *,
        default_clean=True,
        none_value=None,
        db_path: pathlib.Path = None,
    ):
        self.data_path = data_path
        super().__init__(
            series_name,
            data_type,
            units,
            differential_units,
            integral_units,
            time_stamp_type,
            time_type,
            default_clean=default_clean,
            none_value=none_value,
            db_path=db_path,
        )

    @staticmethod
    def get_data_path(path: str, data: dict) -> Any:
        """
        This uses a string to navigate a nested JSON structure

        "cars/data/0/value" is the same as accessing an object in JSON
        as obj.cars.data[0].value.

        The reason for the forward slash was really just to simplify
        processing and allow full stops in key values.

        :raises KeyError:  If there is a path error
        :raises IndexError:  If there is a path error
        """
        path_parts = path.split("/")
        while path_parts and data:
            p = path_parts.pop(0)
            if isinstance(data, list):
                p = int(p)
            data = data[p]
        return data

    def add_value_to_stream(
        self,
        stream: StreamInterface,
        value: dict,
        time_stamp: int,
        quality: Quality = QualityCodes.UNCERTAIN_NON_SPECIFIC,
        raise_on_missing_data=True,
        ignore_missing_data=True,
    ):  # pragma: nocover
        """
        Decodes the raw data and produces an appropriate value

        This function does not have coverage as the called functions
        are all covered.

        If raise_on_missing_data then errors are raised if values are omitted
        from the raw_data.  Otherwise, if ignore_missing_data then nothing
        occurs if data is missing.  If not ignore_missing_data then a
        None and BAD Quality will be added.

        :raises KeyError:  If there is a path error
        :raises IndexError:  If there is a path error
        """
        try:
            value = self.get_data_path(self.data_path, value)
            super().add_value_to_stream(stream, value, time_stamp, quality)
        except (KeyError, ValueError):
            if raise_on_missing_data:
                raise
            elif ignore_missing_data:
                pass  # silently drop
            else:
                super().add_value_to_stream(
                    stream, None, time_stamp, QualityCodes.BAD_NON_SPECIFIC
                )

    def add_value_to_db(
        self,
        value: dict,
        time_stamp: int,
        quality: Quality = QualityCodes.UNCERTAIN_NON_SPECIFIC,
        connection=None,
        raise_on_missing_data=True,
        ignore_missing_data=True,
    ):  # pragma: nocover
        """
        Decodes the raw data and produces an appropriate value

        This function does not have coverage as the called functions
        are all covered.

        If raise_on_missing_data then errors are raised if values are omitted
        from the raw_data.  Otherwise, if ignore_missing_data then nothing
        occurs if data is missing.  If not ignore_missing_data then a
        None and BAD Quality will be added.

        :raises KeyError:  If there is a path error
        :raises IndexError:  If there is a path error
        """
        try:
            raw_value = self.get_data_path(self.data_path, value)
            super().add_value_to_db(raw_value, time_stamp, quality, connection)
        except (KeyError, ValueError):
            if raise_on_missing_data:
                raise
            elif ignore_missing_data:
                pass  # silently drop
            else:
                super().add_value_to_db(
                    None, time_stamp, QualityCodes.BAD_NON_SPECIFIC, connection
                )
