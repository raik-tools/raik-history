import logging
from typing import Optional, Tuple, List

import raik_history.db.connection
import raik_history.db.data_tables
import raik_history.db.series_table
import raik_history.time_and_calendar.epoch_ms
from raik_history.quality import QualityCodes
from raik_history.series.constants import (
    TIME_STAMP_TYPE_REMOTE,
    TIME_STAMP_TYPE_LOCAL,
    Timestamp,
    Quality,
)
from raik_history.series.json_series import JsonSeries
from raik_history.series.series import existing_or_new_connection
from raik_history.series.series_set import SeriesSet
from raik_history.stream import StreamInterface

logger = logging.getLogger(__name__)


class JsonSeriesSet(SeriesSet):
    """
    This is a Series Set that works specifically with the JsonSeries class
    """

    SERIES: Tuple[JsonSeries] = tuple()
    TIME_STAMP_TYPE = TIME_STAMP_TYPE_REMOTE
    TIME_STAMP_PATH = None

    @classmethod
    def series_names(cls) -> List[str]:
        return [s.name for s in cls.SERIES]

    @classmethod
    def get_time_stamp(cls, raw_data: Optional[dict]) -> int:
        """
        Retrieves the timestamp from the raw data or generates a timestamp locally.
        """
        if cls.TIME_STAMP_TYPE == TIME_STAMP_TYPE_LOCAL or not cls.TIME_STAMP_PATH:
            time_stamp = raik_history.time_and_calendar.epoch_ms.now_epoch_ms()
        else:
            time_stamp = JsonSeries.get_data_path(cls.TIME_STAMP_PATH, raw_data)
        return time_stamp

    @classmethod
    def add_values_to_stream(
        cls,
        stream: StreamInterface,
        raw_data: dict,
        time_stamp: Optional[Timestamp] = None,
        quality: Quality = QualityCodes.UNCERTAIN_NON_SPECIFIC,
        raise_on_missing_data=True,
        ignore_missing_data=True,
    ):
        """
        This function is the same as the parent class except that it can
        retrieve a timestamp from the raw data.  It also derives all values
        in the contained series from the raw data.
        """
        if time_stamp is None:
            time_stamp = cls.get_time_stamp(raw_data)
        for s in cls.SERIES:
            s.add_value_to_stream(
                stream,
                raw_data,
                time_stamp,
                quality,
                raise_on_missing_data=raise_on_missing_data,
                ignore_missing_data=ignore_missing_data,
            )

    @classmethod
    def add_values_to_db(
        cls,
        raw_data: dict,
        time_stamp: Optional[Timestamp] = None,
        quality: Quality = QualityCodes.UNCERTAIN_NON_SPECIFIC,
        connection=None,
        raise_on_missing_data=True,
        ignore_missing_data=True,
    ):
        """
        This function is the same as the parent class except that it can
        retrieve a timestamp from the raw data.  It also derives all values
        in the contained series from the raw data.

        :raises KeyError, ValueError:  If there is an issue accessing the
        data.
        """
        if time_stamp is None:
            time_stamp = cls.get_time_stamp(raw_data)
        with existing_or_new_connection(
            existing_connection=connection, db_path=cls.db_path, obj=cls
        ) as c:
            for s in cls.SERIES:
                s.add_value_to_db(
                    raw_data,
                    time_stamp,
                    quality,
                    connection=c,
                    raise_on_missing_data=raise_on_missing_data,
                    ignore_missing_data=ignore_missing_data,
                )
