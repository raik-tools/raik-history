import functools
import logging
import pathlib
import sqlite3
from typing import List, Tuple, Dict, Optional, Type

from raik_history.quality import QualityCodes
from raik_history.series.constants import (
    TIME_STAMP_TYPE_REMOTE,
    Timestamp,
    DataTuple,
    Quality,
    Value,
)
from raik_history.series.series import Series, RawValue
from raik_history.stream import StreamInterface

logger = logging.getLogger(__name__)


@functools.cache
def get_log_string_for_series(s: List[Series]) -> str:  # pragma: nocover
    """
    This is a debugging function only, no coverage needed.
    """
    return ", ".join([f"{x.name}=%s" for x in s])


class SeriesMeta(type):
    """
    This is a metaclass used to synchronise series_set db_path with
    declared series.  While using a metaclass is more implicit in nature,
    it allows for a declarative approach to series and series sets (which
    is their intention).
    """

    def __new__(mcs, name, bases, namespace):
        # noinspection PyTypeChecker
        base_class: Type[SeriesSet] = super().__new__(mcs, name, bases, namespace)
        if base_class.db_path or base_class.db_connection:
            for s in base_class.SERIES:
                if not s.db_path and base_class.db_path:
                    s.db_path = base_class.db_path
                if not s.db_connection and base_class.db_connection:
                    s.db_connection = base_class.db_connection
        return base_class


class SeriesSet(metaclass=SeriesMeta):
    """
    The purpose of this class to collect a group of series which are
    derived from the same data source and share common properties.

    To use this class, create a subclass and set the SERIES class variable
    as a list of all related series.  Then use the class methods to access
    series in bulk.

    The class acts as a common interface for conveniently managing a
    set of series.

    Due to the simplicity of this class no coverage is needed for
    most of the functions as they are thin wrappers applied to
    all contained series objects.

    :cvar db_path:  This is a convenience variable so that a db connection
                    does not need to be provided for all calls.
    """

    SERIES: Tuple[Series] = tuple()
    TIME_STAMP_TYPE = TIME_STAMP_TYPE_REMOTE

    db_path: pathlib.Path = None
    db_connection: sqlite3.Connection = None

    @classmethod
    def create_series(cls):
        for s in cls.SERIES:
            s.sync_series()

    @classmethod
    def add_values_to_stream(
        cls,
        stream: StreamInterface,
        values: Dict[str, RawValue],
        time_stamp: Timestamp,
        quality: Quality = QualityCodes.UNCERTAIN_NON_SPECIFIC,
    ):  # pragma: nocover
        for s in cls.SERIES:
            if s.name in values:
                s.add_value_to_stream(stream, values[s.name], time_stamp, quality)

    @classmethod
    def add_values_to_db(
        cls,
        values: Dict[str, RawValue],
        time_stamp: Timestamp,
        quality: Quality = QualityCodes.UNCERTAIN_NON_SPECIFIC,
    ):  # pragma: nocover
        for s in cls.SERIES:
            if s.name in values:
                s.add_value_to_db(values[s.name], time_stamp, quality)

    @classmethod
    def log_info(cls, log):  # pragma: nocover
        """
        Logs the current values
        """
        log_str = get_log_string_for_series(cls.SERIES)
        log.info(log_str, *[s.strict_value() for s in cls.SERIES])

    @classmethod
    def query_data_values(
        cls,
        ts__lt: Optional[Timestamp] = None,
        ts__lte: Optional[Timestamp] = None,
        ts__eq: Optional[Timestamp] = None,
        ts__gte: Optional[Timestamp] = None,
        ts__gt: Optional[Timestamp] = None,
        val__is_none: Optional[bool] = None,
        val__lt: Optional[Value] = None,
        val__lte: Optional[Value] = None,
        val__eq: Optional[Value] = None,
        val__ne: Optional[Value] = None,
        val__gte: Optional[Value] = None,
        val__gt: Optional[Value] = None,
        quality__lt: Optional[Quality] = None,
        quality__lte: Optional[Quality] = None,
        quality__eq: Optional[Quality] = None,
        quality__ne: Optional[Quality] = None,
        quality__gte: Optional[Quality] = None,
        quality__gt: Optional[Quality] = None,
        include_outer_value_start=False,
        include_outer_value_end=False,
        chronological_order=True,
        count: Optional[int] = None,
        page_size: Optional[int] = 10000,
    ) -> Dict[str, list[DataTuple]]:  # pragma: nocover
        """
        This is a thin wrapper and does not require coverage.
        """
        return {
            s.name: list(
                s.query_data_values(
                    ts__lt=ts__lt,
                    ts__lte=ts__lte,
                    ts__eq=ts__eq,
                    ts__gte=ts__gte,
                    ts__gt=ts__gt,
                    val__is_none=val__is_none,
                    val__lt=val__lt,
                    val__lte=val__lte,
                    val__eq=val__eq,
                    val__ne=val__ne,
                    val__gte=val__gte,
                    val__gt=val__gt,
                    quality__lt=quality__lt,
                    quality__lte=quality__lte,
                    quality__eq=quality__eq,
                    quality__ne=quality__ne,
                    quality__gte=quality__gte,
                    quality__gt=quality__gt,
                    include_outer_value_start=include_outer_value_start,
                    include_outer_value_end=include_outer_value_end,
                    chronological_order=chronological_order,
                    count=count,
                    page_size=page_size,
                )
            )
            for s in cls.SERIES
        }

    @classmethod
    def get_value_at_time(
        cls, ts: Timestamp
    ) -> Dict[str, Optional[DataTuple]]:  # pragma: nocover
        """
        No coverage due to being a fairly thin wrapper.
        """
        return {s.name: s.get_value_at_time(ts) for s in cls.SERIES}

    @classmethod
    def get_latest_value(cls) -> Dict[str, Optional[DataTuple]]:  # pragma: nocover
        """
        No coverage due to being a fairly thin wrapper.
        """
        return {s.name: s.get_latest_value() for s in cls.SERIES}

    @classmethod
    def get_oldest_value(cls) -> Dict[str, Optional[DataTuple]]:  # pragma: nocover
        """
        No coverage due to being a fairly thin wrapper.
        """
        return {s.name: s.get_oldest_value() for s in cls.SERIES}

    @classmethod
    def get_last_or_at_value(
        cls, ts: Timestamp, count=1
    ) -> Dict[str, list[DataTuple]]:  # pragma: nocover
        """
        No coverage due to being a fairly thin wrapper.
        """
        return {s.name: s.get_last_or_at_value(ts, count=count) for s in cls.SERIES}

    @classmethod
    def get_at_or_next_value(
        cls, ts: Timestamp, count=1
    ) -> Dict[str, list[DataTuple]]:  # pragma: nocover
        """
        No coverage due to being a fairly thin wrapper.
        """
        return {s.name: s.get_at_or_next_value(ts, count=count) for s in cls.SERIES}

    @classmethod
    def get_all_values_no_page(cls):  # pragma: nocover
        raise Exception("This function is intentionally not implemented")

    @classmethod
    def get_periodic_series(
        cls,
        start_time_ms: Timestamp,
        period_ms: int,
        end_time_ms: Timestamp = None,
        page_size=10000,
    ) -> Dict[str, List[DataTuple]]:  # pragma: nocover
        """
        No coverage due to being a fairly thin wrapper.
        """
        return {
            s.name: list(
                s.get_periodic_series(
                    start_time_ms=start_time_ms,
                    period_ms=period_ms,
                    end_time_ms=end_time_ms,
                    page_size=page_size,
                )
            )
            for s in cls.SERIES
        }
