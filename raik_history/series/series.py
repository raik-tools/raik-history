import contextlib
import logging
import pathlib
import sqlite3
from typing import List, Any, Tuple, Union, Generator, Optional

import raik_history.db.connection
import raik_history.db.series_table
from raik_history.db import data_tables, series_table
from raik_history.calculations import aggregates
import raik_history.time_and_calendar.epoch_ms
from raik_history.errors import DataExistsError
from raik_history.quality import QualityCodes
from raik_history.series.constants import (
    TIME_TYPE_EPOCH_MS,
    TIME_STAMP_TYPE_REMOTE,
    DATA_TYPE_BOOL,
    DATA_TYPE_STRING,
    DATA_TYPE_INT,
    DATA_TYPE_REAL,
    Value,
    Timestamp,
    Quality,
    DataTuple,
)
from raik_history.stream import StreamInterface

logger = logging.getLogger(__name__)


BatchDataTuple = Tuple[List[Timestamp], List[Value], List[Quality]]
RawValue = Any

CONNECTION_ATTRIBUTE_NAME = "db_connection"
PATH_ATTRIBUTE_NAME = "db_path"


@contextlib.contextmanager
def existing_or_new_connection(
    existing_connection: sqlite3.Connection = None,
    db_path: pathlib.Path = None,
    obj=None,
    perform_migration=False,
):
    """
    This is a convenience context manager that allows functions to be
    called with a connection object, have a connection object attached to the
    class or object, or to have one created if no connection object is provided.
    """
    if obj:
        connection_attribute = getattr(obj, CONNECTION_ATTRIBUTE_NAME, None)
        path_attribute = getattr(obj, PATH_ATTRIBUTE_NAME, None)
    else:
        connection_attribute = None
        path_attribute = None

    if connection_attribute:
        yield connection_attribute
    elif path_attribute:
        with raik_history.db.connection.db_connection(
            db_path=path_attribute, perform_migration=perform_migration
        ) as c:
            yield c
    elif existing_connection:
        yield existing_connection
    elif db_path:
        with raik_history.db.connection.db_connection(
            db_path=db_path, perform_migration=perform_migration
        ) as c:
            yield c
    else:
        raise ValueError(f"Unable to find a database for object {obj}")


class Series:
    """
    This class exists to support a more declarative approach for
    defining and interacting with series.  This becomes more evident
    when working with series sets, which this is a building block towards.

    This class can be subclasses for custom series functionality and configuration.

    It is common for data to be handled in batch.  Many functions are overloaded
    to accept batch data as well as singular data.
    """

    TIME_TYPE = TIME_TYPE_EPOCH_MS

    CLEAN_FX = {
        DATA_TYPE_REAL: float,
        DATA_TYPE_INT: int,
        DATA_TYPE_BOOL: bool,
        DATA_TYPE_STRING: str,
    }

    db_connection: sqlite3.Connection = None

    def __init__(
        self,
        series_name: str,
        data_type: str,
        units: str = "",
        differential_units: str = "",
        integral_units: str = "",
        time_stamp_type=TIME_STAMP_TYPE_REMOTE,
        time_type: str = TIME_TYPE_EPOCH_MS,
        *,
        default_clean=True,
        none_value=None,
        db_path: pathlib.Path = None,
        db_connection: sqlite3.Connection = None,
    ):
        # Metadata values
        self.name = series_name
        self.data_type = data_type
        self.units = units
        self.differential_units = differential_units
        self.integral_units = integral_units
        self.time_stamp_type = time_stamp_type
        self.time_type = time_type
        self.values_received = 0
        self.last_val = None
        self.last_time_stamp = None
        self.last_quality = None
        self.default_clean = default_clean
        self.none_value = none_value
        self.db_path = db_path
        if db_connection:
            self.db_connection = db_connection

    def __str__(self):  # pragma: nocover
        return f"series: {self.name}"

    @classmethod
    def from_db(cls, name: str, connection=None, db_path=None, **kwargs) -> "Series":
        """
        Instantiates the class from the db record
        """
        with existing_or_new_connection(connection, db_path, obj=cls) as c:
            data = raik_history.db.series_table.get_series(c, name)

        return cls(
            series_name=data["name"],
            data_type=data["data_type"],
            time_type=data["time_type"],
            time_stamp_type=data["time_stamp_type"],
            units=data["units"],
            differential_units=data["differential_units"],
            integral_units=data["integral_units"],
            **kwargs,
        )

    def sync_series(self):
        """
        This is a function that will create a series.  If the series
        already exists, then an INFO message is logged and the function
        does nothing.

        :raises ValueError:  If db_path is not set for the series.
        """
        with existing_or_new_connection(obj=self, perform_migration=True) as c:
            interface_list = raik_history.db.series_table.get_series_list(c)
            series_list = [a["name"] for a in interface_list]
            if self.name not in series_list:
                raik_history.db.series_table.add_series(
                    c,
                    raik_history.db.series_table.SeriesInterface(
                        name=self.name,
                        data_type=self.data_type,
                        time_type=self.time_type,
                        time_stamp_type=self.time_stamp_type,
                        units=self.units,
                        differential_units=self.differential_units,
                        integral_units=self.integral_units,
                        storage_configuration={},
                        retrieval_configuration={},
                    ),
                )
            else:
                series_table.update_series(
                    c=c,
                    series_name=self.name,
                    units=self.units,
                    differential_units=self.differential_units,
                    integral_units=self.integral_units,
                )

    def default_clean_val(self, val: Value):
        if val is None:
            return self.none_value
        else:
            return self.CLEAN_FX[self.data_type](val)

    def clean(
        self, time_stamp: Timestamp, val: RawValue, quality: Quality
    ) -> Union[DataTuple, BatchDataTuple]:
        """
        A hook that subclasses can override to clean the value.  This can also
        return iterables for all three variables for bulk data addition.

        The clean function is expected to handle all use cases.  An exception
        is not expected to be raised from this function and instead the data
        should be set to a None Value and Bad Quality.
        """
        if self.default_clean:
            return time_stamp, self.default_clean_val(val), quality
        else:
            return time_stamp, val, quality

    def _generate_values(
        self,
        time_stamp: int,
        value: RawValue,
        quality: Quality = QualityCodes.UNCERTAIN_NON_SPECIFIC,
    ) -> Generator[Tuple[Timestamp, Value, Quality], None, None]:
        """
        This function yields values to be stored in the DB based on the
        data received.  It does not handle DB, just value processing.
        """
        c_time_stamp, value, quality = self.clean(time_stamp, value, quality)
        if hasattr(c_time_stamp, "__len__"):
            for ts, v, q in zip(c_time_stamp, value, quality):
                yield ts, v, q
            self.last_val = value[-1]
            self.last_quality = quality[-1]
            self.last_time_stamp = c_time_stamp[-1]
            self.values_received += len(value)
        else:
            yield time_stamp, value, quality
            self.last_val = value
            self.last_quality = quality
            self.last_time_stamp = time_stamp
            self.values_received += 1

    def add_value_to_stream(
        self,
        stream: StreamInterface,
        value: RawValue,
        time_stamp: int,
        quality: Quality = QualityCodes.UNCERTAIN_NON_SPECIFIC,
    ):
        """
        The clean function may be used to intercept the raw value provided.  This function,
        will add to the database via the stream interface and hence will implement
        compression based on in memory state.
        """
        for ts, v, q in self._generate_values(time_stamp, value, quality):
            stream.add_value(
                series_name=self.name,
                time_stamp=ts,
                val=v,
                quality=q,
            )

    def add_value_to_db(
        self,
        value: RawValue,
        time_stamp: int,
        quality: Quality = QualityCodes.UNCERTAIN_NON_SPECIFIC,
        connection=None,
    ):
        """
        Adds the given value to the database (no processing).  The clean function may
        intercept this function and return a list or set of values to add.  This is useful
        when the source produces a batch of data or a composite data set.
        """
        with existing_or_new_connection(
            existing_connection=connection, db_path=self.db_path, obj=self
        ) as c:
            for ts, v, q in self._generate_values(time_stamp, value, quality):
                try:
                    data_tables.add_value(
                        c,
                        series_name=self.name,
                        ts=ts,
                        val=v,
                        quality=q,
                    )
                except sqlite3.IntegrityError:
                    # Check that this is not spurious and that the data on disk is the same
                    existing_data = data_tables.get_value_at_time(c, self.name, ts)
                    if existing_data[1] != v:
                        raise DataExistsError(self.name, ts)

    def strict_value(self):
        """
        This function will return the last known value strictly in the
        data type for that value.  A null or None like value will result
        in the data type equivalent of 0.
        """
        if self.data_type == DATA_TYPE_BOOL:
            return self.last_val or False
        if self.data_type == DATA_TYPE_STRING:
            return self.last_val or ""
        if self.data_type == DATA_TYPE_INT:
            return self.last_val or 0
        if self.data_type == DATA_TYPE_REAL:
            return self.last_val or 0.0
        else:
            raise NotImplementedError(
                f"Data Type: {self.data_type} is not implemented."
            )

    def query_data_values(
        self,
        connection=None,
        ts__lt: Optional[Timestamp] = None,
        ts__lte: Optional[Timestamp] = None,
        ts__eq: Optional[Timestamp] = None,
        ts__gte: Optional[Timestamp] = None,
        ts__gt: Optional[Timestamp] = None,
        val__is_none: Optional[bool] = None,
        val__lt: Optional[Value] = None,
        val__lte: Optional[Value] = None,
        val__eq: Optional[Value] = None,
        val__ne: Optional[Value] = None,
        val__gte: Optional[Value] = None,
        val__gt: Optional[Value] = None,
        quality__lt: Optional[Quality] = None,
        quality__lte: Optional[Quality] = None,
        quality__eq: Optional[Quality] = None,
        quality__ne: Optional[Quality] = None,
        quality__gte: Optional[Quality] = None,
        quality__gt: Optional[Quality] = None,
        include_outer_value_start=False,
        include_outer_value_end=False,
        chronological_order=True,
        count: Optional[int] = None,
        page_size: Optional[int] = 10000,
    ) -> Generator[DataTuple, None, None]:  # pragma: nocover
        """
        This is a thin wrapper and does not require coverage.
        """
        with existing_or_new_connection(connection, self.db_path, obj=self) as c:
            for x in data_tables.query_data_values(
                c,
                self.name,
                ts__lt=ts__lt,
                ts__lte=ts__lte,
                ts__eq=ts__eq,
                ts__gte=ts__gte,
                ts__gt=ts__gt,
                val__is_none=val__is_none,
                val__lt=val__lt,
                val__lte=val__lte,
                val__eq=val__eq,
                val__ne=val__ne,
                val__gte=val__gte,
                val__gt=val__gt,
                quality__lt=quality__lt,
                quality__lte=quality__lte,
                quality__eq=quality__eq,
                quality__ne=quality__ne,
                quality__gte=quality__gte,
                quality__gt=quality__gt,
                include_outer_value_start=include_outer_value_start,
                include_outer_value_end=include_outer_value_end,
                chronological_order=chronological_order,
                count=count,
                page_size=page_size,
            ):
                yield x

    def get_all_values_no_page(self, connection=None):  # pragma: nocover
        """
        This is a thin wrapper and does not require coverage.
        """
        with existing_or_new_connection(connection, self.db_path, obj=self) as c:
            return data_tables.get_all_values_no_page(c, self.name)

    def get_all_values_in_range(
        self,
        start_time: Optional[Timestamp],
        end_time: Optional[Timestamp],
        connection=None,
    ):  # pragma: nocover
        """
        This is a thin wrapper and does not require coverage.
        """
        with existing_or_new_connection(connection, self.db_path, obj=self) as c:
            return data_tables.get_all_values_in_range(
                c, self.name, start_time, end_time
            )

    def get_value_at_time(self, ts: Timestamp, connection=None):  # pragma: nocover
        """
        This is a thin wrapper and does not require coverage.
        """
        with existing_or_new_connection(connection, self.db_path, obj=self) as c:
            return data_tables.get_value_at_time(c, self.name, ts)

    def get_latest_value(self, connection=None):  # pragma: nocover
        """
        This is a thin wrapper and does not require coverage.
        """
        with existing_or_new_connection(connection, self.db_path, obj=self) as c:
            return data_tables.get_latest_value(c, self.name)

    def get_oldest_value(self, connection=None):  # pragma: nocover
        """
        This is a thin wrapper and does not require coverage.
        """
        with existing_or_new_connection(connection, self.db_path, obj=self) as c:
            return data_tables.get_oldest_value(c, self.name)

    def get_last_or_at_value(
        self, ts: Timestamp, connection=None, count=1
    ):  # pragma: nocover
        """
        This is a thin wrapper and does not require coverage.
        """
        with existing_or_new_connection(connection, self.db_path, obj=self) as c:
            return data_tables.get_last_or_at_value(c, self.name, ts, count=count)

    def get_at_or_next_value(self, ts, connection=None, count=1):  # pragma: nocover
        """
        This is a thin wrapper and does not require coverage.
        """
        with existing_or_new_connection(connection, self.db_path, obj=self) as c:
            return data_tables.get_at_or_next_value(c, self.name, ts, count=count)

    def get_periodic_series(
        self,
        start_time_ms: Timestamp,
        period_ms: int,
        end_time_ms: Timestamp = None,
        page_size=10000,
        connection=None,
    ):  # pragma: nocover
        """
        This is a thin wrapper and does not require coverage.
        """
        with existing_or_new_connection(connection, self.db_path, obj=self) as c:
            return data_tables.get_periodic_series(
                c, self.name, start_time_ms, period_ms, end_time_ms, page_size
            )

    def get_max(
        self,
        start_time: Timestamp,
        end_time: Timestamp,
        include_bad_values=False,
        include_uncertain_values=False,
        connection=None,
    ):  # pragma: nocover
        with existing_or_new_connection(connection, self.db_path, obj=self) as c:
            return aggregates.get_max(
                c,
                self.name,
                start_time,
                end_time,
                include_bad_values,
                include_uncertain_values,
            )

    def get_min(
        self,
        start_time: Timestamp,
        end_time: Timestamp,
        include_bad_values=False,
        include_uncertain_values=False,
        connection=None,
    ):  # pragma: nocover
        with existing_or_new_connection(connection, self.db_path, obj=self) as c:
            return aggregates.get_min(
                c,
                self.name,
                start_time,
                end_time,
                include_bad_values,
                include_uncertain_values,
            )

    def get_count(
        self,
        start_time: Timestamp,
        end_time: Timestamp,
        include_bad_values=False,
        include_uncertain_values=False,
        connection=None,
    ):  # pragma: nocover
        with existing_or_new_connection(connection, self.db_path, obj=self) as c:
            return aggregates.get_count(
                c,
                self.name,
                start_time,
                end_time,
                include_bad_values,
                include_uncertain_values,
            )

    def time_weighted_sum(
        self,
        start_time: Timestamp,
        end_time: Timestamp,
        rate_conversion: aggregates.TimeWeightedRateConversion = (
            aggregates.TimeWeightedRateConversion.PerSecond
        ),
        include_bad_values=False,
        include_uncertain_values=False,
        page_size: int = 10000,
        connection=None,
    ):  # pragma: nocover
        with existing_or_new_connection(connection, self.db_path, obj=self) as c:
            return aggregates.time_weighted_sum(
                c,
                self.name,
                self.time_type,
                start_time,
                end_time,
                rate_conversion,
                include_bad_values,
                include_uncertain_values,
                page_size,
                integral_units=self.integral_units,
            )
