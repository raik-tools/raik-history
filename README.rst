======
README
======

Question
========

How can I quickly and efficiently embed stored time series data within a custom
application or script?

Why
===

Note:  By Industrial, I refer to to high-tech heavy-industries such as
in mining, oil and gas, utilities, and so force.

I have been unsuccessful in finding a time-series specialist database
that is light-weight and suits low power devices.  Coming from an
industrial automation background (IACS, DCS, PLC, SCADA) I have seen
the value of a specialty system in capturing and extracting time-series
data.  There are some great options out there for larger systems, such
as using Casandra or the new wave of IoT platforms.  However, Industrial
platforms such as GE Proficy, Aveva (who I believe consumed Wonderware
Archestra?) , and OSISoft PI have all been solving parts of these problems
for several decades.

Options
-------

https://thingsboard.io - A very interesting product, uses PostgreSQL, Cassandra,
or others as a back end.  Not appropriate for a stand along device / product.

https://www.timescale.com/ - Built on PSQL, another reasonable contender for a
heavier system.

https://en.wikipedia.org/wiki/Time_series_database - Offers a list of mostly
open source solutions and a few others.  From what I can see, these are
heavy weight systems.

https://openremote.io/product/ - Looks interesting, but the source is not readily available,
so doesn't really count as "OpenSource" in my mind.

What I'm missing is something that I can integrate into a custom application
that needs some basic time series capabilities and is significantly smaller
overall than any of these options.

Keep Reading
============

To read more, head to the docs.  To generate the documentation, ensure
that plantuml is also installed.

Roadmap
=======

* Feasibility (see prototype)
* Bare-bones data storage with no compression
* Start implementing functionality to compliment garys_nas
* Establish a library and upload to PyPI (wait for IP to be clear)

