=========
Decisions
=========

Good or bad, here are some key decisions.

Data Storage
============

SQLite for the DB because it is small, simple, and doesn't require many
extra libraries or tools.

Timestamp resolution
--------------------

Timestamp resolution: 32 bit resolution is not enough for any useful
time representation.  SQLite supports 64-bit integers.  If this uses
the Holocene calendar, with 0 corresponding to 10,000 BCE in UTC time
and the time resolution being 100 nanoseconds (common with DBs), then
this database will last: ``end_of_time = 2**64/(10**7)/3600/24/365.25 - 10000 = 48454 ACE``.

.. NOTE::
    Unfortunately, Python does not support dates outside of 1 to 9999 and
    considering this application is for storing sensor data, it is highly
    unlikely it will store data outside of this range.  As such, the
    datum will remain as the EPOCH instead of the Human Era (although the
    later would be nice for philosophical reasons) and will follow Unix
    Time which is based on a measurement of 1 second that is based on
    the Earth's rotation and not on .  For more information consider
    `this link on calendars <https://planetcalc.com/505/>`_ and `this link
    which discusses different time standards
    <http://www.madore.org/~david/computers/unix-leap-seconds.html>`_. Also
    refer to some calculations in the bottom of
    :py:mod`raik_history.tim_and_calendar.constants`.


That is probably the right balance of technology and precision.  I will
be interested to see how this goes.  Note that it will not be an accurate
time measure on most systems and that accurate recording requires a more
holistic design.

Choice of Time Resolution
-------------------------

Rather than trying to find a "one size fits all" approach, a number
of options can be provided.  Currently considered options are:

================  ===============  ============  ========  ========
Name              Datum            Years Range   Min Year  Max Year
================  ===============  ============  ========  ========
Epoch ms          1970-1-1 UTC     584554449     -292 My   +292 My
Holocene 100ns    -9999-1-1 UTC    58455         -39000    +19000
Epoch 100ns       1970-1-1 UTC     58455         -27000    +31000
Epoch us          1970-1-1 UTC     584554        -290000   +290000
Epoch seconds     1970-1-1 UTC     584554449367  -584 Gy   +584 Gy
================  ===============  ============  ========  ========

Notes:

* Years in the table above are only a rough calculation
* All values are based on a signed 64bit number with a range of
  ``−(2**63) to 2**63 − 1`` or -9,223,372,036,854,775,808 to
  9,223,372,036,854,775,807.
* Refer to `This Link <https://en.wikipedia.org/wiki/Holocene_calendar#Conversion>`_
  for details of calender conversions


No Joins
--------

There is no need for joins in the DB itself due to the nature
of the data.  We could technically join the series table to a data table,
but the join is based on a table name, not based on a key and it really
does not add value.

Dependencies
============

Pint
----

This package is being brought in because, although it is in prerelease,
it:

* Is popular on Github
* Has been maintained for a long time
* Is pure python (no dependencies as well)
* Integrates with Pandas and Numpy
* Contains unit tests, coverage reports and follow black.
* Has adequate defaults