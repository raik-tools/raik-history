============
Domain Model
============

Purpose
=======

The purpose of this page is to describe the domain model, that is, the
concept of recording arbitrary historical data independently of the
implementation.  The scope of this page is to capture the abstract concepts
within the problem domain and clarify important terminology.

Terminology
===========

IACS
----

Industrial Automation and Control System:  This is a term used in heavy
industries to describe the collection of systems used for automation of
industrial processes and plant equipment.  As this is my background, a
number of concepts and terms have been drawn from this industry and it
may be referenced as justification for some of my decisions.

Historian
---------

This is a common term in IACSs and refers to a specialised time-series
database that offers both data capture and compression as well as
domain-specific data retrieval.  This project is intended to offer some
of the interfaces common for historical time-series system and sensor
data modelled from this industry.

Series
------

For this system a series refers to the time series data points that
are derived from a single points.  In heavy-industries this may be called
a tag or in IoT this may be called a sensor, but focussing on a single
variable (as some sensors are multivariate sensors).

Data point
----------

This refers to the most atomic piece of data, and in a time-series
system this is typically a time stamp and value within the context of
a series (the series is implied).

The data point may be augmented with additional information such as
quality information (how reliable is the value), and the series the
data point represents.


Model
=====

The following depicts the nature of a time-series system using the
terminology above.  Although this is a class diagram, it should be
considered in its most abstract form.

.. uml::

    entity "Sensor" as s {
        * sensor_uuid
        --
        * description
        * location
    }
    entity "Variable" as x{
        * variable_uuid
        --
        * description
        * data_type
        * units_of_measure
    }
    entity "Data Point" as p{
        * variable_uuid
        * timestamp
        --
        quality
    }
    entity "Series" as series{
        * variable_uuid
        --
        * storage_configuration
        * retrieval_configuration
    }
    entity "Historian" as historian

    s ||--|{ x
    x ||--|{ p
    x ||--o| series
    series ||--o{ p
    historian ||--o{ series
