=========
Prototype
=========

Overview
========

The initial prototype example will be based on SQLite because:
* It is simple
* It supports all the needed data types (int and real)
* There doesn't appear to be any limits of its use (other than it is
  ideal to use it from a single process, but that is not required).

The concept being as follows:
* Build a module of time-series storage that manipulates an encapsulated
  SQLite data file
* Each series (time series) is stored in a dedicated table with a fixed
  structure that is intended to be immutable.
* An interface exists that abstracts the storage and just provides the
  desired common time-series requests and actions.

Research Comments:
* https://stackoverflow.com/questions/11475338/maximum-of-tables-a-sqlite-database-file-supports
* https://www.sqlite.org/datatype3.html
* https://stackoverflow.com/questions/65422890/how-to-use-time-series-with-sqlite-with-fast-time-range-queries

Feasibility
===========

See `Performance Trials`_ for more information.
