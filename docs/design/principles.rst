==========
Principles
==========

This library/component/module is intended to be:
* Opinionated
* As simple as possible
* following PEP20
* fast to use with sensible defaults (batteries included)
* transparent
* data validation is out of scope of this library, read the api function docs for
  important clues.
* data analytics is out of scope for this library.

When developing this, the following paradigms and patterns have been accepted:
* TODO

The following paradigms and patterns were not as being used throughout the application:
* asyncio:  SQLite processes in the same thread as the call, there is no
  server and so any io-waiting that is needed will be deep within the
  SQLite module.  So using asyncio from SQLite does not make sense as
  from the Python perspective the application will be CPU bound as much
  as it is IO Bound (I don't know the ins and outs, but I suspect lookup
  calculations occurring in the thread with numerous disk operations).

.. NOTE::
    If this library needs parallelism, then a single thread that performs
    all SQLite calls might be created and then queues used to send and receive
    requests.  Just an idea while I'm thinking about it.
