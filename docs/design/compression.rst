===========
Compression
===========

Status
======

Compression interfaces are still highly experimental.  From early versions
it is clear that most compression algorithms are going to require ongoing
state in make decisions on compression.

Interface
=========

The compression algorithms are designed to receive a stream of data
to be compressed (data tuples).  Compression algorithms will always
capture data quality changes.  When data is retrieved after compression
it should be representative of the original data going in.

Since the algorithms are intended to receive a stream of data, they only
need to make one of two decisions:
* Store a new value
* Update the previous value to a new value

The algorithms should err on the side of storing more data than needed. These
algorithms won't need an initial state and will determine their state from the
stream as received.