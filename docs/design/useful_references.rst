=================
Useful References
=================

A quick RST guide:
https://docutils.sourceforge.io/docs/user/rst/quickref.html

Sphinx and RST Cheat Sheet:
https://thomas-cokelaer.info/tutorials/sphinx/rest_syntax.html

https://docs.readthedocs.io/en/stable/guides/cross-referencing-with-sphinx.html
