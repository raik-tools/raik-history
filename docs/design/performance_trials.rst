==================
Performance Trials
==================

These are not meant to be comprehensive or used for benchmarking, just indicative.

Prototype Trial
===============

In this trial, a basic script was generated that:
* Creates a set of series
* Adds random data points to each series (in different patterns, and in chronological order)
* See the manual performance run for more information

============================ ==========  ==========  ====================  ==========  ========  =======  =======
Trial                        Tables      Points      Total Points          Duration s  Size MiB  s/point  B/point
============================ ==========  ==========  ====================  ==========  ========  =======  =======
add_data_to_all_series       10          10          100                   0.53        0.059     0.005    614
add_data_to_all_series       10          100         1000                  4.61        0.586     0.0046   61.4
add_data_to_all_series       10          1000        10000                 45.2        0.293     0.0045   30.7
add_data_to_all_series       1000        10          10000                 104         4.16      0.01     435
============================ ==========  ==========  ====================  ==========  ========  =======  =======
