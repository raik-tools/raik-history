============
Architecture
============

Modules
=======

Key modules and interfaces are noted below.

.. uml::

    interface "Historian" as hist
    component package [__init__.py:  This package provides the officially supported
    interface to the historian.]
    hist - [package]
    [package] -> series_int

    component series [series:  This package is responsible for the storage
    and retrieval of data series.]
    series_int - [series]
    [series] -> DB_W
    [series] -> DB_R

    component db [db:  This package is responsible for the db
    interface.]
    DB_W - [db]
    DB_R - [db]


Data Storage
============

To keep this lightweight and as simple as possible and keep to the focus
of storing time series data, the following database tables will be created
in an sqlite database.

.. uml::

    entity "series" {
        * name : text
        --
        * data_type : enum
        * time_type : enum = epoch_ms
        units: text
        storage_configuration: json
        retrieval_configuration: json
    }

    entity "series_<name>" {
        * timestamp : 64bit+ uint
        --
        * val : data_type
        quality: int
        alt : data_type
    }

    entity "series_<name1>..."
    entity "series_<name2>..."
    entity "series_<name3>..."
    entity "series_<name4>..."
    entity "series_<name5>..."

Each series will get a dedicated table of the appropriate type.