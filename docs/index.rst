.. RAIK History documentation master file, created by
   sphinx-quickstart on Tue Feb  1 20:30:19 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to RAIK History's documentation!
========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   user_docs/quick_start
   user_docs/change_log
   design/principles
   design/domain_model
   design/prototype
   design/architecture
   design/decisions
   design/performance_trials
   design/useful_references


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
