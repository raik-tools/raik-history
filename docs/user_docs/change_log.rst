==========
Change Log
==========

This just captures the most relevant changes, especially when refactoring
occurs.

0.14.0
======

* db_path has been removed as a parameter to create_series and sync_series.  Instead, the db_path
  needs to be set at a class level.  The expected use case is that a db_path is
  determined at application load time and does not change (or hard coded for an application).
* connection parameter removed from SeriesSet classes due to changes in 0.13.0
* Database change only compression was modified so that there is always a data point on
  either side of an input plateau.

0.12.0
======

* Standardising on "get" verb instead of "fetch" verb as it is already
  established in data queries and JSON classes.  This reverts some changes
  in 0.10.0

0.11.0
======

* get_last_or_at_value return type changed to be a list with a new kwarg
* get_at_or_next_value return type changed to be a list with a new kwarg


0.10.0
======

* get_series_list renamed to fetch_series_list (reverted in 0.12.0)
* the Event class has been added


0.9.0
=====

* Method create_series_in_db renamed to "sync_series" which creates or updates the series

